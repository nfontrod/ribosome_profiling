#!/bin/bash

# install nextflow into ./bin folder
java -version
curl -s https://get.nextflow.io | bash
mv nextflow bin/nextflow

# download tiny ribosome profiling dataset.
git clone git@gitbio.ens-lyon.fr:nfontrod/tiny_ribosome_profiling_dataset.git data/tiny_ribosome_profiling_dataset


# test the pipelines
if [ -z "$1" ]; then
    PROFILE="singularity"
else
    PROFILE=$1
fi

./bin/nextflow src/pipelines/RibosomeProfiling.nf \
  -c src/pipelines/RibosomeProfiling.config \
  -profile $PROFILE \
  --genome "data/tiny_ribosome_profiling_dataset/synth.fasta" \
  --fastq "data/tiny_ribosome_profiling_dataset/fastq/*.fastq" \
  --rrna_fasta "data/tiny_ribosome_profiling_dataset/contaminants.fasta" \
  -resume