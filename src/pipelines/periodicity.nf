nextflow.enable.dsl=2

/* Periodicity analysis pipeline:
 * Short pipeline dedicated to the creation of periodicity figures
*/


/*
 ****************************************************************
                              Logs
 ****************************************************************
*/


log.info "Folder containing bam file and their index: ${params.bam_folder}"
log.info "Annotation file used: ${params.bed_file}"
log.info "Output folder: results/${params.folder}"
log.info "Parameters of the periodicity figure: "
log.info "  -codon: ${params.figure_parameters.codon}"
log.info "  -size: ${params.figure_parameters.size}"
log.info "  -sample: ${params.figure_parameters.sample}"
log.info "  -normalize: ${params.figure_parameters.normalize}"


/*
 ****************************************************************
                  Channel definitions
 ****************************************************************
*/


Channel
    .fromFilePairs ( "${params.bam_folder}/*_sorted.{bam,bam.bai}" )
    .ifEmpty { error "Unable to file bam file and/or their index !"}
    .map { it -> [it[0], it[1][0], it[1][1]]}
    .set { bam_files }

Channel
    .fromPath( params.bed_file )
    .ifEmpty { error "Unable to find the bed file"}
    .set { bed_file }

Channel
    .from( params.figure_parameters.codon )
    .combine (
        Channel
            .from( params.figure_parameters.size )
    )
    .combine (
        Channel
            .from( params.figure_parameters.sample )
    )
    .combine (
        Channel
            .from( params.figure_parameters.normalize )
    ).set { figure_params }


/*
 ****************************************************************
                          Imports
 ****************************************************************
*/


include { periodicity_table; 
          periodicity_table_psite; 
          merge_tables; 
          merge_tables_psite;
          merge_offset; 
          figures;
          figures as figures_psite } from './nf_modules/periodicity.nf'


/*
 ****************************************************************
                          Workflow
 ****************************************************************
*/


workflow {
    periodicity_table(bam_files, bed_file.collect())
    merge_tables(periodicity_table.out.position.collect(), 
                 periodicity_table.out.offset.collect())
    merge_offset(merge_tables.out.offset)
    
    if (!params.p_site_periodicity) {
        figures(figure_params, merge_tables.out.position.collect())
    } else {
        periodicity_table_psite(bam_files, bed_file.collect(), 
                                merge_offset.out.offset.collect())
        merge_tables_psite(periodicity_table_psite.out.position.collect())
        figures_psite(figure_params, merge_tables_psite.out.position.collect())
    }

}
