nextflow.enable.dsl=2

/* Peak calling pipeline :
 * Pipeline that check the codon/encoded amino acids in peaks obtained from 
* ribosome profiling experiment
*/


/*
 ****************************************************************
                          Imports
 ****************************************************************
*/

peak_module = './nf_modules/peak_calling.nf'

include { bam_to_bigwig;  
          peak_composition; 
          codon_statistics; 
          bedtools_merge;
          get_common_peaks; 
          merge_stats; 
          peak_stat_location } from peak_module

include { peak_caller } from peak_module addParams(coverage: params.filter.coverage)

if (params.slop.slop_type == "bedtools") {
    include { bedtools_slop as slop_process
            } from peak_module addParams(threshold: params.slop.threshold,
                                              slop_size: params.slop.slop_size)
} else {
    include { my_slop as slop_process
            } from peak_module addParams(threshold: params.slop.threshold,
                                              slop_size: params.slop.slop_size)
}


include { merge_codon_table
         } from peak_module \
         addParams(out: "03_peaks_composition/")

include { combine_channels } from './nf_functions/peak_calling.nf'

// updating parameters
bam_list = params.input.collect {k , v -> "${params.bam_folder}/${v.bam}"}


// specifi definition for random control statistics
if (params.stats.random) {
    params.filter.merge = true

    include { random_ctrl_statistics
         } from peak_module \
         addParams(iteration: "${params.stats.iteration}",
                   colors: "${params.stats.colors}")

}

/*
 ****************************************************************
                  Channel definitions
 ****************************************************************
*/


Channel
    .fromPath(bam_list)
    .ifEmpty{ error "Cannot find bam in ${params.bam_folder}" }
    .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
    .set { bam_files }

Channel
    .fromPath(bam_list.collect {v -> "${v}.bai"})
    .ifEmpty{ error "Cannot find min_fp_size in config file" }
    .set { bam_idx }

Channel
    .from(params.input.collect {k , v -> v.sample})
    .ifEmpty{ error "Cannot find samples name in config file" }
    .set { sample_names }

Channel
    .from(params.input.collect {k , v -> v.replicate})
    .ifEmpty{ error "Cannot find replicates name in config file" }
    .set { replicate_names }

Channel
    .from(params.input.collect {k , v -> v.min_fp_size})
    .ifEmpty{ error "Cannot find min_fp_size in config file" }
    .set { min_fp_size }

Channel
    .from(params.input.collect {k , v -> v.max_fp_size})
    .ifEmpty{ error "Cannot find max_fp_size in config file" }
    .set { max_fp_size }

Channel
    .fromPath( "${params.annotation.base}/${params.annotation.bed}" )
    .ifEmpty{ error "annotation_bed not found in yml file"}
    .set { annotation_file }

Channel
    .fromPath( "${params.annotation.base}/${params.annotation.size}" )
    .ifEmpty{ error "annotation_size not found in yml file"}
    .set { annotation_size }

Channel
    .fromPath( "${params.fasta.base}/${params.fasta.file}" )
    .ifEmpty{ error "fasta file not found in yml file"}
    .set { fasta_file }

Channel
    .from( params.stats.analysis.withIndex().collect {it,i -> [i,it]} )
    .join ( 
        Channel
            .from (params.stats.ctrl.withIndex().collect {it,i -> [i,it]} )
    )
    .map { it -> [it[1], it[2]] }
    .combine (
      Channel
        .from ( params.stats.level )
    )
    .combine (
      Channel
        .from ( params.stats.peak_size )
    )
    .ifEmpty{ error "no instruction for statistical analysis in yml file"}
    .set { stat_analysis }

Channel
  .from ( params.ctrl )
  .ifEmpty { error "The control sample is not defined in yml file" }
  .set { ctrl_sample }

Channel
  .from ( params.stats.analysis )
  .ifEmpty { error "The parameters stats.analysis is not defined in yml file" }
  .set { analysis_chan }


/*
 ****************************************************************
                          Workflow
 ****************************************************************
*/


workflow {
  bam_to_bigwig(bam_files, bam_idx, min_fp_size, 
                max_fp_size, sample_names, replicate_names)
  peak_caller(bam_to_bigwig.out.bw, annotation_file.collect())
  slop_process(peak_caller.out.peak, annotation_size.collect())
  if (params.filter.merge) {
      bedtools_merge(slop_process.out.peak.groupTuple(by: [1, 3]))
      bedtools_merge.out.peak.set { slop_done }
  } else {
    slop_process.out.peak.set { slop_done }   
  }
  peak_composition(slop_done, annotation_file.collect(), 
                   fasta_file.collect())
  merge_codon_table(peak_composition.out.count.collect())
  if (!params.stats.random) {
    codon_statistics(stat_analysis, merge_codon_table.out.count.collect())
  } else {
      random_ctrl_statistics(merge_codon_table.out.count.collect(), 
                             annotation_file.collect(), 
                             fasta_file.collect())
  }
  (ch1, ch2) = combine_channels(slop_done, 
                                slop_done,
                                slop_done,
                                slop_done)
  get_common_peaks(ch1, ch2)
  merge_stats(get_common_peaks.out.stat.collect())
  peak_stat_location(analysis_chan, ctrl_sample, merge_stats.out.stat.collect())
}
