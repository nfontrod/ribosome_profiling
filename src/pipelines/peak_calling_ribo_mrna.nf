nextflow.enable.dsl=2

/* Peak calling pipeline :
 * Pipeline that check the codon/encoded amino acids in peaks obtained from 
* ribosome profiling experiment
*/


/*
 ****************************************************************
                          Imports
 ****************************************************************
*/

peak_calling_mod = './nf_modules/peak_calling_ribo_rna.nf'

include { merge_codon_table;
          codon_statistics } from peak_calling_mod 

include { bam_to_bigwig;
          merge_stats;  } from peak_calling_mod addParams(merge_replicate: params.peaks_filter.merge_replicate)

include { peak_caller } from peak_calling_mod \
                             addParams(coverage_threshold: params.peaks_filter.coverage_threshold,
                                       majority: params.peaks_filter.majority)


include { peak_composition } from peak_calling_mod \
                                   addParams(min_ribo: params.read_size.min_ribo,
                                             max_ribo: params.read_size.max_ribo)


if (params.slop.slop_type == "bedtools") {
    include { bedtools_slop as slop_process
            } from peak_calling_mod addParams(threshold: params.slop.threshold,
                                              slop_size: params.slop.slop_size)
} else {
    include { my_slop as slop_process
            } from peak_calling_mod addParams(threshold: params.slop.threshold,
                                              slop_size: params.slop.slop_size)
}



// updating parameters
bam_list = params.ribo.collect {k , v -> "${params.bam_folder}/${v.bam}"}
           .plus(params.rna.collect {k , v -> "${params.bam_folder}/${v.bam}"})


/*
 ****************************************************************
                  Channel definitions
 ****************************************************************
*/


Channel
    .fromPath(bam_list)
    .ifEmpty{ error "Cannot find bam in ${params.bam_folder}" }
    .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
    .set { bam_files }

Channel
    .fromPath(bam_list.collect {v -> "${v}.bai"})
    .ifEmpty{ error "Cannot find min_fp_size in config file" }
    .set { bam_idx }


test = params.ribo.collect {k , v -> v.condition}.unique().findAll { it != params.ctrl }
ctrl = [params.ctrl] * test.size()
test_list = test + ctrl
ctrl_list = ctrl + test
res = [test_list, ctrl_list].transpose()
Channel
    .from(res)
    .set { test_ctrl_names }


Channel
    .from(params.ribo.collect {k , v -> v.condition}
          .plus(params.rna.collect {k , v -> v.condition}))
    .ifEmpty{ error "Cannot find samples name in config file" }
    .set { condition_names }


Channel
    .from(params.ribo.collect {k , v -> v.replicate}
          .plus(params.ribo.collect {k , v -> v.replicate}))
    .ifEmpty{ error "Cannot find replicates name in config file" }
    .set { replicate_names }


Channel
    .from(params.ribo.collect {k , v -> "RIBO"}
          .plus(params.ribo.collect {k , v -> "RNA"}))
    .ifEmpty{ error "Cannot find replicates name in config file" }
    .set { experiment_types }

Channel
    .from(params.ribo.collect {k , v -> params.read_size.min_ribo}
          .plus(params.rna.collect {k , v -> 0}))
    .ifEmpty{ error "Cannot find min_read_size in config file" }
    .set { min_read_size }

Channel
    .from(params.ribo.collect {k , v -> params.read_size.max_ribo}
          .plus(params.rna.collect {k , v -> 0 }))
    .ifEmpty{ error "Cannot find max_read_size in config file" }
    .set { max_read_size }

Channel
    .fromPath( "${params.annotation.base}/${params.annotation.bed}" )
    .ifEmpty{ error "annotation_bed not found in yml file"}
    .set { annotation_file }

Channel
    .fromPath( "${params.annotation.base}/${params.annotation.size}" )
    .ifEmpty{ error "annotation_bed not found in yml file"}
    .set { annotation_size }

Channel
    .fromPath( "${params.fasta.base}/${params.fasta.file}" )
    .ifEmpty{ error "fasta file not found in yml file"}
    .set { fasta_file }

Channel
    .from ( params.stats.level )
    .combine (
      Channel
        .from ( params.stats.peak_size )
    )
    .ifEmpty{ error "no instruction for statistical analysis in yml file"}
    .set { levels }

/*
 ****************************************************************
                          Workflow
 ****************************************************************
*/


workflow {
  bam_to_bigwig(bam_files, bam_idx, experiment_types, min_read_size, 
                max_read_size, condition_names, replicate_names)
  merge_stats(bam_to_bigwig.out.report.collect(), 
              bam_to_bigwig.out.replicate.unique())
  peak_caller(test_ctrl_names.combine(merge_stats.out.design),
              annotation_file.collect(), bam_to_bigwig.out.bw.collect()) 
  slop_process(peak_caller.out.peak, annotation_size.collect())
  peak_composition(slop_process.out.peak, annotation_file.collect(), 
                   fasta_file.collect())
  merge_codon_table(peak_composition.out.count.collect(), 
                    peak_composition.out.conditions.unique())
  merge_chan = levels.combine(merge_codon_table.out.count)
  codon_statistics(merge_chan)
}