nextflow.enable.dsl=2

/* Enrichment analysis pipeline/
 * Pipeline deicated to seek enrichment in codons, encoded amino acids
 * around a ribosome site of interest : (i.e A, E, P sites). This pipeline
 * can also seek for enrichments in codons/amino acids in the full footprints
 * length */


/*
 ****************************************************************
                              Logs
 ****************************************************************
*/


log.info "Folder containing sorted bam: ${params.bam_data.base}."
log.info "File containing offset to p-site: ${params.offset_file}"
log.info "File containing the annotation be file: ${params.bed_file}"
log.info "Ribosome site of interest: ${params.site}"
log.info "Codon(s) around the ribosome site to also consider: ${params.delta}"
log.info "Folder to store the results: ${params.output_folder}"
log.info "Control sample name: ${params.bam_data.ctrl}"
log.info "Feature for wich the enrichment will be performed ${params.stat.level}"


/*
 ****************************************************************
                  Channel definitions
 ****************************************************************
*/


Channel
    .from (params.bam_data.samples.collect {k, v -> 
        [v.sample, v.replicate, v.min_fp_size, v.max_fp_size, 
         file("${params.bam_data.base}/${v.bam}"), 
         file("${params.bam_data.base}/${v.bam}.bai")]})
    .ifEmpty { error "Unable to find the replicate in yml file"}
    .set { bam_files }

Channel
    .fromPath( params.offset_file )
    .ifEmpty { error "Unable to find the file containing offsets to the p-site"}
    .set { offset_file }

Channel
    .fromPath( params.bed_file )
    .ifEmpty { error "Unable to find the bed file"}
    .set { bed_file }

Channel
    .from ( params.stat.level )
    .ifEmpty { error "You must put a level of analysis in the config file" }
    .combine ( params.stat.analysis )
    .set { levels }


/*
 ****************************************************************
                          Imports
 ****************************************************************
*/


include { get_fp_composition; 
          merge_tables; 
          enrichment_analysis } from './nf_modules/enrichment.nf'


/*
 ****************************************************************
                          Workflow
 ****************************************************************
*/


workflow {
    get_fp_composition(bam_files, bed_file.first(), offset_file.first())
    merge_tables(get_fp_composition.out.count.collect())
    enrichment_analysis(levels, merge_tables.out.count.collect())
}
