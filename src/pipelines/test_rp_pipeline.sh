#!/bin/bash

# launch riboview workflow with a tiny dataset for tests using ...

# ... singularity containers

./bin/nextflow src/pipelines/RibosomeProfiling.nf \
  -c src/pipelines/RibosomeProfiling.config \
  -profile singularity \
  --genome "data/tiny_ribosome_profiling_dataset/synth.fasta" \
  --fastq "data/tiny_ribosome_profiling_dataset/fastq/*.fastq" \
  --rrna_fasta "data/tiny_ribosome_profiling_dataset/contaminants.fasta" \
  --skip_trimming true \
  -resume

# ...docker containers

./bin/nextflow src/pipelines/RibosomeProfiling.nf \
  -c src/pipelines/RibosomeProfiling.config \
  -profile docker \
  --genome "data/tiny_ribosome_profiling_dataset/synth.fasta" \
  --fastq "data/tiny_ribosome_profiling_dataset/fastq/*.fastq" \
  --rrna_fasta "data/tiny_ribosome_profiling_dataset/contaminants.fasta" \
  --skip_trimming true \
  -resume