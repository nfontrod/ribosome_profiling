/*
 * Some function used in src/pipelines/peak_calling.nf
*/ 


def select(List val){
    // select only values of interest
    if(val[0] != val[1] && val[0][2] == val[1][2] && val[0][3] == val[1][3]){
        return(val.sort())
    }
}


def merge_channels(chan1, chan2) {
    return chan1.map{ it -> [it] }
             .combine(chan2.map{ it -> [it] })
             .map{ it -> select(it) }
             .unique()
}


def combine_channels(chan1, chan2, chan3, chan4) {
    // combine channels 
    chan5 = merge_channels(chan1, chan2)
    chan6 = merge_channels(chan3, chan4)
    return [chan5.map { it -> it[0]}, chan6.map { it -> it[1]}]
}