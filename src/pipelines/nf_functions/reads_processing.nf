def merge_channels(report1, report2, report3, report4, report5, report6,
                   report7, skip_trimming) {
  if (!skip_trimming) {
    return report1.concat(report2, report3, report4, 
                          report5, report6, report7).collect()
  } else {
    return report1.concat(report2, report4, report5, 
                          report6, report7).collect()
  }
}