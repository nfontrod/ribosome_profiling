/* global parameter */ 

params.output_folder = "peak_calling_result"

/* BigWig creation */

process bam_to_bigwig {
  tag "${file_id}_${fp_size_mini}-${fp_size_maxi}"
  label "multi_thread"
  container = "lbmc/deeptools:3.0.2"
  publishDir "results/${params.output_folder}/01_bigwig/", mode: 'copy'

  input:
    tuple val(file_id), path(bam)
    path idx
    val fp_size_mini
    val fp_size_maxi
    val sp
    val rep

  output:
    tuple val(file_id), val(sp), val(rep), val(fp_size_mini), \
          val(fp_size_maxi), path("*.bw"), emit: bw

  script:
    """
    bamCoverage -p ${task.cpus} -b ${bam} \
    -o ${bam.baseName}_${fp_size_mini}-${fp_size_maxi}.bw \
    -bs 1 --minFragmentLength ${fp_size_mini} \
    --maxFragmentLength ${fp_size_maxi}
    """
}


/* peak calling */

params.coverage = 0

process peak_caller {
  tag "${file_id}_${fp_mini}-${fp_maxi}"
  label "multi_thread"
  container = "lbmc/peak_caller:0.0"
  publishDir "results/${params.output_folder}/02_peaks/", mode: 'copy'

  input:
    tuple val(file_id), val(sp), val(rep), val(fp_mini), val(fp_maxi), path(bw)
    path annotation

  output:
    tuple val(file_id), val(sp), val(rep), val(fp_mini), \
          val(fp_maxi), path("*.bed"), emit: peak

  script:
    """
    python /script/peak_caller.py --bw ${bw} \
    --bed ${annotation} -o ${file_id}_${fp_mini}-${fp_maxi}.bed \
    -p ${task.cpus} -w 1 -c ${params.coverage}
    """
}


/* increase peak size  */

process bedtools_slop {
  tag "${file_id}_${fp_size_mini}-${fp_size_maxi}"
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/03_slop/", mode: 'copy'

  input:
    tuple val(file_id), val(sp), val(rep), val(fp_size_mini), \
          val(fp_size_maxi), path(bed)
    path size

  output:
    tuple val(new_id), val(sp), val(rep), val(interval), \
          path("*merged.bed"), emit: peak

  script:
    interval="${fp_size_mini}-${fp_size_maxi}"
    new_id ="${file_id}_${interval}"
    """
    bedtools slop -i ${bed} -g ${size} \
    -b ${params.slop_size} | \
    awk 'BEGIN{FS="\\t"}{if(\$5 >= ${params.threshold}){print \$0}}' > \
    ${new_id}.enlarged.tmp.bed
    sort -k 1,1 -k2,2n ${new_id}.enlarged.tmp.bed > ${new_id}.enlarged.sort.bed
    bedtools merge -i ${new_id}.enlarged.sort.bed \
    -c 5,6 -o mean,distinct | awk '{print \$1"\\t"\$2"\\t"\$3"\\tpeak_"NR"\\t"\$4"\\t"\$5}' > ${new_id}.merged.bed
    """
}


process my_slop {
  tag "${file_id}_${fp_size_mini}-${fp_size_maxi}"
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/03_slop/", mode: 'copy'

  input:
    tuple val(file_id), val(sp), val(rep), val(fp_size_mini), \
          val(fp_size_maxi), path(bed)
    path size

  output:
    tuple val(new_id), val(sp), val(rep), val(interval), \
          path("*.merged.bed"), emit: peak

  script:
    interval="${fp_size_mini}-${fp_size_maxi}"
    new_id ="${file_id}_${interval}"
    """
    python3 /script/my_slop.py -b ${bed} -s ${params.slop_size} -g ${size} \
    -o ${new_id}_tmp_merged.bed -R False -r False
    awk 'BEGIN{FS="\\t"}{if(\$5 >= ${params.threshold}){print \$0}}' ${new_id}_tmp_merged.bed > \
    ${new_id}.merged.bed
    """
}


process bedtools_merge {
  tag "${sp}"
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/03.5_merge/", mode: 'copy'

  input:
    tuple val(file_ids), val(sp), val(replicates), val(interval), path(beds)
  
  output:
    tuple val(new_id), val(sp), val("RepMerged"), val(interval), path("*.rep_merged.bed"), emit: peak

  script:
    new_id = "${sp}"
    """
    cat *.bed | sort -k1,1 -k2,2n > tmp.bed
    bedtools merge -i tmp.bed \
    -c 1,5,6 -o count,mean,distinct | awk '{if(\$4 >= 2){print \$1"\\t"\$2"\\t"\$3"\\tpeak_"NR"\\t"\$5"\\t"\$6}}' > ${sp}.rep_merged.bed
    """
}

/* get codons in peaks */

process peak_composition {
  tag "${new_id}"
  label "mono_thread"
  container = "lbmc/peak_composition:0.0"
  publishDir "results/${params.output_folder}/03_peaks_composition/", mode: 'copy'

  input:
    tuple val(new_id), val(sp), val(rep), val(interval), path(peak) 
    path annotation 
    path fasta

  output:
    path "*count.txt", emit: count

  script:
    interl = interval.split("-")
    fp_mini = interl[0]
    fp_maxi = interl[1]
    """
    python3 /script/peak_composition.py -p ${peak} \
    -a ${annotation} -g ${fasta} -s ${sp} -r ${rep} \
    --min_fp_size ${fp_mini} --max_fp_size ${fp_maxi} \
    -o ${new_id}_count.txt > stdin.txt
    """
}


/* merge codon composition */

process merge_codon_table {
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/${params.out}", mode: 'copy'
  
  input:
    path count
  
  output:
    path "full_count_recap.txt", emit: count
  
  script:
    """
    cat *count.txt | head -n 1 > full_count_recap.txt
    cat *count.txt | grep -v "count" >> full_count_recap.txt
    """
} 


/* statistical analysis codon */

process codon_statistics {
  tag "${analysis}_${level}"
  label "multi_thread"
  container = "lbmc/peak_stat:0.0"
  publishDir "results/${params.output_folder}/04_codon_statistics/", mode: 'copy'
  
  input:
    tuple val(analysis), val(ctrl), val(level), val(peak_size)
    path count
  
  output:
    path "*.tar.gz", emit: result
  
  script:
    if (peak_size.size() == 2) {
      name_peak = "${peak_size[0]}-${peak_size[1]}"
      peak_size = "${peak_size[0]} ${peak_size[1]}"
    } else {
      name_peak = peak_size[0] 
      peak_size = peak_size[0]
    }
    """
    python3 -m script -t ${count} -a ${analysis} -ps ${task.cpus} \
    -l ${level} -c ${ctrl} -P ${peak_size}

    tar -c --use-compress-program="pigz -p ${task.cpus} " \
    -cf analysis_${analysis}_${level}_ps${name_peak}.tar.gz \
    analysis_${analysis}_${level}_ps${name_peak}
    """
} 


/* codon statistique by sampling random control peaks */
params.iteration = 10000
params.colors = "blue red"
process random_ctrl_statistics {
  label "multi_thread"
  container = "lbmc/peak_composition:0.0"
  publishDir "results/${params.output_folder}/04_codon_statistics/", mode: 'copy'
  
  input:
    path count
    path annotation
    path fasta_file
  
  output:
    path "control_permutation_stat_table*.{pdf,txt}", emit: results
  
  script:
    """
    python3 /script/create_random_peaks.py -p ${count} -a ${annotation} \
    -g ${fasta_file} -i ${params.iteration} \
    -o control_permutation_stat_table.txt --ps ${task.cpus} --colors ${params.colors}
    """
} 


/* find common peaks */

process get_common_peaks {
  tag "${file_id1}:${file_id2}"
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/05_stats/", mode: 'copy'

  input:
    tuple val(file_id1), val(sp1), val(rep1), val(in1), path(bed1)
    tuple val(file_id2), val(sp2), val(rep2), val(in2), path(bed2)

  output:
    path "*stats.txt", emit: stat

  script:
    new_id = "${file_id1}_n_${file_id2}"
    """
    bedtools intersect -a ${bed1} -b ${bed2} -wa -u > ${file_id1}_intersect.bed
    bedtools intersect -a ${bed1} -b ${bed2} -wb -u > ${file_id2}_intersect.bed
    u1=\$(wc -l ${file_id1}_intersect.bed | cut -f1 -d' ')
    u2=\$(wc -l ${file_id2}_intersect.bed | cut -f1 -d' ')
    let s1=\$(wc -l ${bed1} | cut -f1 -d' ' )-\$u1
    let s2=\$(wc -l ${bed2} | cut -f1 -d' ' )-\$u2
    echo -e "${sp1}\t${sp2}\t${rep1}\t${rep2}\t${in1}\t${in2}\t\$s1\t\$s2\t\$u1\t\$u2" \
    > ${new_id}_stats.txt
    """
}

/* merge stats */

process merge_stats {
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/06_stat_numbers", mode: 'copy'
  
  input:
    path stat 

  output:
    path "*stats_report.txt", emit: stat

  script:
    """
    echo -e "sample1\tsample2\trep1\trep2\tsize1\tsize2\tspecific1\tspecific2\tcommon1\tcommon2" > stats_report.txt \
    && cat *stats.txt >> stats_report.txt
    """
}


/* peak location analysis */

process peak_stat_location {
  label "mono_thread"  
  container = "lbmc/peak_stat:0.0"
  publishDir "results/${params.output_folder}/07_stat_peak_location"

  input:
    val analysis
    val ctrl
    path report
  
  output:
    path "*.tar.gz", emit: stat
  
  script:
    """
    python3 -m script.common_peaks -t ${report} -c ${ctrl} \
    -a ${analysis} > output.txt  

    if [ -d "analysis_${analysis}" ]; then
      tar -czvf analysis_${analysis}.tar.gz analysis_${analysis}
    else
      tar -czvf output.tar.gz output.txt
    fi
    """
}