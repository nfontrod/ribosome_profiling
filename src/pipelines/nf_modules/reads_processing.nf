/* global parameter */

params.folder = "project"


/* Trimming */

params.cutadapt = ""


process adaptor_removal {
  tag "$file_id"
  label "multi_thread"
  container = "lbmc/cutadapt:2.1"
  publishDir "results/${params.folder}/02_adaptor_removal/", mode: 'copy'

  input:
    tuple val(file_id), path(fastq_raw)

  output:
    tuple val(file_id), path("*_cut*.fastq.gz"), emit: fastq
    path "*.txt", emit: report

  script:
  if (file_id instanceof List){
    file_prefix = file_id[0]
  } else {
    file_prefix = file_id
  }
  if (fastq_raw.size() == 2)
    """
    cutadapt ${params.cutadapt} -j ${task.cpus} \
    -o ${file_prefix}_cut_1.fastq.gz -p ${file_prefix}_cut_2.fastq.gz \
    ${fastq_raw[0]}  ${fastq_raw[1]} > ${file_prefix}_report.txt
    """
  else
    """
    cutadapt ${params.cutadapt} -j ${task.cpus} \
    -o ${file_prefix}_cut.fastq.gz \
    ${fastq_raw} > ${file_prefix}_report.txt
    """
}

/* quality trimming */

params.urqt = ""


process quality_trimming {
  tag "$file_id"
  label "multi_thread"
  container = "lbmc/urqt:d62c1f8"
  publishDir "results/${params.folder}/03_quality_trimming/", mode: 'copy'

  input:
    tuple val(file_id) , path(fastq_cut)

  output:
    tuple val(file_id), path("*_trim*.fastq.gz"), emit: fastq

  script:
  if (file_id instanceof List){
    file_prefix = file_id[0]
  } else {
    file_prefix = file_id
  }
  if (fastq_cut.size() == 2)
    """
    UrQt ${params.urqt} --m ${task.cpus} --gz --in ${fastq_cut[0]} \
    --inpair ${fastq_cut[1]} --out ${file_prefix}_trim_1.fastq.gz \
    --outpair ${file_prefix}_trim_2.fastq.gz
    """
  else
    """
    UrQt ${params.urqt} --m ${task.cpus} --gz --in ${fastq_cut} \
    --out ${file_prefix}_trim.fastq.gz
    """
}


/* building contaminant index */

process rrna_indexing_se {
  tag "$contaminant.baseName"
  label "multi_thread"
  container = "lbmc/bowtie:1.2.2"
  publishDir "results/${params.folder}/03_indexing_rrna/", mode: 'copy'

  input:
    path contaminant

  output:
    path "*.index*", emit: index
    path "*_report.txt", emit: report

  script:
    """
    bowtie-build --threads ${task.cpus} -f ${contaminant} \
    ${contaminant.baseName}.index &> ${contaminant.baseName}_bowtie_report.txt

    if grep -q "Error" ${contaminant.baseName}_bowtie_report.txt; then
    exit 1
    fi
    """
}


/* building contaminant index */

process rrna_indexing_pe {
  tag "$contaminant.baseName"
  label "multi_thread"
  container = "lbmc/bowtie2:2.3.4.1"
  publishDir "results/${params.folder}/03_indexing_rrna/", mode: 'symlink'

  input:
    path contaminant

  output:
    path "*.index*", emit: index
    path "*_report.txt", emit: report

  script:
    """
    bowtie2-build --threads ${task.cpus} ${contaminant} \
    ${contaminant.baseName}.index &> ${contaminant.baseName}_bowtie_report.txt

    if grep -q "Error" ${contaminant.baseName}_bowtie_report.txt; then
    exit 1
    fi
    """
}


/* rRNA and tRNA filtering */

params.bowtie_seedlen = 20

process rRNA_removal_se {
  tag "$file_id"
  label "multi_thread"
  container = "lbmc/bowtie:1.2.2"
  publishDir "results/${params.folder}/04_rRNA_removal/", mode: 'copy'

  input:
    tuple val(file_id), path(reads)
    path index

  output:
    tuple val(file_id), path("*no_conta*.fastq.gz"), emit: fastq
    tuple val(file_id), path("*only_conta*.fastq.gz"), emit: conta
    path "*report.txt", emit: report

  script:
    if (file_id instanceof List){
      file_prefix = file_id[0]
    } else {
      file_prefix = file_id
    }
    index_id = index[0]
    for (index_file in index) {
        if (index_file =~ /.*\.1\.ebwt/ && !(index_file =~ /.*\.rev\.1\.ebwt/)) {
            index_id = ( index_file =~ /(.*)\.1\.ebwt/)[0][1]
        }
    }
    """
    zcat ${reads} > ${file_prefix}_tmp.fq
    bowtie -v 2 --seedlen ${params.bowtie_seedlen} --best \
    --tryhard --norc --phred33-quals \
    --sam -p ${task.cpus} ${index_id} \
    -q ${file_prefix}_tmp.fq \
    --al ${file_prefix}_only_conta.fastq --un ${file_prefix}_no_conta.fastq 2> \
    ${file_prefix}_bowtie_report_tmp.txt 

    gzip ${file_prefix}_only_conta.fastq
    gzip ${file_prefix}_no_conta.fastq

    if grep -q "Error" ${file_prefix}_bowtie_report_tmp.txt; then
    exit 1
    fi
    tail -n 19 ${file_prefix}_bowtie_report_tmp.txt > ${file_prefix}_bowtie_report.txt
    """
}


/* rRNA and tRNA filtering paired-end*/


process rRNA_removal_pe {
  tag "$pair_id"
  label "multi_thread"
  container = "lbmc/bowtie2:2.3.4.1"
  publishDir "results/${params.folder}/04_rRNA_removal/", mode: 'symlink'

  input:
    tuple val(pair_id), file(reads)
    path index

  output:
    tuple val(pair_id), path("*_noconta_{1,2}.fastq.gz"), emit: fastq
    tuple val(pair_id), path("*_conta_{1,2}.fastq.gz"), emit: conta
    path "*report.txt", emit: report

  script:
    if (pair_id instanceof List){
      file_prefix = pair_id[0]
    } else {
      file_prefix = pair_id
    }
    index_id = index[0]
    for (index_file in index) {
        if (index_file =~ /.*\.1\.bt2/ && !(index_file =~ /.*\.rev\.1\.bt2/)) {
            index_id = ( index_file =~ /(.*)\.1\.bt2/)[0][1]
        }
    }
    """
    bowtie2 --very-sensitive --phred33 \
    -p ${task.cpus} -x ${index_id} \
    -1 ${reads[0]} -2 ${reads[1]} \
    --al-conc-gz ${file_prefix}_conta.fastq.gz \
    --un-conc-gz ${file_prefix}_noconta.fastq.gz \
     2> ${file_prefix}_bowtie_report_tmp.txt 

    mv ${file_prefix}_conta.fastq.1.gz ${file_prefix}_conta_1.fastq.gz
    mv ${file_prefix}_conta.fastq.2.gz ${file_prefix}_conta_2.fastq.gz
    mv ${file_prefix}_noconta.fastq.1.gz ${file_prefix}_noconta_1.fastq.gz
    mv ${file_prefix}_noconta.fastq.2.gz ${file_prefix}_noconta_2.fastq.gz
    if grep -q "Error" ${file_prefix}_bowtie_report_tmp.txt; then
    exit 1
    fi
    tail -n 19 ${file_prefix}_bowtie_report_tmp.txt > ${file_prefix}_bowtie_report.txt
    """
}


/* indexing reference genome */

process index_genome {
  tag "$fasta.baseName"
  label "multi_thread"
  container = "lbmc/hisat2:2.1.0"
  publishDir "results/06_indexed_genome/", mode: 'copy'

  input:
    path fasta

  output:
    path "*.index*", emit: index

  script:
    """
    hisat2-build -p ${task.cpus} ${fasta} ${fasta.baseName}.index
    """
}


/*	mapping against human genome with hisat2 */

params.hisat2 = ""

process genome_mapping {
  tag "$file_id"
  label "multi_thread"
  container = "lbmc/hisat2:2.1.0"
  publishDir "results/${params.folder}/07_hisat2/", mode: 'copy'

  input:
    tuple val(file_id), file(fastq_filtred)
    path index

  output:
    tuple val(file_id), path("*notaligned*.fastq.gz"), emit: unaligned
    tuple val(file_id), path("*_aligned*.bam"), emit: aligned
    path "*.txt", emit: report

  script:
    if (file_id instanceof List){
      file_prefix = file_id[0]
    } else {
      file_prefix = file_id
    }
    index_id = index[0]
    for (index_file in index) {
        if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
            index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
        }
    }
    if (fastq_filtred.size() == 2)
      """
      hisat2 -x ${index_id} -p ${task.cpus} \
      -1 ${fastq_filtred[0]} -2  ${fastq_filtred[1]} \
      --un-conc-gz ${file_prefix}_notaligned.fastq.gz \
      ${params.hisat2} \
      2> ${file_prefix}_hisat2_report.txt | samtools view -bS -F 4 -o ${file_prefix}.bam
      samtools view -h ${file_prefix}.bam | egrep "(NH:i:1|^@)" | samtools view -Sb \
      -o ${file_prefix}_aligned.bam

      mv ${file_prefix}_notaligned.fastq.1.gz ${file_prefix}_notaligned_1.fastq.gz
      mv ${file_prefix}_notaligned.fastq.2.gz ${file_prefix}_notaligned_2.fastq.gz

      if grep -q "Error" ${file_prefix}_hisat2_report.txt; then
      exit 1
      fi
      """
    else
      """
      hisat2 -x ${index_id} -p ${task.cpus} \
      -U ${fastq_filtred} --un-gz ${file_prefix}_notaligned.fastq.gz \
      --rna-strandness 'F' --norc \
      2> ${file_prefix}_hisat2_report.txt | samtools view -bS -F 4 -o ${file_prefix}.bam
      samtools view -h ${file_prefix}.bam | egrep "(NH:i:1|^@)" | samtools view -Sb \
      -o ${file_prefix}_aligned.bam

      if grep -q "Error" ${file_prefix}_hisat2_report.txt; then
      exit 1
      fi
      """
}


/* get fastq without multimapped reads */


process bamtofastq {
  tag "$file_id"
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.folder}/07_hisat2/", mode: 'copy'

  input:
    tuple val(file_id), path(bam)

  output:
    tuple val(file_id), path("*_aligned*.fastq.gz"), emit: fastq

  script:
  if (file_id instanceof List){
    file_prefix = file_id[0]
  } else {
    file_prefix = file_id
  }
  if (params.paired_end == true) {
  """
  bedtools bamtofastq -i ${bam} \
  -fq ${file_prefix}_aligned_1.fastq -fq2 ${file_prefix}_aligned_2.fastq
  gzip ${file_prefix}_aligned_1.fastq
  gzip ${file_prefix}_aligned_2.fastq
  """
  } else {
  """
  bedtools bamtofastq -i ${bam} -fq ${file_prefix}_aligned.fastq
  gzip ${file_prefix}_aligned.fastq
  """
  }
}


/*                   sorting                             */

process index_bam {
  tag "$file_id"
  label "mono_thread"
  container = "lbmc/samtools:1.7"
  publishDir "results/${params.folder}/08_hisat2_sorted/", mode: 'copy'

  input:
    tuple val(file_id), path(bam)

  output:
    tuple val(file_id), path("*_sorted.{bam,bam.bai}"), emit: bam

  script:
    if (file_id instanceof List){
      file_prefix = file_id[0]
    } else {
      file_prefix = file_id
    }
    """
    samtools sort -@ ${task.cpus} -O BAM -o ${file_prefix}_sorted.bam ${bam}
    samtools index ${file_prefix}_sorted.bam
    """
}

