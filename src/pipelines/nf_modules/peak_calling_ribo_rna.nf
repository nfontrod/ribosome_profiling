/* global parameter */ 

params.output_folder = "peak_calling_rib_rna"

/* BigWig creation */

process bam_to_bigwig {
  tag "${file_id}_${fp_size_mini}-${fp_size_maxi}"
  label "multi_thread"
  container = "lbmc/deeptools:3.0.2"
  publishDir "results/${params.output_folder}/01_bigwig/", mode: 'copy'

  input:
    tuple val(file_id), path(bam)
    path idx
    val experiment
    val fp_size_mini
    val fp_size_maxi
    val condition
    val rep

  output:
    path "*.bw", emit: bw
    path "report*.txt", emit: report
    val my_rep, emit: replicate

  script:
    interval = "${fp_size_mini}-${fp_size_maxi}"
    if (params.merge_replicate) {
      my_rep = "RepMean"
    } else {
      my_rep = rep
    }
    """
    bamCoverage -p ${task.cpus} -b ${bam} \
    -o ${bam.baseName}_${fp_size_mini}-${fp_size_maxi}.bw \
    -bs 1 --normalizeUsing CPM --minFragmentLength ${fp_size_mini} \
    --maxFragmentLength ${fp_size_maxi}

    echo -e "${bam.baseName}_${interval}.bw\t${condition}\t${experiment}\t${rep}\t${interval}" > report_${file_id}.txt
    """
}


/* merge stats */

process merge_stats {
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/02_design/", mode: 'copy'
  
  input:
    path report
    val rep 

  output:
    tuple val(rep), path("design*.txt"), emit: design

  script:
      if (params.merge_replicate) {
        """
        echo -e "bw\tconditions\texperiments\treplicates\tread_size" > design_${rep}.txt \
        && cat report*.txt >> design_${rep}.txt
        """
      } else {
        """
        echo -e "bw\tconditions\texperiments\treplicates\tread_size" > design_${rep}.txt \
        && cat report*.txt | grep ${rep} >> design_${rep}.txt
        """
      }
}


/* peak calling */

params.ctrl = "ctrl"

process peak_caller {
  tag "${test}_vs_${ctrl}_${rep}"
  label "multi_thread"
  container = "lbmc/fp_peak_caller_ribo_rna:0.0"
  publishDir "results/${params.output_folder}/03_peaks/", mode: 'copy'

  input:
    tuple val(test), val(ctrl), val(rep), path(design)
    path annotation
    path bw

  output:
    tuple val(file_id), val(test), val(ctrl), val(rep), path("*.bed"), emit: peak

  script:
    file_id = "${test}_vs_${ctrl}"
    """
    python /script/peak_caller.py --design ${design} \
    --ctrl ${ctrl} --test ${test} --annotation ${annotation} \
    -o ${file_id}_${rep}.bed \
    -p ${task.cpus} --window 1 --write_bw False \
    --coverage_threshold ${params.coverage_threshold} \
    --min_majority ${params.majority}
    """
}


/* increase peak size  */

process bedtools_slop {
  tag "${file_id}"
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/04_slop/", mode: 'copy'

  input:
    tuple val(file_id), val(test), val(ctrl), val(rep), path(bed)
    path size

  output:
    tuple val(file_id), val(rep), path("*merged.bed"), emit: peak

  script:
    """
    bedtools slop -i ${bed} -g ${size} \
    -b ${params.slop_size} | \
    awk 'BEGIN{FS="\\t"}{if(\$5 >= ${params.threshold}){print \$0}}' > \
    ${file_id}.enlarged.tmp.bed
    sort -k 1,1 -k2,2n ${file_id}.enlarged.tmp.bed > ${file_id}.enlarged.sort.bed
    bedtools merge -i ${file_id}.enlarged.sort.bed \
    -d 6 -c 5,6 -o mean,distinct | awk '{print \$1"\\t"\$2"\\t"\$3"\\tpeak_"NR"\\t"\$4"\\t"\$5}' > ${file_id}_${rep}.merged.bed 
    """
}


/* increase peak size  */

process my_slop {
  tag "${file_id}"
  label "mono_thread"
  container = "lbmc/my_slop:0.0"
  publishDir "results/${params.output_folder}/04_slop/", mode: 'copy'

  input:
    tuple val(file_id), val(test), val(ctrl), val(rep), path(bed)
    path size

  output:
    tuple val(file_id), val(rep), path("*.merged.bed"), emit: peak

  script:
    """
    python3 /script/my_slop.py -b ${bed} -s ${params.slop_size} -g ${size} \
    -o ${file_id}_tmp_merged.bed -R False -r False
    awk 'BEGIN{FS="\\t"}{if(\$5 >= ${params.threshold}){print \$0}}' ${file_id}_tmp_merged.bed > \
    ${file_id}_${rep}.merged.bed
    """
}

/* get codons in peaks */

params.min_ribo = 0
params.max_ribo = 50
process peak_composition {
  tag "${new_id}"
  label "mono_thread"
  container = "lbmc/peak_composition:0.0"
  publishDir "results/${params.output_folder}/05_peaks_composition/", mode: 'copy'

  input:
    tuple val(new_id), val(rep), path(peak) 
    path annotation 
    path fasta

  output:
    path "*count.txt", emit: count
    tuple val(test), val(ctrl), emit: conditions

  script:
    val = new_id.split("_vs_")
    if ( val[0] != params.ctrl ) {
      test = new_id
      ctrl = "${val[1]}_vs_${val[0]}"
    } else {
      test = "${val[1]}_vs_${val[0]}"
      ctrl = new_id
    }
    """
    python3 /script/peak_composition.py -p ${peak} \
    -a ${annotation} -g ${fasta} -s ${new_id} -r ${rep} \
    --min_fp_size ${params.min_ribo} --max_fp_size ${params.max_ribo} \
    -o ${new_id}_${rep}_count.txt > stdin.txt
    """
}


process merge_codon_table {
  label "mono_thread"
  container = "lbmc/bedtools:2.25.0"
  publishDir "results/${params.output_folder}/06_merged_peaks_composition/", mode: 'copy'
  
  input:
    path count
    tuple val(test), val(ctrl)
  
  output:
    tuple val(ctrl), path("${test}_count_recap.txt"), emit: count
  
  script:
    """
    cat *count.txt | head -n 1 > ${test}_count_recap.txt
    cat *count.txt | grep -v "count" | grep -P "${test}|${ctrl}" >> ${test}_count_recap.txt
    """
} 


/* statistical analysis codon */

process codon_statistics {
  tag "sample_${level}"
  label "multi_thread"
  container = "lbmc/peak_stat:0.0"
  publishDir "results/${params.output_folder}/07_codon_statistics/", mode: 'copy'
  
  input:
    tuple val(level), val(peak_size), val(ctrl), path(count)
  
  output:
    path "*.tar.gz", emit: result
  
  script:
    analysis = "sample"
    if (peak_size.size() == 2) {
      name_peak = "${peak_size[0]}-${peak_size[1]}"
      peak_size = "${peak_size[0]} ${peak_size[1]}"
    } else {
      name_peak = peak_size[0] 
      peak_size = peak_size[0]
    }
    """
    python3 -m script -t ${count} -a ${analysis} -ps ${task.cpus} \
    -l ${level} -c ${ctrl} -P ${peak_size}

    tar -c --use-compress-program="pigz -p ${task.cpus} " \
    -cf analysis_${analysis}_${level}_${ctrl}_ps${name_peak}.tar.gz \
    analysis_${analysis}_${level}_ps${name_peak}
    """
} 
