/* global parameter */

params.folder = "project"
params.out = "fastqc"


/* Fastqc */

process fastqc {
  tag "$file_id"
  label "mono_thread"
  container = "lbmc/fastqc:0.11.5"
  publishDir "results/${params.folder}/${params.out}/", mode: 'copy'

  input:
    tuple val(file_id), file(reads)

  output:
    path "*.{zip,html}", emit: report

  script:
    """
    fastqc --quiet --threads ${task.cpus} --format fastq --outdir ./ ${reads}
    """
}

/* multiQC */

process multiqc {
  label "mono_thread"
  container = "ewels/multiqc:1.9"
  publishDir "results/${params.folder}/10_multiqc/", mode: 'copy'

  input:
    path reports

  output:
    path "multiqc_report.html", emit: report

  script:
    """
    multiqc . --interactive
    """
}