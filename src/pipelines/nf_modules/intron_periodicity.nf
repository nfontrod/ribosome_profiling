/* global parameters */

params.folder = "intron_periodicity"


/* creation of the intron annotation file */

process intron_bed {
    label "mono_thread"

    input:
        path bed_file
    
    output:
        path "intron.bed", emit: bed

    script:
        """
        python3 /script/create_intron_file.py -a ${bed_file} \
        -o '.' -n 'intron.bed'
        """
}


/* creation of periodicity tables */

process periodicity_table {
    tag "${file_id}"
    label "mono_thread"

    input:
        tuple val(file_id), path(bam), path(idx)
        path offset
        path bed
        path ibed
    
    output:
        path "sum*.txt", emit: position
    
    script:
        if ( offset.toString() == "void") {
            offset = "\'\'"
        }
        """
        python3 /script/intron_periodicity_table.py -b ${bam} -be ${ibed} \
        -B ${bed} -o ${offset} -w ${params.figure_parameters.wb} \
        -W  ${params.figure_parameters.wa}
        """
}


/* creating figures */ 

process figures {
    label "mono_thread"
    publishDir "results/${params.folder}/", mode: 'copy'

    input:
        tuple val(size_min), val(size_max), val(samples), val(normalize)
        path table
    
    output:
        path "*.pdf"
    
    script:
        if(samples instanceof List){
            samples = samples.toString()
                .replace(',', '')
                .replace('[', '')
                .replace(']', '')
        }
        """
        python3 /script/intron_periodicity_figure.py -o all -t ${table} \
        --size ${size_min} ${size_max} -s ${samples} -n ${normalize}
        """
}

