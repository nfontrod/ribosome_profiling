/* global parameter */

params.folder = "periodicity"


/* creation of periodicity tables */

process periodicity_table {
    tag "${file_id}"
    label "mono_thread"

    input:
        tuple val(file_id), path(bam), path(idx)
        path bed
    
    output:
        path "sum*.txt", emit: position
        path "offset*.txt", emit: offset
    
    script:
        """
        python3 /script/periodicity_table.py -b ${bam} -a ${bed} -w 80
        """
}


/* creation of periodicity tables with offset parameter*/

process periodicity_table_psite {
    tag "${file_id}"
    label "mono_thread"

    input:
        tuple val(file_id), path(bam), path(idx)
        path bed
        path offset
    
    output:
        path "sum*.txt", emit: position
    
    script:
        """
        python3 /script/periodicity_table.py -b ${bam} -a ${bed} -w 80 \
        --offset ${offset}
        """
}


/* combining periodicity tables */

process merge_tables {
    label "mono_thread"
    publishDir "results/${params.folder}/", mode: 'copy'

    input:
        path position
        path offset
    
    output:
        path "full_periodicity_table.txt", emit: position
        path "full_offset_table.txt", emit: offset
    
    script:
        """
        cat sum*.txt | head -n 1 > full_periodicity_table.txt
        cat sum*.txt | grep -v "fp_size" >> full_periodicity_table.txt

        cat offset*.txt | head -n 1 > full_offset_table.txt
        cat offset*.txt | grep -v "fp_size" >> full_offset_table.txt
        """
}

process merge_tables_psite {
    label "mono_thread"
    publishDir "results/${params.folder}/", mode: 'copy'

    input:
        path position
    
    output:
        path "full_periodicity_table_psite.txt", emit: position
    
    script:
        """
        cat sum*.txt | head -n 1 > full_periodicity_table_psite.txt
        cat sum*.txt | grep -v "fp_size" >> full_periodicity_table_psite.txt
        """
}
 


/* creating a general offset table accross all samples */

process merge_offset {
    label "mono_thread"
    publishDir "results/${params.folder}/", mode: 'copy'

    input: 
        path offset
    
    output:
        path "general_offset_table.txt", emit: offset
    
    script:
        """
        python3 /script/merge_sample_offset.py -o ${offset}
        """
}


/* creating periodicity_figures */

process figures {
    label "mono_thread"
    publishDir "results/${params.folder}/", mode: 'copy'

    input:
        tuple val(codon), val(size_min), val(size_max), val(samples), val(normalize)
        path table
    
    output:
        path "*.pdf", emit: figure
    
    script:
        if(samples instanceof List){
            samples = samples.toString()
                .replace(',', '')
                .replace('[', '')
                .replace(']', '')
        }
        """
        python3 /script/periodicity_figure.py -t ${table}  -c ${codon} \
        --size ${size_min} ${size_max} -s ${samples} -n ${normalize}
        """
}