/* global parameter */

params.output_folder = "ribosite_analysis"


/* create a codon table with the codon counts around 
  *the ribosome site of interest 
*/

process get_fp_composition {
    tag "${file_id}"
    label 'mono_thread'

    input:
        tuple val(file_id), val(replicate), val(min_s), val(max_s), \
              path(bam), path(idx)
        path bed 
        path offset
    
    output:
        path 'codon_count*.txt', emit: count
    
    script:
        """
        python3 /script/ribosite_finder.py --bam ${bam} --site ${params.site} \
        --sample_name ${file_id} --bed ${bed} --offset_file ${offset} \
        --replicate ${replicate} --delta ${params.delta} --min_fp_size ${min_s} \
        --max_fp_size ${max_s}
        """
}


/* Merging the tables */

process merge_tables {
    label 'mono_thread'
    publishDir "results/${params.output_folder}/01_count_table/", mode: 'copy'

    input:
        path table
    
    output:
        path "*count_table.txt", emit: count
    
    script:
        """
        cat codon_count*.txt | head -n 1 > ${params.site}_${params.delta}_count_table.txt
        cat codon_count*.txt | grep -v "sample" >> ${params.site}_${params.delta}_count_table.txt
        """
}


/* Enrichment analysis */

process enrichment_analysis {
  tag "${level}"
  label 'multi_thread'
  publishDir "results/${params.output_folder}/02_enrichment_analysis/", mode: 'copy'
  
  input:
    tuple val(level), val(analysis)
    path count
  
  output:
    path "*.tar.gz", emit: result
  
  script:
    """
    python3 -m script -t ${count} -P ${task.cpus} \
    -l ${level} -c ${params.bam_data.ctrl} -p n -s ${params.site} -d ${params.delta}

    tar -c --use-compress-program="pigz -p ${task.cpus} " \
    -cf analysis_${analysis}_${level}_${params.site}d${params.delta}.tar.gz \
    analysis_${analysis}_${level}
    """
} 