nextflow.enable.dsl=2
/*
*	RibosomeProfiling Analysis pipeline
* Adapted from the pipeline created by emmanuel labaronne
* located here: https://gitbio.ens-lyon.fr/LBMC/RMI2/rmi2_pipelines/-/blob/master/src/RibosomeProfiling.nf
*/

/*
 ****************************************************************
                       parameters
 ****************************************************************
*/

params.paired_end = false
/* false for single end data, true for paired-end data

@type: Boolean
*/

params.fastq = "data/tiny_ribosome_profiling_dataset/fastq/*.fastq"
/* Fastq files containing ribosome footprint

@type: Files
*/
params.rrna_fasta = "data/tiny_ribosome_profiling_dataset/contaminants.fasta"
/* A fasta file containing Rrna or/and other ncRNA sequences
@type: File
*/
params.genome = "data/tiny_ribosome_profiling_dataset/synth.fasta"
/* A genome file

@type: File
*/
params.cutadapt = "" // -u 13
/* cutadapt aditional parameter
Note: option -j for multithreading should not be used as it is defined \
 with the configuration file

@type: File
*/

params.urqt = ""
/* Parameter for urqt.
Note: option --m for multithreading should not be used as it is defined \
 with the configuration file

@type: String
*/
params.bowtie_seedlen = 20
/* seedlen parameter for bowtie

It should be lower than the smallest read after trimming
@type: integer
*/

params.folder = "project"
/* project output folder 

@Type: String
*/
params.skip_trimming = false
/* parameter to skip quality trimming

@type: boolean
*/

/*
 ****************************************************************
                              Logs
 ****************************************************************
*/


log.info "paired-end data: ${params.paired_end}"
log.info "fastq files : ${params.fastq}"
log.info "rrna fasta files : ${params.rrna_fasta}"
log.info "genome files : ${params.genome}"
log.info "cutadapt parameters : ${params.cutadapt}"
log.info "urqt parameters : ${params.urqt}"
log.info "bowtie seedlen : ${params.bowtie_seedlen}"
log.info "output folder results/${params.folder}"
log.info "skipping quality trimming: ${params.skip_trimming}"


/*
 ****************************************************************
                  Channel definitions
 ****************************************************************
*/


/* Raw reads fastq */
Channel
  .fromFilePairs( params.fastq, size: -1 )
  .set { fastq_files }

/* contaminant fasta file */
Channel
  .fromPath( params.rrna_fasta )
  .ifEmpty { error "Cannot find any rrna files matching: ${params.rrna_fasta}" }
  .set { contaminants_file }

/* genome file */
Channel
  .fromPath( params.genome )
  .ifEmpty { error "Cannot find any genome files matching: ${params.genome}" }
  .set { genome_file }


/*
 ****************************************************************
                          Imports
 ****************************************************************
*/

fastq_mod = './nf_modules/fastqc.nf'

include { fastqc as fastqc_raw } from fastq_mod addParams(out: '01_fastqc')
include { fastqc as fastqc_cut } from fastq_mod addParams(out: '02_fastqc_cutadapt')
include { fastqc as fastqc_trim } from fastq_mod addParams(out: '03_fastqc_trimming')
include { fastqc as fastqc_no_conta } from fastq_mod addParams(out: '04_fastqc_without_rRNA')
include { fastqc as fastqc_conta } from fastq_mod addParams(out: '04_fastqc_rRNA')
include { fastqc as fastqc_aligned } from fastq_mod addParams(out: '08_fastqc_aligned')
include { fastqc as fastqc_unaligned } from fastq_mod addParams(out: '08_fastqc_unaligned')
include { multiqc } from './nf_modules/fastqc.nf'
include { adaptor_removal;
          quality_trimming;
          index_genome;
          genome_mapping;
          index_bam; } from './nf_modules/reads_processing.nf'
if ( params.paired_end == false ) {
  include { rrna_indexing_se as rrna_indexing;
            rRNA_removal_se as rRNA_removal;
            } from './nf_modules/reads_processing.nf'
} else {
  include { rrna_indexing_pe as rrna_indexing;
            rRNA_removal_pe as rRNA_removal;
            } from './nf_modules/reads_processing.nf'
}
include { bamtofastq } from './nf_modules/reads_processing.nf' addParams(paired_end: "${params.paired_end}" )
include { merge_channels } from './nf_functions/reads_processing.nf'


/*
 ****************************************************************
                          Workflow
 ****************************************************************
*/


workflow {
  adaptor_removal(fastq_files)
  rrna_indexing(contaminants_file)
  if (!params.skip_trimming) {
    quality_trimming(adaptor_removal.out.fastq)
    rRNA_removal(quality_trimming.out.fastq, rrna_indexing.out.index.collect())
  } else {
    rRNA_removal(adaptor_removal.out.fastq, rrna_indexing.out.index.collect())
  }
  index_genome(genome_file)
  genome_mapping(rRNA_removal.out.fastq, index_genome.out.index.collect())
  index_bam(genome_mapping.out.aligned)
  bamtofastq(genome_mapping.out.aligned)

  // fastqc
  fastqc_raw(fastq_files)
  fastqc_cut(adaptor_removal.out.fastq)
  if (!params.skip_trimming) { 
    fastqc_trim(quality_trimming.out.fastq)
    report_trim = fastqc_trim.out.report 
  } else {
    report_trim = ""
  }
  fastqc_no_conta(rRNA_removal.out.fastq)
  fastqc_conta(rRNA_removal.out.conta)
  fastqc_unaligned(genome_mapping.out.unaligned)
  fastqc_aligned(bamtofastq.out.fastq)

  // multiQC
  res = merge_channels(
    fastqc_raw.out.report, fastqc_cut.out.report, report_trim, 
    fastqc_no_conta.out.report, fastqc_conta.out.report, 
    fastqc_unaligned.out.report, fastqc_aligned.out.report, 
    params.skip_trimming)
  multiqc(res)

}