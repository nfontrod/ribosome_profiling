nextflow.enable.dsl=2

/* Periodicity analysis pipeline:
 * Short pipeline dedicated to the creation of periodicity figures
*/


/*
 ****************************************************************
                              Logs
 ****************************************************************
*/


log.info "Folder containing bam file and their index: ${params.bam_folder}"
log.info "Annotation file used: ${params.bed_file}"
log.info "Output folder: results/${params.folder}"
log.info "Parameters of the intron periodicity figure: "
log.info "  -size: ${params.figure_parameters.size}"
log.info "  -sample: ${params.figure_parameters.sample}"
log.info "  -normalize: ${params.figure_parameters.normalize}"


/*
 ****************************************************************
                  Channel definitions
 ****************************************************************
*/


Channel
    .fromFilePairs ( "${params.bam_folder}/*_sorted.{bam,bam.bai}" )
    .ifEmpty { error "Unable to file bam file and/or their index !"}
    .map { it -> [it[0], it[1][0], it[1][1]]}
    .set { bam_files }

Channel
    .fromPath( params.bed_file )
    .ifEmpty { error "Unable to find the bed file"}
    .set { bed_file }

Channel
    .fromPath(params.offset_table)
    .ifEmpty('XXX.txt')
    .set { offset_table }

Channel
    .from( params.figure_parameters.size )
    .combine (
        Channel
            .from( params.figure_parameters.sample )
    )
    .combine (
        Channel
            .from( params.figure_parameters.normalize )
    ).set { figure_params }


/*
 ****************************************************************
                          Imports
 ****************************************************************
*/


include { intron_bed; 
          periodicity_table; 
          figures } from './nf_modules/intron_periodicity.nf'
include { merge_tables_psite as merge_tables 
          } from './nf_modules/periodicity.nf'


/*
 ****************************************************************
                          Workflow
 ****************************************************************
*/


workflow {
    intron_bed(bed_file.collect())
    periodicity_table(bam_files, offset_table.collect(), 
                      bed_file.collect(), intron_bed.out.bed.collect())
    merge_tables(periodicity_table.out.position.collect())
    figures(figure_params, merge_tables.out.position.collect())
}
