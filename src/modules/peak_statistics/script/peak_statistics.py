#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to load a table of counts of codon \
peaks and analyse it
"""
import doctest

import pandas as pd
from pathlib import Path
from typing import List, Union, Dict, Optional, Tuple
from .stat_annot import create_figure, create_figure_fp
import multiprocessing as mp
from functools import reduce
import matplotlib.pyplot as plt
import seaborn as sns

def get_feature(level: str = "codon") -> Union[List, Dict]:
    """
    Return the list of coding feature

    :param level: The kind of feature to analyse ('aa' or 'codon')
    :return: The list of coding codons
    """
    aa_dic = {
        "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG"],
        "H": ["CAT", "CAC"],
        "K": ["AAA", "AAG"],
        "D": ["GAT", "GAC"],
        "E": ["GAA", "GAG"],
        "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC"],
        "T": ["ACT", "ACC", "ACA", "ACG"],
        "N": ["AAT", "AAC"],
        "Q": ["CAA", "CAG"],
        "C": ["TGT", "TGC"],
        "G": ["GGT", "GGC", "GGA", "GGG"],
        "P": ["CCT", "CCC", "CCA", "CCG"],
        "A": ["GCT", "GCC", "GCA", "GCG"],
        "I": ["ATT", "ATC", "ATA"],
        "L": ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG"],
        "M": ["ATG"],
        "F": ["TTT", "TTC"],
        "W": ["TGG"],
        "Y": ["TAT", "TAC"],
        "V": ["GTT", "GTC", "GTA", "GTG"]
    }
    if level == "codon":
        return reduce(lambda x, y: x + y, [aa_dic[aa] for aa in aa_dic])
    elif level == "aa":
        return list(aa_dic.keys())
    else:
        return aa_dic


def reorder_dataframe(df: pd.DataFrame, list_ft: List) -> pd.DataFrame:
    """
    Reorder the dataframe df by the column ft.

    :param df: The dataframe
    :param list_ft: list of feature of interest
    :return: The dataframe ordered by codons
    """
    tmp = pd.DataFrame({"ft": list_ft, "order": range(len(list_ft))})
    df = df.merge(tmp, how="left", on="ft")
    return df.sort_values("order", ascending=True, axis=0)


def add_background_color_codon(ax: plt.axes) -> plt.axes:
    """
    Add background color for each amino acid encoded by a codon.

    :param ax: The graphic
    :return: graphic updated
    """
    interval = []
    ax_range = ax.get_xlim()
    ymin = ax.get_ylim()[0]
    start = - 0.5
    aa_dic = get_feature("dic")
    for aa in aa_dic:
        interval.append([start, start + len(aa_dic[aa]), aa])
        start += len(aa_dic[aa])
    for k, cinterval in enumerate(interval):
        color = 'white' if k % 2 == 0 else "grey"
        ax.axvspan(cinterval[0], cinterval[1], facecolor=color, alpha=0.2)
        ax.text((cinterval[0] + cinterval[1]) / 2, ymin + abs(ymin * 0.05),
                 cinterval[2], ha="center")
        ax.set_xlim(ax_range)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=90, ha='center')
    return ax


def create_recap_figure(dft: pd.DataFrame, output: Path,
                        level: str, analysis: str, ctrl: str,
                        ft: str, other: str) -> None:
    """

    :param dft: the dataframe of delta proportions
    :param output: Folder where the result will be created
    :param level: aa or codon
    :param analysis: sample or size
    :param ctrl: Control sample or size
    :param ft: A sample name or a size
    :param other: A sample name or a size
    """
    df = dft.loc[dft[analysis] == ft, :].copy()
    df['Pr(>|z|)'] = df['Pr(>|z|)'].astype(str)
    df['Pr(>|z|)'] = df['Pr(>|z|)'].str.replace(',', '.').astype(float)
    df['significant'] = df['Pr(>|z|)'].apply(lambda x: 'p < 0.05'
                                             if x < 0.05 else 'p > 0.05')
    sns.set(context="talk", style="white")
    g = sns.relplot(x="ft", y="rel_count", style="replicate",
                    hue='significant', data=df, height=12, aspect=2.2,
                    palette={"p < 0.05": "#EE2222", "p > 0.05": "#2222EE"})
    g.set_xlabels("Codons" if level == "codon" else "Amino acids")
    g.set_ylabels(f"{ft} - {ctrl} proportions")
    if level == "codon":
        add_background_color_codon(g.ax)
    if analysis == "sample":
        title = f'Difference of {level} proportions in peaks\n' \
                f'between {ft} and {ctrl} samples ' \
                f'(for footprints of size {other})'
    else:
        title = f'Difference of {level} proportions in peaks\n' \
                f'between {ft} and {ctrl} footprint size ' \
                f'(for sample {other})'
    g.fig.suptitle(title)
    xrange = g.ax.get_xlim()
    plt.hlines(0, xrange[0], xrange[1], linestyles="dashed")
    g.ax.set_xlim(xrange)
    g.savefig(output / f'recap_{ft}_{other}_{level}.pdf')


def filter_peak_composition_table(df: pd.DataFrame,
                                  peak_size: Tuple[Optional[int],
                                  Optional[int]]) -> pd.DataFrame:
    """
    Filter the peak composition table to only keep peaks with the wanted \
    size in codons.

    :param df: a dataframe containing the peak composition in codon \
    and amino acids
    :param peak_size: the range of peak sizes in codons to use for \
    the statistical analysis
    :return: The same dataframe with only peaks containing \
    a number of codon inside peak_size range

    >>> d = pd.DataFrame({'count': [1, 0, 1, 2, 0, 1, 2, 0],
    ... 'ft': ['AAA'] * 8,
    ... 'ft_type': ['codon'] * 8,
    ... 'peak': [f'peak_{i}' for i in range(1, 9)],
    ... 'gene': ['DAZAP1'] * 8,
    ... 'sample': ['CTRL'] * 8,
    ... 'replicate': ['R1'] * 8,
    ... 'size': ['0-80'] * 8,
    ... 'codon_in_peak': [8, 7, 6, 5, 4, 3, 2, 1]})
    >>> filter_peak_composition_table(d, (None, None))
       count   ft ft_type    peak    gene sample replicate  size  codon_in_peak
    0      1  AAA   codon  peak_1  DAZAP1   CTRL        R1  0-80              8
    1      0  AAA   codon  peak_2  DAZAP1   CTRL        R1  0-80              7
    2      1  AAA   codon  peak_3  DAZAP1   CTRL        R1  0-80              6
    3      2  AAA   codon  peak_4  DAZAP1   CTRL        R1  0-80              5
    4      0  AAA   codon  peak_5  DAZAP1   CTRL        R1  0-80              4
    5      1  AAA   codon  peak_6  DAZAP1   CTRL        R1  0-80              3
    6      2  AAA   codon  peak_7  DAZAP1   CTRL        R1  0-80              2
    7      0  AAA   codon  peak_8  DAZAP1   CTRL        R1  0-80              1
    >>> filter_peak_composition_table(d, (1, 5))
       count   ft ft_type    peak    gene sample replicate  size  codon_in_peak
    3      2  AAA   codon  peak_4  DAZAP1   CTRL        R1  0-80              5
    4      0  AAA   codon  peak_5  DAZAP1   CTRL        R1  0-80              4
    5      1  AAA   codon  peak_6  DAZAP1   CTRL        R1  0-80              3
    6      2  AAA   codon  peak_7  DAZAP1   CTRL        R1  0-80              2
    7      0  AAA   codon  peak_8  DAZAP1   CTRL        R1  0-80              1
    >>> filter_peak_composition_table(d, (6, 8))
       count   ft ft_type    peak    gene sample replicate  size  codon_in_peak
    0      1  AAA   codon  peak_1  DAZAP1   CTRL        R1  0-80              8
    1      0  AAA   codon  peak_2  DAZAP1   CTRL        R1  0-80              7
    2      1  AAA   codon  peak_3  DAZAP1   CTRL        R1  0-80              6

    """
    if None in peak_size:
        return df
    return df[(peak_size[0] <= df["codon_in_peak"]) &
              (df["codon_in_peak"] <= peak_size[1])].copy()


def make_analysis(df: pd.DataFrame, ctrl: Optional[str], output: Path,
                  analysis: str = "sample",
                  ps: int = 1, level: str = "codon",
                  peak_analysis: bool = True, site: str = "",
                  delta: int = 0,
                  peak_size: Tuple[Optional[int], Optional[int]] = (None, None)
                  ) -> None:
    """
    Make the statistical analysis on sizes.

    :param df: The dataframe containing count codon in peaks.
    :param ctrl: The control sample to use.
    :param output: Folder where results will be created
    :param analysis: The kind of analysis we want to make (sample or size)
    :param ps: number of processes to create
    :param level: The kind of feature to analyse ('aa' or 'codon')
    :param peak_analysis: 'y' if the analysis corresponds to peaks coverage \
    'n' if it corresponds to footprint directly
    :param site: The site of interest sought in footprints. This parameter \
    is only used if peak_analysis is False.
    :param delta: The number of codon around the site `site`. This parameter \
    is only used if peak_analysis is False
    :param peak_size: the range of peak sizes in codons to use for the \
    statistical analysis
    """
    ps = mp.cpu_count() if ps > mp.cpu_count() else (1 if ps < 1 else ps)
    df = filter_peak_composition_table(df, peak_size)
    other = "sample" if analysis == "size" else "size"
    features = df[other].unique()
    for ft in features:
        print(f"Working on {ft}")
        ndf = df.loc[df[other] == ft, :]
        ift = ndf[analysis].unique()
        if len(ift) == 1:
            if analysis == "sample":
                print(f"skipping the analysis for footprint of size {ft}, "
                      f"as it has only one sample")
            else:
                print(f"skipping the analysis for footprint of size {ift}, "
                      f"as it is present for only one sample")
            continue
        noutput = output / f"{ft}_stat"
        stat_out = noutput / "stats_file"
        diag_out = noutput / "diagnositcs"
        bar_out = noutput / "barplot"
        stat_out.mkdir(parents=True, exist_ok=True)
        diag_out.mkdir(parents=True, exist_ok=True)
        bar_out.mkdir(parents=True, exist_ok=True)
        use_replicate = len(df["replicate"].unique()) > 1
        pool = mp.Pool(processes=ps)
        process = []
        list_ft = get_feature(level)
        for coding_ft in list_ft:
            cdf = ndf.loc[(ndf["ft"] == coding_ft) &
                          (ndf['ft_type'] == level), :].copy()
            if peak_analysis:
                args = [cdf, ctrl, coding_ft, stat_out, diag_out, bar_out,
                        use_replicate, level, ft, analysis]
                process.append(pool.apply_async(create_figure, args))
            else:
                args = [cdf, ctrl, coding_ft, stat_out, diag_out, bar_out,
                        use_replicate, level, ft, analysis, other, site, delta,
                        ]
                process.append(pool.apply_async(create_figure_fp, args))
        result = [proc.get(timeout=None) for proc in process]
        if ctrl is not None:
            df_delta = pd.concat(result, axis=0, ignore_index=True)
            df_delta.to_csv(output / "tmp_delta_table.txt", sep="\t",
                            index=False)
            df_delta = reorder_dataframe(df_delta, list_ft)
            for cft in df_delta[analysis].unique():
                create_recap_figure(df_delta, output, level, analysis,
                                    ctrl, cft, ft)


if __name__ == "__main__":
    doctest.testmod()
