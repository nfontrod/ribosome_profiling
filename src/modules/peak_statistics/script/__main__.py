#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Laun cher of the peak statistics module.
"""
import doctest

import lazyparser as lp
import pandas as pd
from pathlib import Path
from .peak_statistics import make_analysis
from typing import List, Tuple, Optional


def get_peak_size(peak_size: List[str]
                  ) -> Tuple[Tuple[Optional[int], Optional[int]], str]:
    """
    Check if the peak_size given in input is in the correct format, and \
    convert it into a tuple of two int.

    :param peak_size: The size of peaks in codons to use for statistical \
    analysis
    :return: the range of peak sizes in codons to use for the statistical \
    analysis and the name for the output folder

    >>> get_peak_size(['ALL'])
    ((None, None), 'ALL')
    >>> get_peak_size(['1', '2'])
    ((1, 2), '1-2')
    >>> get_peak_size(['1', '2', '3'])
    Traceback (most recent call last):
    ...
    IndexError: parameter peak_size should have a length of one if it is \
equal to 'ALL' or two if it contains integers
    >>> get_peak_size(['1', 'ALL'])
    Traceback (most recent call last):
    ...
    IndexError: parameter peak_size should have a length of one if it is \
equal to 'ALL' or two if it contains integers
    >>> get_peak_size(['ALL', 'ALL'])
    Traceback (most recent call last):
    ...
    IndexError: parameter peak_size should have a length of one if it is \
equal to 'ALL' or two if it contains integers
    >>> get_peak_size(['1', '2', 'ALL'])
    Traceback (most recent call last):
    ...
    IndexError: parameter peak_size should have a length of one if it is \
equal to 'ALL' or two if it contains integers
    >>> get_peak_size(['1', 'lul'])
    Traceback (most recent call last):
    ...
    ValueError: The values given with peak_size argument should be integers \
or all
    """
    if peak_size[0] == "ALL" and len(peak_size) == 1:
        return (None, None), 'ALL'
    elif ('ALL' in peak_size and len(peak_size) != 1) or (len(peak_size) != 2):
        msg = f"parameter peak_size should have a length of one " \
              f"if it is equal to 'ALL' or two if it contains " \
              f"integers"
        raise IndexError(msg)
    try:
        res = (int(peak_size[0]), int(peak_size[1]))
        return (res), '-'.join(list(map(str, res)))
    except ValueError:
        raise ValueError("The values given with peak_size argument should be "
                         "integers or all")


@lp.parse(table_count="file", analysis=["sample", "size"],
          level=["codon", "aa"])
def stat_maker(table_count: str, output: str = ".",
               analysis: str = "sample", ctrl: str = "",
               ps: int = 1, level: str = "codon",
               peak_analysis: str = "y", site: str = "",
               delta: int = 0, peak_size: List[str] = ("ALL")) -> None:
    """
    Make statistical analysis on codon peak composition.

    :param table_count: A table with the codon count on each peaks.
    :param output: Folder where the results will be created (default '.')
    :param analysis: The kind a analysis we want to make (size, sample) \
    (default 'sample')
    :param ps: Number of process to create
    :param level: The kind of feature to analyse ('aa' or 'codon') \
    (default 'codon')
    :param ctrl: The name of a sample if analysis = 'sample' or a size \
    if analysis = 'size'.
    :param peak_analysis: 'y' if the analysis corresponds to peaks coverage \
    'n' if it corresponds to footprint directly. (default 'y')
    :param site: The site of interest sought in footprints. This parameter \
    is only used if peak_analysis is equals to 'n'.
    :param delta: The number of codon around the site `site`. This parameter \
    is only used if peak_analysis equals 'n'.
    :param peak_size: The range of peak size (in codons) that we want to \
    select. For example if peak_size is equal to [0, 10] then only \
    peak containing 10 codons are used for the enrichment analysis. If \
    peak_size = ALL then all peaks are considered (default = 'ALL')
    """
    peak_analysis = peak_analysis.lower() == 'y'
    new_peak_size, name_peak = get_peak_size(peak_size)
    if not peak_analysis and site not in ['A', 'E', 'P']:
        raise ValueError(f"You must give the ribosome site analysed in your "
                         f"table if peak_analysis is 'n' and delta != -1")
    if table_count.endswith('.gz'):
        df = pd.read_csv(table_count, sep="\t", compression='gzip')
    else:
        df = pd.read_csv(table_count, sep="\t")
    if ctrl != "" and ctrl not in df[analysis].unique():
        raise NameError(f"the parameter ctrl corresponds to a value ({ctrl})"
                        f" not found in the column {analysis}. Make sure "
                        f"the parameter given in --ctrl parameter corresponds"
                        f" to an {analysis}, which is given with --analysis ")
    if ctrl == "":
        ctrl = None
    output = Path(output) / f"analysis_{analysis}_{level}_ps{name_peak}"
    output.mkdir(parents=True, exist_ok=True)
    print(f"analysis chosen : {analysis}")
    make_analysis(df, ctrl, output, analysis, ps, level, peak_analysis,
                  site, delta, new_peak_size)


stat_maker()


