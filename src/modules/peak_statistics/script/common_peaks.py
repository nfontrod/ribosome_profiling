#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to check what sample \
is more similar to the control than others
"""


import pandas as pd
import doctest
from pathlib import Path
from .stat_annot import create_common_peak_figure
import lazyparser as lp


class EmptyTableError(Exception):
    pass


def drop_homogeneous_replicate(df: pd.DataFrame) -> pd.DataFrame:
    """
    Remove all lines with different replicate and footprint size, \
    and same sample

    :param df: The dataframe showing specific and common peaks between \
    each samples.
    :return: The dataframe with only one homogeneous replicate column

    >>> d = pd.DataFrame(
    ... {"sample1": ["S1", "S1", "S1", "S1", "S1", "S2", "S1"],
    ...  "sample2": ["S1", "S2", "S2", "S2", "S2", "S2", "S3"],
    ...  "rep1": ["R1", "R1", "R1", "R2", "R2", "R1", "R1"],
    ...  "rep2": ["R2", "R1", "R2", "R1", "R2", "R2", "R1"],
    ...  "size1": ["10-20"] * 7,
    ...  "size2": ["10-20"] * 6 + ["20-30"]})
    >>> drop_homogeneous_replicate(d)
      sample1 sample2 replicate   size
    1      S1      S2        R1  10-20
    4      S1      S2        R2  10-20
    """
    df = df.loc[(df['rep1'] == df['rep2']) &
                (df['sample1'] != df["sample2"]) &
                (df["size1"] == df["size2"]), :].copy()
    if df.empty:
        raise EmptyTableError("The dataframe doesn't contain any "
                              "line in which rep1 = rep2")
    df.drop(['rep2', 'size2'], inplace=True, axis=1)
    return df.rename({'rep1': 'replicate', 'size1': 'size'}, axis=1)


def create_logistic_table(df: pd.DataFrame, ctrl: str) -> pd.DataFrame:
    """
    Create a table to make logistics regression with it.

    :param df: A dataframe with common peaks and specific peaks between \
    each sample
    :param ctrl: The control sample
    :return: A table with 1 for a common peak with the control sample \
    and 0 for a specific peak
    >>> d = pd.DataFrame(
    ... {"sample1": ["S1", "S2"],
    ...  "sample2": ["S2", "S3"],
    ...  "replicate": ["R1", "R1"],
    ...  "size": ["10-20"] * 2,
    ...  "specific1": [3, 2],
    ...  "specific2": [1, 0],
    ...  "common1": [3, 2],
    ...  "common2": [1, 2]})
    >>> create_logistic_table(d, 'S2')
       peak sample replicate   size
    0     1  S1-S2        R1  10-20
    1     1  S1-S2        R1  10-20
    2     1  S1-S2        R1  10-20
    3     0  S1-S2        R1  10-20
    4     0  S1-S2        R1  10-20
    5     0  S1-S2        R1  10-20
    6     1  S3-S2        R1  10-20
    7     1  S3-S2        R1  10-20
    """
    df = df.loc[(df["sample1"] == ctrl) | (df['sample2'] == ctrl), :].copy()
    d = {"peak": [], "sample": [], "replicate": [], "size": []}
    for i in range(df.shape[0]):
        row = df.iloc[i, :]
        specific = "specific1"
        common = "common1"
        sample = "sample1"
        if row["sample1"] == ctrl:
            sample = "sample2"
            specific = "specific2"
            common = "common2"
        vect = [1] * row[common] + [0] * row[specific]
        d['peak'] += vect
        d["sample"] += [f"{row[sample]}-{ctrl}"] * len(vect)
        d["replicate"] += [row["replicate"]] * len(vect)
        d["size"] += [row["size"]] * len(vect)
    return pd.DataFrame(d)


def make_analysis(df: pd.DataFrame, output: Path, ctrl: str,
                  analysis: str = "sample") -> None:
    """
    Make the statistical analysis on sizes.

    :param df: The dataframe containing count codon in peaks.
    :param output: Folder where results will be created
    :param analysis: The kind of analysis we want to make (sample or size)
    :param ctrl: The control sample
    """
    other = "sample" if analysis == "size" else "size"
    features = df[other].unique()
    for ft in features:
        print(f"Working on {ft}")
        ndf = df.loc[df[other] == ft, :].copy()
        ift = ndf[analysis].unique()
        if len(ift) == 1:
            if analysis == "sample":
                print(f"skipping the analysis for footprint of size {ft}, "
                      f"as it has only one sample")
            else:
                print(f"skipping the analysis for footprint of size {ift}, "
                      f"as it is present for only one sample")
            continue
        stat_out = output / "stats_file"
        diag_out = output / "diagnositcs"
        bar_out = output / "barplot"
        stat_out.mkdir(parents=True, exist_ok=True)
        diag_out.mkdir(parents=True, exist_ok=True)
        bar_out.mkdir(parents=True, exist_ok=True)
        use_replicate = True if len(df["replicate"].unique()) > 1 else False
        create_common_peak_figure(ndf, stat_out, diag_out, bar_out,
                                  use_replicate, ft, ctrl, analysis)


@lp.parse(table_peak="file", analysis=["sample", "size"])
def make_common_peak_analysis(table_peak: str, ctrl: str, output: str = ".",
                              analysis: str = "sample") -> None:
    """
    Create figures showing the samples more similar to another one.

    :param table_peak: A table showing common and specific peaks \
    between each sample
    :param ctrl: The control sample.
    :param output: folder where the results will be created (default '.')
    :param analysis: The kind of analysis to make (sample, size) \
    (default 'sample)
    """
    df = pd.read_csv(table_peak, sep="\t")
    df = drop_homogeneous_replicate(df)
    stat_table = create_logistic_table(df, ctrl)
    output = Path(output) / f"analysis_{analysis}"
    make_analysis(stat_table, output, ctrl, analysis)


if __name__ == "__main__":
    # doctest.testmod()
    make_common_peak_analysis()