#!/usr/bin/env python3

# -*- coding;: utf-8 -*-

"""
Description:
    Add annotation on figure.
    Script adapted from https://github.com/webermarcolivier/statannot/blob/master/statannot/statannot.py.
"""


import matplotlib.pyplot as plt
from matplotlib import lines
import matplotlib.transforms as mtransforms
from matplotlib.font_manager import FontProperties
import numpy as np
import seaborn as sns
from seaborn.utils import remove_na
from .gml_stat import get_stat_annot_tuple, glm_stats
import pandas as pd
from pathlib import Path
import doctest


def add_stat_annotation(ax,
                        data=None, x=None, y=None, hue=None, order=None, hue_order=None,
                        boxPairList=None,
                        loc='inside',
                        useFixedOffset=False, lineYOffsetToBoxAxesCoord=None,
                        lineYOffsetAxesCoord=None,
                        lineHeightAxesCoord=0.02, textYOffsetPoints=1,
                        color='0.2', linewidth=1.5, fontsize='medium',
                        verbose=1):
    """
    User should use the same argument for the data, x, y, hue, order, hue_order as the seaborn boxplot function.
    boxPairList can be of either form:
    boxplot: [(cat1, cat2, pval), (cat3, cat4, pval)]
    """

    def find_x_position_box(boxPlotter, boxName):
        """
        boxName can be either a name "cat" or a tuple ("cat", "hue")
        """
        if boxPlotter.plot_hues is None:
            cat = boxName
            hueOffset = 0
        else:
            cat = boxName
            hueOffset = np.median(boxPlotter.hue_offsets)

        groupPos = boxPlotter.group_names.index(cat)
        boxPos = groupPos + hueOffset
        return boxPos


    def get_box_data(boxPlotter, boxName):
        """
        boxName can be either a name "cat" or a tuple ("cat", "hue")
        Here we really have to duplicate seaborn code, because there is not direct access to the
        box_data in the BoxPlotter class.
        """
        cat = boxName

        i = boxPlotter.group_names.index(cat)
        group_data = boxPlotter.plot_data[i]

        if boxPlotter.plot_hues is None:
            box_data = remove_na(group_data)
            return np.mean(box_data)
        else:
            box_data = []
            for hue in boxPlotter.hue_names:
                hue_mask = boxPlotter.plot_hues[i] == hue
                box_data.append(np.mean(remove_na(group_data[hue_mask])))
            return max(box_data)

    fig = plt.gcf()

    validList = ['inside', 'outside']
    if loc not in validList:
        raise ValueError("loc value should be one of the following: {}.".format(', '.join(validList)))

    # Create the same BoxPlotter object as seaborn's boxplot
    boxPlotter = sns.categorical._BoxPlotter(x, y, hue, data, order, hue_order,
                                             orient=None, width=.8, color=None, palette=None, saturation=.75,
                                             dodge=True, fliersize=5, linewidth=None)
    plotData = boxPlotter.plot_data

    xtickslabels = [t.get_text() for t in ax.xaxis.get_ticklabels()]
    ylim = ax.get_ylim()
    yRange = ylim[1] - ylim[0]

    if lineYOffsetAxesCoord is None:
        if loc == 'inside':
            lineYOffsetAxesCoord = 0.05
            if lineYOffsetToBoxAxesCoord is None:
                lineYOffsetToBoxAxesCoord = 0.06
        elif loc == 'outside':
            lineYOffsetAxesCoord = 0.03
            lineYOffsetToBoxAxesCoord = lineYOffsetAxesCoord
    else:
        if loc == 'inside':
            if lineYOffsetToBoxAxesCoord is None:
                lineYOffsetToBoxAxesCoord = 0.06
        elif loc == 'outside':
            lineYOffsetToBoxAxesCoord = lineYOffsetAxesCoord
    yOffset = lineYOffsetAxesCoord*yRange
    yOffsetToBox = lineYOffsetToBoxAxesCoord*yRange

    yStack = []
    annList = []
    for box1, box2, pval in boxPairList:

        valid = None
        groupNames = boxPlotter.group_names
        hueNames = boxPlotter.hue_names
        cat1 = box1
        cat2 = box2
        hue1 = None
        hue2 = None
        label1 = '{}'.format(cat1)
        label2 = '{}'.format(cat2)
        valid = cat1 in groupNames and cat2 in groupNames

        if valid:
            # Get position of boxes
            x1 = find_x_position_box(boxPlotter, box1)
            x2 = find_x_position_box(boxPlotter, box2)
            box_data1 = get_box_data(boxPlotter, box1)
            box_data2 = get_box_data(boxPlotter, box2)
            ymax1 = box_data1
            ymax2 = box_data2

            if pval > 1e-16:
                text = "p = {:.2e}".format(pval)
            else:
                text = "p < 1e-16"

            if loc == 'inside':
                yRef = max(ymax1, ymax2)
            elif loc == 'outside':
                yRef = ylim[1]

            if len(yStack) > 0:
                yRef2 = max(yRef, max(yStack))
            else:
                yRef2 = yRef

            if len(yStack) == 0:
                y = yRef2 + yOffsetToBox
            else:
                y = yRef2 + yOffset
            h = lineHeightAxesCoord*yRange
            lineX, lineY = [x1, x1, x2, x2], [y, y + h, y + h, y]
            if loc == 'inside':
                ax.plot(lineX, lineY, lw=linewidth, c=color)
            elif loc == 'outside':
                line = lines.Line2D(lineX, lineY, lw=linewidth, c=color, transform=ax.transData)
                line.set_clip_on(False)
                ax.add_line(line)

            if text is not None:
                ann = ax.annotate(text, xy=(np.mean([x1, x2]), y + h),
                                  xytext=(0, textYOffsetPoints), textcoords='offset points',
                                  xycoords='data', ha='center', va='bottom', fontsize=fontsize,
                                  clip_on=False, annotation_clip=False)
                annList.append(ann)

            ax.set_ylim((ylim[0], 1.1*(y + h)))

            if text is not None:
                plt.draw()
                yTopAnnot = None
                gotMatplotlibError = False
                if not useFixedOffset:
                    try:
                        bbox = ann.get_window_extent()
                        bbox_data = bbox.transformed(ax.transData.inverted())
                        yTopAnnot = bbox_data.ymax
                    except RuntimeError:
                        gotMatplotlibError = True

                if useFixedOffset or gotMatplotlibError:
                    if verbose >= 1:
                        print("Warning: cannot get the text bounding box. Falling back to a fixed y offset. Layout may be not optimal.")
                    # We will apply a fixed offset in points, based on the font size of the annotation.
                    fontsizePoints = FontProperties(size='medium').get_size_in_points()
                    offsetTrans = mtransforms.offset_copy(ax.transData, fig=fig,
                                                          x=0, y=1.0*fontsizePoints + textYOffsetPoints, units='points')
                    yTopDisplay = offsetTrans.transform((0, y + h))
                    yTopAnnot = ax.transData.inverted().transform(yTopDisplay)[1]
            else:
                yTopAnnot = y + h

            yStack.append(yTopAnnot)
        else:
            raise ValueError("boxPairList contains an unvalid box pair.")
            pass

    yStackMax = max(yStack)
    if loc == 'inside':
        ax.set_ylim((ylim[0], 1.03*yStackMax))
    elif loc == 'outside':
        ax.set_ylim((ylim[0], ylim[1]))

    return ax


def get_df_proportion_vs_ctrl(dataframe: pd.DataFrame, ctrl: str,
                              analysis: str) -> pd.DataFrame:
    """
    Return a dataframe relative to the control defined with the 'ctrl' \
    argument.

    :param dataframe: A dataframe with the proportion of
    :param ctrl: The control sample or size
    :param analysis: The kind of analysis to make (on sample or size)
    :return: Dataframe with proportion relative to the control

    >>> d = pd.DataFrame({"ncount": [0.1, 0.2, 0.1, 0.2, 0, 0.1, 0, 0.1,
    ...                              0.2, 0.25, 0.2, 0.25, 0, 0.05, 0, 0.05],
    ...                   "peak": [1, 2, 3, 4] * 4,
    ...                   "ft_type": ["aa"] * 16,
    ...                   "ft": ["R", "R", "R", "R"] * 4,
    ...                   "replicate": [1, 1, 1, 1] * 2 + [2, 2, 2, 2] * 2,
    ...                   "size": [20] * 16,
    ...                   "sample": ["test"] * 4 + ["ctrl"] * 4 +
    ...                             ["test"] * 4 + ["ctrl"] * 4})
    >>> get_df_proportion_vs_ctrl(d, 'ctrl', 'sample')
      sample ft ft_type  size  replicate  rel_count
    0   test  R      aa    20          1        0.1
    1   test  R      aa    20          2        0.2
    >>> d = pd.DataFrame({"ncount": [0.1, 0.2, 0.1, 0.2, 0, 0.1, 0, 0.1],
    ...                   "peak": [1, 2, 3, 4] * 2,
    ...                   "ft_type": ["aa"] * 8,
    ...                   "ft": ["R", "R", "R", "R"] * 2,
    ...                   "replicate": [1, 1, 1, 1] * 2,
    ...                   "size": [20] * 8,
    ...                   "sample": ["test"] * 4 + ["ctrl"] * 4})
    >>> get_df_proportion_vs_ctrl(d, 'ctrl', 'sample')
      sample ft ft_type  size  replicate  rel_count
    0   test  R      aa    20          1        0.1
    """
    ctrl_prop = dataframe.loc[dataframe[analysis] == ctrl,
                              ['ncount', 'replicate']]\
        .groupby("replicate")\
        .mean()\
        .to_dict()['ncount']
    df = dataframe.loc[dataframe[analysis] != ctrl, ["ncount", 'ft', 'ft_type',
                                                     "sample", "size",
                                                     "replicate"]].copy()
    ndf = df.groupby(['sample', 'ft', 'ft_type', 'size', "replicate"]
                     ).mean().reset_index()
    ndf['rel_count'] = ndf.apply(lambda x: x['ncount'] -
                                           ctrl_prop[x['replicate']], axis=1)
    return ndf.drop('ncount', axis=1)


def add_pvalue(dataframe: pd.DataFrame, ctrl: str,
               analysis: str, df_stat: pd.DataFrame):
    """
    Merge dataframe and df_stat to add the p_value to dataframe.

    :param dataframe: A dataframe containing relative proportion to ctrl
    :param ctrl:  The control sample or size
    :param df_stat: A dataframe containing stats
    :return: The dataframe with the p-value

    >>> d = pd.DataFrame({"sample": ["test1", "test2", "test1", "test2"],
    ... "ft": ["R"] * 4,
    ... "ft_type": ["aa"] * 4, "size": [20] * 4,
    ... "rel_count": [-0.2, 0.1, -0.3, 0.2],
    ... "replicate": [1, 1, 2, 2]})
    >>> ds = pd.DataFrame({"Comparison": ["test1 - ctrl", "ctrl - test2",
    ... "test1 - test2"],
    ... "Estimate": [-0.2, 0.1, 0.0], "Std.Error": [0.01, 0.02, 0.5],
    ... 'Pr(>|z|)': [0.01, 0.05, 1.0]})
    >>> add_pvalue(d, "ctrl", "sample", ds)
      sample ft ft_type  size  rel_count  replicate good_el  Pr(>|z|)
    0  test1  R      aa    20       -0.2          1   test1      0.01
    1  test2  R      aa    20        0.1          1   test2      0.05
    2  test1  R      aa    20       -0.3          2   test1      0.01
    3  test2  R      aa    20        0.2          2   test2      0.05
    """
    df_stat['el1'] = df_stat["Comparison"].apply(lambda x: x.split(" - ")[0])
    df_stat['el2'] = df_stat["Comparison"].apply(lambda x: x.split(" - ")[1])
    df_stat = df_stat.loc[(df_stat['el1'] == ctrl) |
                          (df_stat['el2'] == ctrl ), :].copy()
    df_stat['good_el'] = df_stat.apply(lambda x: x.el1 if x.el1 != ctrl else
                                       x.el2, axis=1)
    ndf = df_stat[['good_el', 'Pr(>|z|)']].copy()
    return dataframe.merge(ndf, how='left', left_on=analysis,
                           right_on='good_el')


def make_delta_df(dataframe: pd.DataFrame, ctrl: str, analysis: str,
                  df_stat: pd.DataFrame) -> pd.DataFrame:
    """
    The dataframe with delta proportion compared to control

    :param dataframe: A dataframe containing relative proportion to ctrl
    :param ctrl:  The control sample or size
    :param df_stat: A dataframe containing stats
    :return: The dataframe with the p-value
    """
    delta_df = get_df_proportion_vs_ctrl(dataframe, ctrl, analysis)
    return add_pvalue(delta_df, ctrl, analysis, df_stat)


def create_figure(dataframe: pd.DataFrame, ctrl: str, codon: str,
                  output_stat: Path, output_diag: Path,
                  output_fig: Path, use_replicate: bool, level: str,
                  ft: str, analysis: str = "sample") -> pd.DataFrame:
    """
    Create the figure of interest.

    :param dataframe: the distribution of codon coverage.
    :param ctrl:  The control sample or size
    :param codon: the name of a codon
    :param output_stat: an output dir for stat files.
    :param output_diag: an output dir for diag figures
    :param output_fig: an output dir for seaborn figure
    :param use_replicate: True to use replicate, false else
    :param level: The level of interest, 'aa' or 'codon'
    :param ft: Current ft of interest
    :param analysis: The kind of analysis to make (sample or size).
    """
    sns.set()
    print(f"Working on {codon}")
    dataframe["ncount"] = dataframe["count"] / dataframe["codon_in_peak"]
    g = sns.catplot(y="ncount", x=analysis, hue="replicate", kind="bar",
                    ci=None, data=dataframe, height=6, aspect=1.5,
                    estimator=np.mean)
    clevel = "amino acid" if level == "aa" else level
    g.set_ylabels(f"Mean proportion of {clevel} {codon} in peaks")
    if analysis == "sample":
        g.set_xlabels(f"Samples")
        title = f'Mean proportion of {clevel} {codon} in ribosome profiling ' \
                f'peaks between samples\n' \
                f'(for footprints of size {ft})'
    else:
        g.set_xlabels(f"Footprint size interval")
        title = f'Mean proportion of {clevel} {codon} in ribosome profiling ' \
                f'peaks\nbetween different footprint sizes ' \
                f'for sample {ft}'
    plt.subplots_adjust(top=0.9)
    g.fig.suptitle(title)
    df_stat = glm_stats(dataframe, codon, output_diag, output_stat,
                        use_replicate, analysis, "glm_nb")
    mtuple = get_stat_annot_tuple(df_stat)
    if len(mtuple) < 7:
        ax = g.axes[0, 0]
        add_stat_annotation(ax, data=dataframe, x=analysis, y="ncount",
                            hue="replicate", boxPairList=mtuple)
    plt.savefig("%s/%s_figure.pdf" % (output_fig, codon))
    plt.clf()
    plt.close()
    return make_delta_df(dataframe, ctrl, analysis, df_stat)


def creating_prop_table(df: pd.DataFrame, analysis: str) -> pd.DataFrame:
    """
    From a table containing the count of a particular codon \
    around a ribosome site of interest and the total number of codons \
    counted around those site of interest create a dataframe of 1 for each \
    time the ribosome site falls on this codons and zero each time it doesnt.

    :param df: table containing the count of a particular codon \
    around a ribosome site of interest and the total number of codons \
    counted around those site of interest
    :param analysis: The kind of analysis to make (sample, size)
    :return: dataframe of 1 for each \
    time the ribosome site falls on this codons and zero each time it doesnt
    """
    dic = {"proportion": [], analysis: [], "replicate": []}

    for i in range(df.shape[0]):
        mserie = df.iloc[i, :]
        one_val = int(round(mserie['count'] / 100))
        zero_val = int(round((mserie['tot_count'] - mserie['count']) / 100))
        prop = [1] * one_val + \
               [0] * zero_val
        dic['proportion'] += prop
        dic[analysis] += [mserie[analysis]] * len(prop)
        dic["replicate"] += [mserie["replicate"]] * len(prop)
    return pd.DataFrame(dic)


def create_figure_fp(dataframe: pd.DataFrame, ctrl: str, codon: str,
                     output_stat: Path, output_diag: Path,
                     output_fig: Path, use_replicate: bool, level: str,
                     ft: str, analysis: str = "sample", other: str = "size",
                     site: str = "", delta: int = 0) -> pd.DataFrame:
    """
    Create the figure of interest.

    :param dataframe: the distribution of codon coverage.
    :param ctrl:  The control sample or size
    :param codon: the name of a codon
    :param output_stat: an output dir for stat files.
    :param output_diag: an output dir for diag figures
    :param output_fig: an output dir for seaborn figure
    :param use_replicate: True to use replicate, false else
    :param level: The level of interest, 'aa' or 'codon'
    :param ft: Current ft of interest
    :param analysis: The kind of analysis to make (sample or size).
    :param site: The site of interest sought in footprints. This parameter \
    is only used if peak_analysis is False.
    :param delta: The number of codon around the site `site`. This parameter \
    is only used if peak_analysis is False
    """
    sns.set()
    print(f"Working on {codon}")
    df = creating_prop_table(dataframe, analysis)
    g = sns.catplot(y="proportion", x=analysis, hue="replicate", kind="bar",
                    ci=None, data=df, height=6, aspect=1.5,
                    estimator=np.mean)
    clevel = "amino acid" if level == "aa" else level
    if delta == 0:
        location = f"in ribosome {site} site"
    elif delta > 0:
        location = f"in ribosome {site} site +/- {delta} codons"
    else:
        location = f"in entire footprints"
    g.set_ylabels(f"Proportion of {clevel} {codon} {location}")
    if analysis == "sample":
        g.set_xlabels(f"Samples")
        title = f'Mean proportion of {clevel} {codon} {location} ' \
                f'between samples\n' \
                f'(for footprints of size {ft})'
    else:
        g.set_xlabels(f"Footprint size interval")
        title = f'Mean proportion of {clevel} {codon} {location} ' \
                f'\nbetween different footprint sizes ' \
                f'for sample {ft}'
    plt.subplots_adjust(top=0.9)
    g.fig.suptitle(title)
    df_stat = glm_stats(df, codon, output_diag, output_stat,
                        use_replicate, analysis, "glm_logit_fp", False)
    mtuple = get_stat_annot_tuple(df_stat)
    if len(mtuple) < 7:
        ax = g.axes[0, 0]
        add_stat_annotation(ax, data=df, x=analysis, y="proportion",
                            hue="replicate", boxPairList=mtuple)
    plt.savefig("%s/%s_figure.pdf" % (output_fig, codon))
    plt.clf()
    plt.close()
    df.rename({'proportion': 'ncount'}, axis=1, inplace=True)
    df['ft'] = [codon] * df.shape[0]
    df['ft_type'] = [level] * df.shape[0]
    df[other] = [ft] * df.shape[0]
    return make_delta_df(df, ctrl, analysis, df_stat)


def create_common_peak_figure(dataframe: pd.DataFrame,
                              output_stat: Path, output_diag: Path,
                              output_fig: Path, use_replicate: bool,
                              cur_ft: str, ctrl: str,
                              analysis: str = "sample") -> None:
    """
    Create the figure of interest.

    :param dataframe: the distribution of codon coverage.
    :param codon: the name of a codon
    :param output_stat: an output dir for stat files.
    :param output_diag: an output dir for diag figures
    :param output_fig: an output dir for seaborn figure
    :param use_replicate: True to use replicate, false else
    :param cur_ft: The current feature to analyse
    :param ctrl: The control sample
    :param analysis: The kind of analysis to make (sample or size).
    """
    sns.set()
    g = sns.catplot(y="peak", x=analysis, hue="replicate", kind="bar",
                    ci=None, data=dataframe, height=6, aspect=1.5,
                    estimator=np.mean)
    g.set_ylabels("Proportion of common peaks")
    if analysis == "sample":
        g.set_xlabels(f"Samples vs {ctrl} sample")
        title = f'Proportion of common ribosome profiling peaks ' \
                f'between samples\nrelative to the same control {ctrl} ' \
                f'(for footprints of size {cur_ft})'
    else:
        g.set_xlabels(f"Footprint size interval")
        tmp = cur_ft.replace(f'-{ctrl}', '')
        title = f'Proportion of common ribosome profiling peaks ' \
                f'between sample {tmp}\nrelative to {ctrl} ' \
                f'for different footprint sizes'
    plt.subplots_adjust(top=0.9)
    g.fig.suptitle(title)
    df_stat = glm_stats(dataframe, cur_ft, output_diag, output_stat,
                        use_replicate, analysis, "glm_logit")
    mtuple = get_stat_annot_tuple(df_stat)
    if len(mtuple) < 7:
        ax = g.axes[0, 0]
        add_stat_annotation(ax, data=dataframe, x=analysis, y="peak",
                            hue="replicate", boxPairList=mtuple)
    plt.savefig("%s/%s_figure.pdf" % (output_fig, cur_ft))
    plt.clf()
    plt.close()


if __name__ == "__main__":
    doctest.testmod()