#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: make glm statistical analysis
"""

from rpy2.robjects import r, pandas2ri
import pandas as pd
from pathlib import Path


def handle_diagnostics_figure(codon: str, n: int) -> str:
    """
    return a tryCatch to try making diagnostics figure.

    :param codon: The current codon (or amino acid) of interest
    :param n: The n parameter of simulateResiduals function in DHARMa
    :return: The tryCatch to use
    """
    mycatch = """
    myError <- FALSE
    tryCatch({
            simulationOutput <- simulateResiduals(fittedModel = mlm, n = %s)
            png(paste(output, "/%s_dignostics", ".png", sep=""), 
            height=1080, width=1920)
            par(mfrow=c(1, 2))
            plot(simulationOutput)
            dev.off()
        }, error = function(cond){
            print('Error on diag figure with n = %s and codon = %s')
            print(cond)
            myError <- TRUE})""" % (n, codon, n, codon)
    return mycatch


def glm_stats(dataframe: pd.DataFrame, codon: str,
              output_diag: Path, output_stat: Path,
              replicate: bool, analysis: str = "sample",
              stat: str = "glm_nb", diag: bool = True) -> pd.DataFrame:
    """
    Perform a glm nb test on ``dataframe``.

    :param dataframe: The table containing codon counts in peaks
    :param codon: The codon of interest
    :param output_diag: Folder where the diagnostic figure will be created
    :param output_stat: Folder where the stat file will be stored
    :param replicate: True to take into account replicate, false else
    :param analysis: The kind of analysis to make (sample or size)
    :param stat: glm_nb to make a negative binomial glm (default) or \
    glm_logistic to make a logistic glm.
    :param diag: True to make diagnostics figure, False else.
    """
    rep = "+ replicate" if replicate else ""
    if stat == "glm_nb":
        model = "mlm <- glm.nb(count ~ %s + codon_in_peak %s, data=data)" % \
                (analysis, rep)
    elif stat == "glm_logit":
        model = """mlm <- glm(peak ~ %s %s, family=binomial(link = 'logit'),
                              data=data)""" % (analysis, rep)
    else:
        model = """mlm <- glm(proportion ~ %s %s, 
                              family=binomial(link = 'logit'),
                              data=data)""" % (analysis, rep)
    catch500 = handle_diagnostics_figure(codon, 500)
    catch2000 = handle_diagnostics_figure(codon, 2000)
    diag_str = ""
    if diag:
        diag_str = """
        %s
        if(myError) {
            %s
        }""" % (catch500, catch2000)
    pandas2ri.activate()
    stat_s = r("""
    require("MASS")
    require("DHARMa")
    require("multcomp")
    function(data, output){
        data$%s <- as.factor(data$%s)
        %s
        %s
        s <- summary(glht(mlm, mcp(%s = "Tukey")))
        tab <- as.data.frame(cbind(names(s$test$coefficients), 
                                   s$test$coefficients, s$test$sigma, 
                                   s$test$tstat, s$test$pvalues))
        colnames(tab) <- c("Comparison", "Estimate", "Std.Error", 
                           "z.value", "Pr(>|z|)")
        return(tab)
    }
               """ % (analysis, analysis, model, diag_str, analysis))
    df_stats = stat_s(dataframe, str(output_diag))
    pandas2ri.deactivate()
    df_stats.to_csv("%s/%s_stat.txt" % (output_stat, codon),
                    sep="\t", index=False)
    return df_stats


def get_stat_annot_tuple(df_stat):
    """
    Get the list of tuple that will be used to display the result in \
    a seaborn figure.

    :param df_stat: (pandas Dataframe) a dataframe.
    :return: (list of tuple)
    """
    list_res = []
    for i in range(df_stat.shape[0]):
        groups = df_stat.iloc[i, 0].split(" - ")
        pval = float(df_stat.iloc[i, 4].replace(",", "."))
        groups.append(pval)
        list_res.append(groups)
    return list_res
