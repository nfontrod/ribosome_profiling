FROM python:3.8-slim
LABEL maintainer="Nicolas Fontrodona <nicolas.fontrodona@ens-lyon.fr"

RUN apt-get update \
    && apt-get install -y r-base \
    && apt-get -y install libssl-dev \
    && apt-get -y install libcurl4-openssl-dev \
    && apt-get -y install libgit2-dev \
    && apt-get -y install libssh2-1-dev \
    && apt-get -y install libxml2-dev \
    && apt-get -y install pigz \
    && apt-get install -y libnlopt-dev

RUN Rscript -e 'install.packages("devtools")'

RUN echo 'require("devtools") \n\
    install_version("multcomp", version="1.4-12") \n\
    install_version("MASS", version="7.3-49") \n\
    install_version("mgcv", version="1.8-31") \n\
    install_version("DHARMa", version="0.3.1")' \
    | sed 's/ //g' > /tmp/pkg.R \
    && Rscript /tmp/pkg.R

RUN pip install matplotlib==3.1.2 \
    && pip install numpy==1.19.2 \
    && pip install pandas==1.0.3 \
    && pip install seaborn==0.10.1 \
    && pip install rpy2==3.3.3 \
    && pip install lazyparser

COPY script/ /script

RUN useradd statUser \
    && chown -R statUser /script

USER statUser
ENV PYTHONPATH="/"

CMD ["bash"]