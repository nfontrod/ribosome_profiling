#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create a file bed like file containing introns and their \
ORF offset based on teh previous exons provided their not spliced.
"""

from pathlib import Path
from typing import Dict, Tuple, List, Any, Union, Optional
import pandas as pd
import lazyparser as lp
from loguru import logger


COUNTER = {"multiple_CDS": [0, 0],
           "CDS_end_mismatch": [0, 0],
           "missing_intron": [0, 0]}


class BadExonCoupleError(Exception):
    pass


def convert(my_list: List[str]) -> List[Any]:
    """
    Turn every str digit into a int digit.

    :param my_list: A str list
    :return: The same list with int type for digit and str type for other \
    types

    >>> convert(['a', 'b123', '145'])
    ['a', 'b123', 145]
    """
    new_list = []
    for ft in my_list:
        if ft.isdigit():
            new_list.append(int(ft))
        else:
            new_list.append(ft)
    return new_list


def read_annotation_bed(bed: Path) -> Tuple[Dict, Dict]:
    """
    Create two dictionary linking each gene to its CDS or exons.

    :param bed: A bed file
    :return: A dictionary containing a bed file

    >>> mfile = Path(__file__).parent / "tests" / "files" / "annot_test.bed"
    >>> exond, cdsd = read_annotation_bed(mfile)
    >>> exond == {'OR4F5': [['OR4F5', 0, 15, 'exon-OR4F5_1', '.', '+'],
    ...                     ['OR4F5', 101, 155, 'exon-OR4F5_2', '.', '+'],
    ...                     ['OR4F5', 3618, 6167, 'exon-OR4F5_3', '.', '+']],
    ...           'OR4F29': [['OR4F29', 0, 939, 'exon-OR4F29_1', '.', '+']],
    ...           'DDX5': [['DDX5', 577, 791, 'exon-DDX5_3', '.', '+'],
    ...                    ['DDX5', 929, 1121, 'exon-DDX5_4', '.', '+']]}
    True
    >>> cdsd == {'OR4F5': [['OR4F5', 146, 155, 'CDS-OR4F5_1', 0, '+'],
    ...                    ['OR4F5', 3618, 4587, 'CDS-OR4F5_2', 0, '+']],
    ...          'OR4F29': [['OR4F29', 0, 936, 'CDS-OR4F29_1', 0, '+']],
    ...          'DDX5': [['DDX5', 747, 791, 'CDS-DDX5_1', 0, '+'],
    ...                   ['DDX5', 1077, 1121, 'CDS-DDX5_2', 0, '+']]}
    True
    """
    with bed.open("r") as bedin:
        dic_exon, dic_cds = ({}, {})
        for line in bedin:
            cline = line.strip().split("\t")[0:6]
            cline = convert(cline)
            if cline[3].startswith("exon-"):
                if cline[0] in dic_exon:
                    dic_exon[cline[0]].append(cline)
                else:
                    dic_exon[cline[0]] = [cline]
            elif cline[3].startswith("CDS-"):
                if cline[0] in dic_cds:
                    dic_cds[cline[0]].append(cline)
                else:
                    dic_cds[cline[0]] = [cline]
    return dic_exon, dic_cds


def is_included(ft1: List, ft2: List) -> bool:
    """
    Return True if ft1 is included in ft2.

    :param ft1: The feature 1 (corresponding to a bed line)
    :param ft2: The feature 2 (corresponding to a bed line)
    :return: True if ft1 is included in ft2 false else

    >>> ft_1 = ["LOL", 5, 10, 'lol1', '.', '+']
    >>> ft_2 = ["LOL", 0, 20, 'lol1', '.', '+']
    >>> is_included(ft_1, ft_2)
    True
    >>> is_included(ft_2, ft_1)
    False
    >>> ft_2 = ["LOL", 6, 20, 'lol1', '.', '+']
    >>> is_included(ft_1, ft_2)
    False
    >>> ft_2 = ["LOL", 5, 20, 'lol1', '.', '+']
    >>> is_included(ft_1, ft_2)
    True
    >>> ft_2 = ["LOL", 0, 10, 'lol1', '.', '+']
    >>> is_included(ft_1, ft_2)
    True
    >>> ft_2 = ["LOL", 0, 9, 'lol1', '.', '+']
    >>> is_included(ft_1, ft_2)
    False
    """
    return ft1[0] == ft2[0] and \
        ft2[1] <= ft1[1] <= ft2[2] and \
        ft2[1] <= ft1[2] <= ft2[2] and \
        ft2[5] == ft1[5]


def merge_exon_cds(exon_list: List[List], cds_list: List[List]
                   ) -> List[Tuple[List, List]]:
    """
    Links each exons to their CDS that containing them.

    :param exon_list: A list of exons from the same gene
    :param cds_list: A list of CDS from the same gene
    :return: link each exons to it's CDS

    >>> el = [["LOL", 0, 10, 'lol1', '.', '+'],
    ...       ["LOL", 30, 40, 'lol2', '.', '+'],
    ...       ["LOL", 50, 60, 'lol3', '.', '+']]
    >>> cl = [["LOL", 5, 7, 'lol1', '.', '+'],
    ...       ["LOL", 35, 37, 'lol2', '.', '+'],
    ...       ["LOL", 55, 57, 'lol3', '.', '+']]
    >>> merge_exon_cds(el, cl) == [
    ... (['LOL', 0, 10, 'lol1', '.', '+'], ['LOL', 5, 7, 'lol1', '.', '+']),
    ... (['LOL', 30, 40, 'lol2', '.', '+'], ['LOL', 35, 37, 'lol2', '.', '+']),
    ... (['LOL', 50, 60, 'lol3', '.', '+'], ['LOL', 55, 57, 'lol3', '.', '+'])]
    True
    >>> cl = [["LOL", 5, 20, 'lol1', '.', '+'],
    ...       ["LOL", 35, 37, 'lol2', '.', '+'],
    ...       ["LOL", 55, 57, 'lol3', '.', '+']]
    >>> merge_exon_cds(el, cl) == [
    ... (['LOL', 0, 10, 'lol1', '.', '+'],),
    ... (['LOL', 30, 40, 'lol2', '.', '+'], ['LOL', 35, 37, 'lol2', '.', '+']),
    ... (['LOL', 50, 60, 'lol3', '.', '+'], ['LOL', 55, 57, 'lol3', '.', '+'])]
    True
    """
    result = []
    global COUNTER
    for exon in exon_list:
        chosen = False
        for cds in cds_list:
            if is_included(cds, exon):
                if chosen:
                    COUNTER['multiple_CDS'][0] += 1
                    break
                COUNTER['multiple_CDS'][1] += 1
                result.append((exon, cds))
                chosen = True
        if not chosen:
            result.append((exon,))
    return result


def sort_exons(list_exon: List[List]) -> List[List]:
    """
    Sort the list of exons coming from the same gene together.

    :param list_exon: A list of exons of interest.
    :return: The same list of exons sorted

    >>> el = [["LOL", 50, 60, 'lol3', '.', '+'],
    ...       ["LOL", 30, 40, 'lol2', '.', '+'],
    ...       ["LOL", 0, 10, 'lol1', '.', '+']]
    >>> sort_exons(el) == [['LOL', 0, 10, 'lol1', '.', '+'],
    ...                    ['LOL', 30, 40, 'lol2', '.', '+'],
    ...                    ['LOL', 50, 60, 'lol3', '.', '+']]
    True
    """
    return pd.DataFrame(list_exon).sort_values([0, 1, 2]).values.tolist()


def merge_dic(exon_dic: Dict, cds_dic: Dict) -> Dict:
    """
    Link every exons to it's corresponding CDS.

    :param exon_dic: A dictionary containing exons linked to their gene
    :param cds_dic: A dictionary containing CDS linked to their gene
    :return: A dictionary where every exons linked to their gene are \
    linked to their CDS

    >>> ed = {"LOL": [["LOL", 50, 60, 'lol3', '.', '+'],
    ...               ["LOL", 30, 40, 'lol2', '.', '+'],
    ...               ["LOL", 0, 10, 'lol1', '.', '+']],
    ...       "TEST": [["TEST", 50, 60, 'TEST1', '.', '+']],
    ...       "BOU": [["BOU", 50, 60, 'BOU1', '.', '+']]}
    >>> cd = {"LOL": [["LOL", 5, 7, 'lol1', '.', '+'],
    ...               ["LOL", 35, 37, 'lol2', '.', '+'],
    ...               ["LOL", 55, 57, 'lol3', '.', '+']],
    ...       "TEST": [["TEST", 51, 60, 'TEST1', '.', '+']]}
    >>> merge_dic(ed, cd) == {
    ... 'LOL': [(['LOL', 0, 10, 'lol1', '.', '+'],
    ...          ['LOL', 5, 7, 'lol1', '.', '+']),
    ...         (['LOL', 30, 40, 'lol2', '.', '+'],
    ...          ['LOL', 35, 37, 'lol2', '.', '+']),
    ...         (['LOL', 50, 60, 'lol3', '.', '+'],
    ...          ['LOL', 55, 57, 'lol3', '.', '+'])],
    ... 'TEST': [(['TEST', 50, 60, 'TEST1', '.', '+'],
    ...           ['TEST', 51, 60, 'TEST1', '.', '+'])]}
    True
    """
    new_dic = {}
    for gene in exon_dic:
        if gene in cds_dic:
            exon_list = sort_exons(exon_dic[gene])
            cds_list = cds_dic[gene]
            new_dic[gene] = merge_exon_cds(exon_list, cds_list)
    return new_dic


def intron_location(exon1: List, exon2: List) -> List:
    """
    Create an intron based on the two exons surrounding it.

    :param exon1: The first exons
    :param exon2: The second exons
    :return: The intron
    >>> intron_location(["LOL", 0, 10, 'lol1', '.', '+'],
    ...                 ["LOL", 30, 40, 'lol2', '.', '+'])
    ['LOL', 10, 30, 'intron_temp', '.', '+']
    >>> intron_location(["LOL", 30, 40, 'lol2', '.', '+'],
    ...                 ["LOL", 0, 10, 'lol1', '.', '+'])
    []
    """
    global COUNTER
    COUNTER["missing_intron"][1] += 1
    if exon1[0] != exon2[0] or \
       exon1[2] >= exon2[1] or \
       exon1[5] != exon2[5]:
        COUNTER["missing_intron"][0] += 1
        return []
    return [exon1[0], exon1[2], exon2[1], "intron_temp", '.', exon1[5]]


def intron_offset(previous_exon: List, previous_cds: List) -> Union[int, str]:
    """
    Return the potential offset of an intron, based on the previous exons.

    :param previous_exon: A list in a bed format containing the exon \
    before the intron
    :param previous_cds: A list at a bed format containing the CDS of the \
    previous intron.
    :return: The intron offset

    >>> intron_offset(["LOL", 0, 10, 'lol1', '.', '+'],
    ... ["LOL", 0, 10, 'lol1', 0, '+'])
    2
    >>> intron_offset(["LOL", 0, 10, 'lol1', '.', '+'],
    ... ["LOL", 0, 10, 'lol1', 1, '+'])
    0
    >>> intron_offset(["LOL", 0, 10, 'lol1', '.', '+'],
    ... ["LOL", 0, 10, 'lol1', 2, '+'])
    1
    >>> intron_offset(["LOL", 0, 10, 'lol1', '.', '+'],
    ... ["LOL", 0, 8, 'lol1', 2, '+'])
    '.'
    """
    global COUNTER
    COUNTER["CDS_end_mismatch"][1] += 1
    if previous_cds[2] != previous_exon[2]:
        COUNTER["CDS_end_mismatch"][0] += 1
        return '.'
    if isinstance(previous_cds[4], int):
        dic_off = {0: 0, 1: 2, 2: 1}
        return dic_off[(previous_cds[2] -
                        (previous_cds[1] + previous_cds[4])) % 3]
    else:
        return '.'


def add_name(intron_list: List[List]) -> List[List]:
    """
    Add the intron name.

    :param intron_list: List of intron
    :return: The list of intron with their name

    >>> il = [['LOL', 30, 40, 'intron_temp', '.', '+'],
    ...       ['LOL', 20, 30, 'intron_temp', '.', '+']]
    >>> res = [['LOL', 20, 30, 'intron-LOL_1', '.', '+'],
    ...        ['LOL', 30, 40, 'intron-LOL_2', '.', '+']]
    >>> add_name(il) == res
    True
    >>> il = [['LOL', 20, 30, 'intron_temp', '.', '+'],
    ...       ['LOL', 30, 40, 'intron_temp', '.', '+']]
    >>> add_name(il) == res
    True
    """
    intron_list = sort_exons(intron_list)
    for i, intron in enumerate(intron_list):
        intron[3] = f"intron-{intron[0]}_{i + 1}"
    return intron_list


def unpack_function(exon_list: List[Tuple[List, List]], pos: int
                    ) -> Tuple[List, List, Optional[List], Optional[List]]:
    """
    Unpack two element in exon_list.

    :param exon_list: The list of exons of interest
    :param pos: position within the intron list
    :return: The exons at pos and after pos and their respective cds \
    if they have one.

    >>> ex_l = [(['LOL', 0, 10, 'lol1', '.', '+'],
    ...          ['LOL', 0, 10, 'lol1', 0, '+']),
    ...         (['LOL', 30, 40, 'lol2', '.', '+'],
    ...          ['LOL', 30, 40, 'lol2', 0, '+']),
    ...         (['LOL', 50, 60, 'lol3', '.', '+'],)]
    >>> unpack_function(ex_l, 0) == (['LOL', 0, 10, 'lol1', '.', '+'],
    ... ['LOL', 30, 40, 'lol2', '.', '+'],
    ... ['LOL', 0, 10, 'lol1', 0, '+'],
    ... ['LOL', 30, 40, 'lol2', 0, '+'])
    True
    >>> unpack_function(ex_l, 1) == (['LOL', 30, 40, 'lol2', '.', '+'],
    ... ['LOL', 50, 60, 'lol3', '.', '+'],
    ... ['LOL', 30, 40, 'lol2', 0, '+'], None)
    True
    """
    cds1 = None
    cds2 = None
    if len(exon_list[pos]) == 2:
        exon1, cds1 = exon_list[pos]
    else:
        exon1 = exon_list[pos][0]
    if len(exon_list[pos + 1]) == 2:
        exon2, cds2 = exon_list[pos + 1]
    else:
        exon2 = exon_list[pos + 1][0]
    return exon1, exon2, cds1, cds2


def create_gene_intron_content(exon_list: List[Tuple[List, List]]
                               ) -> List[List]:
    """
    Create a list of list of intron.

    :param exon_list: From a list of tuple containing an exon \
    and it's CDS return it's list of intron.
    :return: The list of intron

    >>> ex_l = [(['LOL', 0, 10, 'lol1', '.', '+'],
    ...          ['LOL', 0, 10, 'lol1', 0, '+']),
    ...         (['LOL', 30, 40, 'lol2', '.', '+'],
    ...          ['LOL', 30, 40, 'lol2', 0, '+']),
    ...         (['LOL', 50, 60, 'lol3', '.', '+'],
    ...          ['LOL', 50, 60, 'lol3', 0, '+'])]
    >>> res = [['LOL', 10, 30, 'intron-LOL_1', 2, '+', 'lol1', 'lol1'],
    ... ['LOL', 40, 50, 'intron-LOL_2', 2, '+', 'lol2', 'lol2']]
    >>> create_gene_intron_content(ex_l) == res
    True
    """
    content = []
    for i in range(len(exon_list) - 1):
        exon1, exon2, cds1, cds2 = unpack_function(exon_list, i)
        intron = intron_location(exon1, exon2)
        if intron:
            if cds1 is not None:
                intron[4] = intron_offset(exon1, cds1)
                intron += [exon1[3], cds1[3]]
            else:
                intron += [exon1[3], None]
            content.append(intron)
    if content:
        return add_name(content)
    return []


def create_intron_content(merged_dic: Dict[str, List[Tuple[List, List]]]
                          ) -> List[List]:
    """
    Create a list of list of intron.

    :param merged_dic:  A dictionary where every exons linked to their gene\
     are linked to their CDS
    :return: The list of intron

    >>> ex_l = {'LOL': [(['LOL', 0, 10, 'lol1', '.', '+'],
    ...                  ['LOL', 0, 10, 'lol1', 0, '+']),
    ...                 (['LOL', 30, 40, 'lol2', '.', '+'],
    ...                  ['LOL', 30, 40, 'lol2', 0, '+']),
    ...                 (['LOL', 50, 60, 'lol3', '.', '+'],
    ...                  ['LOL', 50, 60, 'lol3', 0, '+'])],
    ...          'TEST': [(['TEST', 0, 10, 'TEST1', '.', '+'],
    ...                    ['TEST', 0, 10, 'TEST1', 0, '+']),
    ...                   (['TEST', 30, 40, 'TEST2', '.', '+'],
    ...                    ['TEST', 30, 40, 'TEST2', 0, '+'])]}
    >>> res = [['LOL', 10, 30, 'intron-LOL_1', 2, '+', 'lol1', 'lol1'],
    ...        ['LOL', 40, 50, 'intron-LOL_2', 2, '+', 'lol2', 'lol2'],
    ...        ['TEST', 10, 30, 'intron-TEST_1', 2, '+', 'TEST1', 'TEST1']]
    >>> create_intron_content(ex_l) == res
    True
    """
    content = []
    for gene, value in merged_dic.items():
        content += create_gene_intron_content(value)
    return content


def write_res(content: List[List], output: Path, name: str) -> None:
    """
    Write the list of introns into a file.

    :param content: The list of introns
    :param output: Folder where the file will be created
    :param name: name of the output file
    """
    df = pd.DataFrame(content)
    df.to_csv(output / name, sep="\t", index=False, header=False)


def get_final_content(annotation: str) -> List[List]:
    """
    Create the content of bed file containing introns.

    :param annotation: A bed file containing, exons, cds and start \
    and stop codon .
    :return: The final content of the intron bed file

    >>> mfile = Path(__file__).parent / "tests" / "files" / "annot_test.bed"
    >>> get_final_content(mfile) == [
    ... ['OR4F5', 15, 101, 'intron-OR4F5_1', '.', '+', 'exon-OR4F5_1', None],
    ... ['OR4F5', 155, 3618, 'intron-OR4F5_2', 0, '+', 'exon-OR4F5_2',
    ... 'CDS-OR4F5_1'], ['DDX5', 791, 929, 'intron-DDX5_1', 1, '+',
    ... 'exon-DDX5_3', 'CDS-DDX5_1']]
    True
    """
    dic_exon, dic_cds = read_annotation_bed(Path(annotation))
    merged_dic = merge_dic(dic_exon, dic_cds)
    return create_intron_content(merged_dic)


@lp.parse(annotation="file")
def create_intron_file(annotation: str, output: str, name: str) -> None:
    """
    Create the bed file containing introns.

    :param annotation: A bed file containing, exons, cds and start \
    and stop codon .
    :param output: Folder where the result will be created
    :param name: The name of the output file
    """
    intron_content = get_final_content(annotation)
    for k in COUNTER:
        logger.warning(f"{k}: {COUNTER[k][0]} / {COUNTER[k][1]}")
    write_res(intron_content, Path(output), name)


if __name__ == "__main__":
    create_intron_file()
