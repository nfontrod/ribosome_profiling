#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: create a periodicity table
"""

from typing import List, Dict, Tuple, Union, Optional
import numpy as np
import pandas as pd
import pysam
from pathlib import Path
import lazyparser as lp
from create_intron_file import convert
from loguru import logger


class BadExonIntronError(Exception):
    pass


def load_intron_bed(bed: str) -> Dict[str, List]:
    """
    Load a bed file containing intron and their potential ORF if \
    it exists.

    :param bed: A bed fine containing introns
    :return: A dictionary containing the introns of each gene

    >>> mfile = str(Path(__file__).parent) + "/tests/files/intron_test.bed"
    >>> load_intron_bed(mfile) == {
    ... 'exon-OR4F5_2': ['OR4F5', 155, 3618, 'intron-OR4F5_2', 0, '+'],
    ... 'exon-DDX5_3': ['DDX5', 791, 929, 'intron-DDX5_3', 1, '+']}
    True
    """
    dic = {}
    with open(bed, "r") as bed_file:
        for line in bed_file:
            nline = line.strip().split("\t")
            if nline[4] != '.':
                if nline[6] is not None and nline[6] not in dic:
                    dic[nline[6]] = convert(nline[0:6])
                else:
                    msg = f"{nline[6]} should not be None or duplicated !"
                    logger.exception(msg)
                    raise ValueError(msg)
    return dic


def load_exon_bed(bed: str) -> Dict[str, List]:
    """
    Load a dictionary from a file containing cds, exons and start/stop codons.

    :param bed: A bed file containing containing cds, exons and \
    start/stop codons.
    :return: A dictionary of exons located in the bed file

    >>> mfile = str(Path(__file__).parent) + "/tests/files/annot_test.bed"
    >>> load_exon_bed(mfile) == {
    ... 'exon-OR4F5_1': ['OR4F5', 0, 15, 'exon-OR4F5_1', '.', '+'],
    ... 'exon-OR4F5_2': ['OR4F5', 101, 155, 'exon-OR4F5_2', '.', '+'],
    ... 'exon-OR4F5_3': ['OR4F5', 3618, 6167, 'exon-OR4F5_3', '.', '+'],
    ... 'exon-OR4F29_1': ['OR4F29', 0, 939, 'exon-OR4F29_1', '.', '+'],
    ... 'exon-DDX5_3': ['DDX5', 577, 791, 'exon-DDX5_3', '.', '+'],
    ... 'exon-DDX5_4': ['DDX5', 929, 1121, 'exon-DDX5_4', '.', '+']}
    True
    """
    dic = {}
    with open(bed, "r") as bed_file:
        for line in bed_file:
            nline = line.strip().split("\t")
            if nline[3].startswith('exon-'):
                if nline[3] is not None and nline[3] not in dic:
                    dic[nline[3]] = convert(nline[0:6])
                else:
                    msg = f"{nline[3]} should not be None or duplicated !"
                    logger.exception(msg)
                    raise ValueError(msg)
    return dic


def merge_intron_exon(exon: List, intron: List) -> Optional[List]:
    """
    Merges an intron and a exon together.

    :param exon: A exon
    :param intron: A intron
    :return: list corresponding to genomic intervals composed \
    by [gene_name, start_exon, end_following_intron,
        pos_first_codon_bas_intron, offset]

    >>> merge_intron_exon(['OR4F5', 101, 155, 'exon-OR4F5_2', '.', '+'],
    ... ['OR4F5', 155, 3618, 'intron-OR4F5_2', 0, '+'])
    ['OR4F5', 101, 3618, 155, 0]
    >>> merge_intron_exon(['OR4F5', 101, 155, 'exon-OR4F5_2', '.', '+'],
    ... ['OR4F5', 155, 3618, 'intron-OR4F5_2', 1, '+'])
    ['OR4F5', 101, 3618, 156, 1]
    >>> merge_intron_exon(['OR4F5', 101, 155, 'exon-OR4F5_2', '.', '+'],
    ... ['OR4F5', 155, 156, 'intron-OR4F5_2', 1, '+'])
    >>> merge_intron_exon(['OR4F', 101, 155, 'exon-OR4F5_2', '.', '+'],
    ... ['OR4F5', 155, 3618, 'intron-OR4F5_2', 1, '+'])
    Traceback (most recent call last):
    ...
    intron_periodicity_table.BadExonIntronError: exon: \
['OR4F', 101, 155, 'exon-OR4F5_2', '.', '+'] and intron: \
['OR4F5', 155, 3618, 'intron-OR4F5_2', 1, '+'] should be on the \
same gene/strand
    """
    if exon[0] != intron[0] or exon[5] != intron[5] != "+":
        msg = f'exon: {exon} and intron: {intron} should be on the same ' \
              f'gene/strand'
        logger.exception(msg)
        raise BadExonIntronError(msg)
    if intron[2] - intron[1] < 30:
        return None
    return [exon[0], exon[1], intron[2], intron[1] + intron[4], intron[4]]


def merge_dic_intron_exon(dic_exon: Dict[str, List],
                          dic_intron: Dict[str, List]) -> List[List]:
    """
    Merge the dictionary containing exons and the dictionary containing \
    introns.

    :param dic_exon: A dictionary containing exons
    :param dic_intron: A dictionary containing introns
    :return: A list of list corresponding to genomic intervals composed \
    by [gene_name, start_exon, end_following_intron,
        pos_first_codon_bas_intron, offset]

    >>> d_intron = {
    ... 'exon-OR4F5_2': ['OR4F5', 155, 3618, 'intron-OR4F5_2', 0, '+']}
    >>> d_exon = {
    ... 'exon-OR4F5_1': ['OR4F5', 0, 15, 'exon-OR4F5_1', '.', '+'],
    ... 'exon-OR4F5_2': ['OR4F5', 101, 155, 'exon-OR4F5_2', '.', '+'],
    ... 'exon-OR4F5_3': ['OR4F5', 3618, 6167, 'exon-OR4F5_3', '.', '+']}
    >>> merge_dic_intron_exon(d_exon, d_intron) == [
    ... ['OR4F5', 101, 3618, 155, 0]]
    True
    """
    intervals = []
    for k, intron in dic_intron.items():
        exon = dic_exon[k]
        tmp = merge_intron_exon(exon, intron)
        if tmp is not None:
            intervals.append(merge_intron_exon(exon, intron))
    return intervals


def create_empty_vector(interval: List, window_exon: int, window_intron: int,
                        output: str = "array"
                        ) -> Union[np.array, Tuple[int, int]]:
    """
    Create a vector containing zero value and np.NaN when positions \
    are out of exons.

    :param interval: list corresponding to genomic intervals composed \
    by [gene_name, start_exon, end_following_intron,
        pos_first_codon_bas_intron, offset]
    :param window_exon: The number of nucleotide to show before \
    the first nucleotide of the first intron 'codon'.
    :param window_intron: The number of nucleotide to show after \
    the first nucleotide of the first intron 'codon'.
    :param output: array to return an array else return an interval of value
    :return: vector containing zero value and np.NaN when positions \
    are out of exons if output = "array" else the new interval of value \
    in the wanted window

    >>> create_empty_vector(['OR4F5', 101, 3618, 155, 0], 25, 25)
    array([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
           0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
           0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.])
    >>> create_empty_vector(['OR4F5', 10, 15, 12, 0], 10, 10)
    array([nan, nan, nan, nan, nan, nan, nan, nan,  0.,  0.,  0.,  0.,  0.,
           nan, nan, nan, nan, nan, nan, nan, nan])
    >>> create_empty_vector(['OR4F5', 10, 15, 12, 0], 7, 9)
    array([nan, nan, nan, nan, nan,  0.,  0.,  0.,  0.,  0., nan, nan, nan,
           nan, nan, nan, nan])
    >>> create_empty_vector(['OR4F5', 101, 3618, 155, 0], 25, 25, 'interval')
    [130, 181]
    >>> create_empty_vector(['OR4F5', 101, 3618, 155, 0], 10, 30, 'interval')
    [145, 186]
    >>> create_empty_vector(['OR4F5', 10, 15, 12, 0], 10, 10, 'interval')
    [10, 15]
    """
    codon_loc = interval[3]
    diff_beg = abs(interval[3] - interval[1])
    diff_end = abs(interval[3] - interval[2] + 1)
    na_beg = na_end = 0
    if diff_beg < window_exon:
        na_beg = abs(window_exon - diff_beg)
    if diff_end < window_intron:
        na_end = abs(window_intron - diff_end)
    size_null = window_exon + window_intron + 1 - na_beg - na_end
    if output == "array":
        return np.concatenate([np.full(na_beg, np.nan),
                               np.zeros(size_null),
                               np.full(na_end, np.nan)])
    else:
        return [codon_loc - window_exon + na_beg,
                codon_loc + window_intron + 1 - na_end]


def create_first_codon_dic(dic, tmp_dic, interval, loc, positions) -> Dict:
    """
    update a dictionary that shows the coverage around the first codon of \
    retained introns with the coverage of this region for a particular intron.

    :param dic: dictionary that shows the coverage around the first codon of \
    retained introns
    :param tmp_dic: Dictionary containing the coverage of the first \
    base of footprints (or their p,A,E site).
    :param interval: The region around the first base of a codon in introns.
    :param loc: The position of the first codon base in an intron
    :param positions: List of position around the first codon base in a \
    intron considered.
    :return: The update dictionary

    >>> d = {x: [] for x in ['gene', 'offset', 'loc', 'fp_size', 'pos_-1',
    ... 'pos_0', 'pos_1']}
    >>> td = {20: [4, 1, 2], 21: [9, 0, 5], 22: [7, 6, 0]}
    >>> inter = ["g1", 10, 20, 16, 1]
    >>> create_first_codon_dic(d, td, inter, inter[3], [-1, 0, 1]) == {
    ... 'gene': ['g1', 'g1', 'g1'], 'offset': [1, 1, 1], 'loc': [16, 16, 16],
    ... 'fp_size': [20, 21, 22], 'pos_-1': [4, 9, 7], 'pos_0': [1, 0, 6],
    ... 'pos_1': [2, 5, 0]}
    True
    """
    for fp_size, value in tmp_dic.items():
        dic["gene"].append(interval[0])
        dic["offset"].append(interval[4])
        dic["loc"].append(loc)
        dic["fp_size"].append(fp_size)
        for i, pos in enumerate(positions):
            dic[f"pos_{pos}"].append(value[i])
    return dic


def create_table(bam: str, intervals: List[List], window_exon: int,
                 window_intron: int,
                 base_name: str, offset: Dict) -> pd.DataFrame:
    """

    :param bam: A bam file containing footprint
    :param intervals: A list of list corresponding to genomic intervals \
    composed by [gene_name, start_exon, end_following_intron,
    pos_first_codon_bas_intron, offset]
    :param window_exon: The number of nucleotide to show before \
    the first nucleotide of the first intron 'codon'.
    :param window_intron: The number of nucleotide to show after \
    the first nucleotide of the first intron 'codon'.
    :param base_name: The basename of the bam file
    :param offset: A dictionary linking footprint size to the offset of \
    it's p-site
    :return: A dataframe showing the regions around the first base \
    of the first potential codon in introns
    """
    bf = pysam.AlignmentFile(bam, "rb")
    positions = list(range(-window_exon, window_intron + 1))
    dic_tmp = {"gene": [], "offset": [], "loc": [], "fp_size": []}
    dic = dict(dic_tmp, **{f"pos_{pos}": [] for pos in positions})
    for interval in intervals:
        loc = interval[3]
        index_ref = list(range(loc - window_exon, loc + window_intron + 1))
        ni = create_empty_vector(interval, window_exon - 5, window_intron + 5,
                                 'interval')
        tmp_dic = {}
        for fp in bf.fetch(interval[0], ni[0], ni[1]):
            fp_len = fp.query_length
            if fp.is_unmapped or fp_len not in offset:
                continue
            pos = fp.reference_start - fp.query_alignment_start + \
                offset[fp_len]
            if pos not in index_ref:
                continue
            if fp_len not in tmp_dic:
                tmp_dic[fp_len] = create_empty_vector(interval, window_exon,
                                                      window_intron, 'array')
                if len(tmp_dic[fp_len]) != len(index_ref) != len(positions):
                    raise IndexError(
                        f"the length of index ({len(index_ref)}), "
                        f"the length of vect ({len(tmp_dic[fp_len])}), "
                        f"and the length of positions ({len(positions)}) "
                        f"should be the same")
            tmp_dic[fp_len][index_ref.index(pos)] += 1
        dic = create_first_codon_dic(dic, tmp_dic, interval, loc, positions)
    dic['sample'] = [base_name] * len(dic["gene"])
    return pd.DataFrame(dic)


def create_count_table(df: pd.DataFrame) -> pd.DataFrame:
    """
    Create a dataframe containing the sum of the 5'end number of \
    footprints.

    :param df: A dataframe showing the start location of every footprint starts
    positions.
    :return: A dataframe containing the suof the 5'end number of \
    footprints.

    >>> d = pd.DataFrame({"loc": [10, 10, 11, 11], 'offset': [0, 1, 0, 2],
    ... "sample": ["s1", "s1", "s1", "s1"], 'fp_size': [20, 21, 20, 21],
    ... "pos_0": [1, 2, 3, 4]})
    >>> create_count_table(d)
       offset  fp_size sample  pos_0
    0       0       20     s1      4
    1       1       21     s1      2
    2       2       21     s1      4
    >>> d = pd.DataFrame({"loc": [10, 10, 11, 11], 'offset': [0, 1, 0, 2],
    ... "sample": ["s1", "s1", "s1", "s1"], 'fp_size': [20, 21, 23, 21],
    ... "pos_0": [1, 2, 3, 4]})
    >>> create_count_table(d)
       offset  fp_size sample  pos_0
    0       0       20     s1      1
    1       0       23     s1      3
    2       1       21     s1      2
    3       2       21     s1      4
    """
    return df.drop('loc', axis=1).\
        groupby(['offset', 'fp_size', 'sample']).sum().reset_index()


def tot_count(bam: str) -> int:
    """
    return the totla number of footprint in the bam
    :param bam: a ban file
    :return: The total number of footprint

    >>> mfile = str(Path(__file__).parent) + "/tests/files/test_DDX5.bam"
    >>> tot_count(mfile)
    20
    """
    bf = pysam.AlignmentFile(bam, "rb")
    return sum(1 for fp in bf.fetch() if not fp.is_unmapped)


def create_dic(offset: str) -> Dict:
    """
    Return a dict for each footprint size.

    :param offset: A table containing offset for each size of footprint.
    :return: A dictionary indicating the footprint offset to the \
    p-site for each footprint size

    >>> mfile = str(Path(__file__).parent) + "/tests/files/test_offset.txt"
    >>> create_dic(mfile)
    {20: 12, 23: 12, 30: 12, 31: 12, 32: 13, 33: 12, 34: 13, 37: 14}
    """
    df = pd.read_csv(offset, sep="\t")
    df.index = df['fp_size']
    df.drop('fp_size', inplace=True, axis=1)
    return df.to_dict()['offset']


def create_full_tables(bam: str, bed_intron: str, bed_exon: str,
                       window_exon: int, window_intron: int,
                       offset: str, base_name: str
                       ) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Create periodicity figures.

    :param bam: A bam file of aligned features
    :param bed_intron: A bed file containing intron
    :param bed_exon: A bed file containing exon, cds, start and stop location \
    of every gene used to map the footprints in bam parameter
    :param window_exon: The number of nucleotide to show before \
    the first nucleotide of the first intron 'codon'.
    :param window_intron: The number of nucleotide to show after \
    the first nucleotide of the first intron 'codon'.
    :param offset: An offset table.
    :param base_name: base name of the bed file

    >>> mfile = str(Path(__file__).parent) + "/tests/files/test_DDX5.bam"
    >>> intron_f = str(Path(__file__).parent) + "/tests/files/intron_test.bed"
    >>> exon_file = str(Path(__file__).parent) + "/tests/files/annot_test.bed"
    >>> p, s = create_full_tables(mfile, intron_f, exon_file, 25, 25, '',
    ... 'test_DDX5')
    >>> s[["offset", "fp_size"] + [f"pos_{i}"
    ... for i in range(-24, -19)]]
       offset  fp_size  pos_-24  pos_-23  pos_-22  pos_-21  pos_-20
    0       1       20      1.0      0.0      0.0      0.0      5.0
    1       1       23      0.0      1.0      0.0      0.0      0.0
    2       1       25      0.0      0.0      0.0      0.0      0.0
    3       1       28      0.0      0.0      0.0      0.0      0.0
    4       1       31      0.0      0.0      0.0      0.0      0.0
    5       1       32      0.0      0.0      0.0      0.0      0.0
    6       1       33      0.0      0.0      0.0      0.0      0.0
    7       1       37      0.0      0.0      0.0      0.0      0.0
    >>> p[['sample', 'gene', 'offset', 'loc', 'fp_size', 'pos_-20']]
          sample  gene  offset  loc  fp_size  pos_-20
    0  test_DDX5  DDX5       1  792       20      5.0
    1  test_DDX5  DDX5       1  792       23      0.0
    2  test_DDX5  DDX5       1  792       37      0.0
    3  test_DDX5  DDX5       1  792       33      0.0
    4  test_DDX5  DDX5       1  792       32      0.0
    5  test_DDX5  DDX5       1  792       28      0.0
    6  test_DDX5  DDX5       1  792       31      0.0
    7  test_DDX5  DDX5       1  792       25      0.0

    >>> test_f = str(Path(__file__).parent) + "/tests/files/test_offset.txt"
    >>> p, s = create_full_tables(mfile, intron_f, exon_file, 25, 25, test_f,
    ... 'test_DDX5')
    >>> s[["offset", "fp_size"] + [f"pos_{i}"
    ... for i in range(-12, -7)]]
       offset  fp_size  pos_-12  pos_-11  pos_-10  pos_-9  pos_-8
    0       1       20      1.0      0.0      0.0     0.0     5.0
    1       1       23      0.0      1.0      0.0     0.0     0.0
    2       1       31      0.0      0.0      0.0     0.0     0.0
    3       1       32      0.0      0.0      0.0     0.0     0.0
    4       1       33      0.0      0.0      0.0     0.0     0.0
    5       1       37      0.0      0.0      0.0     0.0     0.0
    >>> p, s = create_full_tables(mfile, intron_f, exon_file, 5, 10, '',
    ... 'test_DDX5')
    >>> s.columns
    Index(['offset', 'fp_size', 'sample', 'gene', 'pos_-5', 'pos_-4', 'pos_-3',
           'pos_-2', 'pos_-1', 'pos_0', 'pos_1', 'pos_2', 'pos_3', 'pos_4',
           'pos_5', 'pos_6', 'pos_7', 'pos_8', 'pos_9', 'pos_10', \
'total_count'],
          dtype='object')
    """
    offset = {i: 0 for i in range(200)} if offset == '' else create_dic(offset)
    dic_intron = load_intron_bed(bed_intron)
    dic_exon = load_exon_bed(bed_exon)
    intervals = merge_dic_intron_exon(dic_exon, dic_intron)
    df_pos = create_table(bam, intervals, window_exon, window_intron,
                          base_name, offset)
    df_sum = create_count_table(df_pos)
    tot = tot_count(bam)
    df_sum['total_count'] = [tot] * len(df_sum['fp_size'])
    return df_pos, df_sum


@lp.parse(bam="file", annot="file", window_exon=range(101),
          window_intron=range(5, 101))
def make_periodicity(bam: str, bed_intron: str, bed_exon: str,
                     window_exon: int,
                     window_intron: int,
                     output: str = ".", offset: str = "") -> None:
    """
    Create periodicity figures.

    :param bam: A bam file of aligned features
    :param bed_intron: A bed file containing intron
    :param bed_exon: A bed file containing exon, cds, start and stop location \
    of every gene used to map the footprints in bam parameter
    :param window_exon: The number of nucleotide to show before \
    the first nucleotide of the first intron 'codon'.
    :param window_intron: The number of nucleotide to show after \
    the first nucleotide of the first intron 'codon'.
    :param output: Folder where the table will be created
    :param offset: An offset table.
    """
    base_name = Path(bam).name.replace("_sorted.bam", "").replace(".bam", "")
    df_pos, df_sum = create_full_tables(bam, bed_intron, bed_exon, window_exon,
                                        window_intron, offset, base_name)
    df_pos.to_csv(Path(output) / f"tmp_fp_position_{base_name}.txt", sep="\t",
                  index=False)
    df_sum.to_csv(Path(output) / f"sum_fp_position_{base_name}.txt", sep="\t",
                  index=False)


if __name__ == "__main__":
    make_periodicity()
