#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: From a periodicity table create a periodicity figure
"""


import lazyparser as lp
import seaborn as sns
import matplotlib.ticker as ticker
import pandas as pd
from pathlib import Path
from typing import Union, List


def make_figure(df: pd.DataFrame, size: List[int],
				outfile: str, offset: str,
				normalize: bool, output: Path = ".") -> None:
	"""

	:param df: A dataframe containing the start positions of footprint around \
	start or stop codon
	:param size: The range of size of footprint of interest
	:param outfile: The name of output file
	:param output: Folder where the figure will be created
	:param offset: The number of nucleotides before the first nucleotides \
	of the first codon of intron if they are not spliced.
	:param normalize: True is the count are normalized, false else.
	"""
	sns.set()
	df2 = df.melt(id_vars=["sample"],
				  value_vars=[p for p in df.columns if "pos_" in p])
	df2["variable"] = df2["variable"].str.replace("pos_", "").astype(int)
	df2 = df2.sort_values("variable")
	g = sns.relplot(x="variable", y="value", data=df2, height=12, aspect=1.77,
					kind="line", hue="sample")
	g.ax.set_xticklabels(g.ax.get_xticklabels(), rotation=90, ha='center')
	g.ax.xaxis.set_major_locator(ticker.MultipleLocator(3))
	g.ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
	title = f"Periodicity figure for footprint of size {size[0]}-{size[1]}\n" \
			f"around first base of first intron 'codon' (offset {offset})"
	g.fig.suptitle(title)
	g.set_ylabels("Footprint number (per million reads)"
				  if normalize else "Footprint number")
	g.set_xlabels(f"Positions around the first base of first intron 'codon'")
	g.savefig(output / outfile)


def select_wanted_lines(df: pd.DataFrame, offset: str, size: List[int],
						samples: Union[str, List[str]]) -> pd.DataFrame:
	"""
	Filter the count dataframe

	:param df: A dataframe containing the start positions of footprint around \
	start or stop codon
	:param offset: The number of nucleotides before the first nucleotides \
	of the first codon of intron if they are not spliced.
	:param size: The range of size of footprint of interest
	:param samples: The list of sample to keep
	"""
	if samples != "__all__":
		df = df.loc[df["sample"].isin(samples), :].copy()
	df = df.loc[(df["fp_size"] >= size[0]) & (df["fp_size"] <= size[1]),
		 :]
	if offset != "all":
		df = df.loc[df["offset"] == int(offset), :]
	return df.drop(['fp_size', 'offset'], axis=1).\
		groupby('sample').sum().reset_index()


def normalize_table(df: pd.DataFrame) -> pd.DataFrame:
	"""
	Normalize the read counts.

	:param df: A dataframe containing the start positions of footprint around \
	start or stop codon
	:return: the dataframe normalized
	"""
	interest_col = [p for p in df.columns if 'pos_' in p]
	for col in interest_col:
		df[col] = df[col] / (df['total_count'] / 1000000)
	return df


@lp.parse(table="file", offset=["0", "1", "2", "all"])
def create_figure(table: str, offset: str = "all",
				  size: List[int] = (0, 50), samples: List[str] = ("__all__"),
				  normalize: str = 'n', output: str = "."):
	"""
	Create a periodicity figure.

	:param table: A table containing the count of every footprints around \
	start and stop codon.
	:param offset: The number of nucleotides before the first nucleotides \
	of the first codon of intron if they are not spliced. (choose from '0', \
	'1', '2' or 'all').
	:param size: The range of footprint size to represent. (default [0,50])
	:param samples: The samples list for which we want to test periodicity. \
	(default [__all__] to analyse every samples).
	:param normalize: 'y' to represent footprints per million (default 'n')
	:param output: Folder where the results will be created. (default .)
	"""
	samples = "__all__" if "__all__" in samples else list(samples)
	df = pd.read_csv(table, sep="\t")
	fdf = select_wanted_lines(df, offset, size, samples)
	if fdf.empty:
		print(f"Warning: The dataframe is empty with the parameters: "
			  f"{offset} - {size} - {samples}")
	else:
		normalize = normalize.lower() == 'y'
		norm_name = "_norm" if normalize else ""
		outfile = f"periodicity_offset-{offset}_fp_{size[0]}-{size[1]}" \
				  f"{norm_name}.pdf"
		if normalize:
			fdf = normalize_table(fdf)
		make_figure(fdf, size, outfile, offset, normalize, Path(output))


if __name__ == "__main__":
	create_figure()