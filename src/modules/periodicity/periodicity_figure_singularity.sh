#!/bin/bash

if [[ -z "$@" ]]; then
  singularity exec bin/lbmc-fp_periodicity-0.0.img python3 /script/periodicity_figure.py -h
else
  singularity exec -B $PWD bin/lbmc-fp_periodicity-0.0.img python3 /script/periodicity_figure.py "$@"
fi