#!/bin/bash

if [[ -z "$@" ]]; then
  docker container run --rm lbmc/fp_periodicity:0.0 python3 /script/periodicity_figure.py -h
else
  docker container run --rm -v $PWD:/host -w /host lbmc/fp_periodicity:0.0 python3 /script/periodicity_figure.py $@
fi