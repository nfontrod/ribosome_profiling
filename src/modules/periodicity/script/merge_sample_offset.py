#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to merge the offset table by sample
"""

import lazyparser as lp
import pandas as pd


@lp.parse(offset_table="file")
def merge_offset(offset_table: str) -> None:
    """
    Create a table corresponding of the mean offset per footprint size \
    across sample.

    :param offset_table: An offset table
    """
    df = pd.read_csv(offset_table, sep="\t")
    num_sample = len(df['sample'].unique())
    good_len = [mlen for mlen in df['fp_size'].unique()
                if df[df['fp_size'] == mlen].shape[0] == num_sample]
    df = df[df['fp_size'].isin(good_len)].copy()
    df = df.groupby(['fp_size']).mean().reset_index()
    df['offset'] = df['offset'].round().astype(int)
    df.to_csv("general_offset_table.txt", sep="\t", index=False)


if __name__ == "__main__":
    merge_offset()
