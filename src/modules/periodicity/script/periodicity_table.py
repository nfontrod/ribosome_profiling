#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: create a periodicity table
"""

from typing import List, Dict, Tuple, Union, Optional
from itertools import product
import doctest
import numpy as np
import pandas as pd
import pysam
from pathlib import Path
import lazyparser as lp


def load_bed(bed: str) -> Dict[str, Dict]:
    """
    Load a bed file containing cds, exons, start and stop codon for each gene.

    :param bed:   bed file containing exon, cds, start and stop location \
    of every gene
    :return: A dictionary containing the exons, the start and stop codon of \
    each gene
    """
    dic = {}
    with open(bed, "r") as bed_file:
        for line in bed_file:
            nline = line.replace("\n", "").split("\t")
            if nline[0] not in dic:
                dic[nline[0]] = {"exons": [], "starts": [], "stops": [],
                                 "CDS": []}
            if "exon-" in nline[3]:
                key = "exons"
            elif "start_codon" in nline[3]:
                key = "starts"
            elif "stop_codon" in nline[3]:
                key = "stops"
            elif "CDS-" in nline[3]:
                key = "CDS"
            else:
                key = None
            if key is not None:
                dic[nline[0]][key].append([nline[3], int(nline[1]),
                                           int(nline[2]), nline[4]])
    return dic


def is_hosted(codon: List, exon: List) -> bool:
    """
    Check if codon is hosted by exon.

    :param codon: A codon identified by its name, start and stop \
    positions
    :param exon: An exon identified by its name, start and stop \
    positions
    :return: True if exon contains the codon false else.

    >>> is_hosted(["codon1", 100, 103], ["exon1", 50, 150])
    True
    >>> is_hosted(["codon1", 100, 103], ["exon1", 150, 200])
    False
    """
    return exon[1] <= codon[1] < exon[2] and \
        exon[1] < codon[2] <= exon[2]


def cds_in_frame(codon: List, kind: str, exon: List, cds: List) -> bool:
    """
    True if the codon start or stop are located in frame, False else.

    :param codon: a codon start or stop
    :param kind: starts for start codon, stops for stops codon.
    :param exon: An exon containing the CDS
    :param cds: A CDS
    :return: True if the CDS as not multiple frame and fit to the exons \
    size and codon start and stop position
    """
    if cds[3] == "multiple":
        return False
    if kind == "starts":
        if cds[2] == exon[2] and cds[1] == codon[1]:
            return True
    elif kind == "stops":
        if cds[1] == exon[1] and cds[2] + 3 == codon[2]:
            return True
    return False


def dic_transform(dic_bed: Dict[str, Dict]) -> Dict[str, Dict]:
    """
    Change the dictionary of dic bed to a dictionary containing \
    for each start/stop codon the coordinate of exons containing it.

    :param dic_bed: A dictionary containing the exons, the start and \
    stop codon of each gene
    :return: A dictionary that contains the exons hosting each start or stop \
    codon

    >>> d = {"ORF45": {"exons": [["exon1", 0, 100], ["exon2", 200, 300]],
    ...                "starts": [["start1", 50, 53], ["start2", 203, 206]],
    ...                "stops": [["stop1", 70, 73], ["stop2", 233, 236]]}}
    >>> dic_transform(d)
    {'ORF45': {'starts': {50: [0, 100], 203: [200, 300]}, 'stops': {70: [0, 100], 233: [200, 300]}}}
    """
    overlap_0 = 0
    overlap_many = 0
    overlap_0_cds = 0
    overlap_many_cds = 0
    no_cds_fit_1_cds_overlap = 0
    correct_codon_start = 0
    correct_codon_stop = 0
    dic = {}
    prod = product(dic_bed.keys(), ["starts", "stops"])
    for gene, ft in prod:
        for codon in dic_bed[gene][ft]:
            overlapping_ft = [exon for exon in dic_bed[gene]["exons"] if
                              is_hosted(codon, exon)]
            if len(overlapping_ft) != 1:
                if not overlapping_ft:
                    overlap_0 += 1
                else:
                    overlap_many += 1
                if len(overlapping_ft) > 1:
                    sizes = [cft[2] - cft[1] for cft in overlapping_ft]
                    overlapping_ft = [overlapping_ft[sizes.index(max(sizes))]]
            if gene not in dic.keys():
                dic[gene] = {"starts": {}, "stops": {}}
            if len(overlapping_ft) != 0:
                overlapping_cds = [cds for cds in dic_bed[gene]["CDS"] if
                                   is_hosted(cds, overlapping_ft[0])]
                if len(overlapping_cds) == 0:
                    overlap_0_cds += 1
                elif len(overlapping_cds) == 1:
                    if cds_in_frame(codon, ft, overlapping_ft[0],
                                    overlapping_cds[0]):
                        if ft == "starts":
                            correct_codon_start += 1
                        elif ft == "stops":
                            correct_codon_stop += 1
                        dic[gene][ft][codon[1]] = overlapping_ft[0][1:]
                    else:
                        no_cds_fit_1_cds_overlap += 1
                else:
                    overlap_many_cds += 1
    print(f"Warning: Found {overlap_0} codon(s) start/stop overlapping 0 "
          f"exons.")
    print(f"Warning: Found {overlap_many} codon(s) overlapping more than "
          f"one exons (in that case the largest exon is selected).")
    print(f"Warning: Found {overlap_0_cds} codons(s) in an exon that is not "
          f"overlapping any cds")
    print(f"Warning: Found {overlap_many_cds} codon(s) in an exon that "
          f"overlaps many CDS")
    print(f"Warning: Found {no_cds_fit_1_cds_overlap} that have no cds correct"
          f" fit")
    print(f"Warning: Found {correct_codon_start} good start codons")
    print(f"Warning: Found {correct_codon_stop} good stop codons")
    return dic


def create_empty_vector(gene: str, window: int,
                        codon_loc: int,
                        codon_type: str,
                        codon_dic: Dict,
                        output: str = "array"
                        ) -> Union[List, Tuple[int, int]]:
    """
    Create a vector containing zero value and np.NaN when positions \
    are out of exons.

    :param gene: A gene name
    :param window: The window half size
    :param codon_dic: A dictionary that contains the exons hosting each \
    start or stop codon
    :param codon_type: The type of codon (starts or stops)
    :param codon_loc: The location of the start/stop codon.
    :param output: array to return an array else return an interval of value
    :return: vector containing zero value and np.NaN when positions \
    are out of exons if output = "array" else the new interval of value \
    in the wanted window

    >>> d = {'ORF45': {'starts': {50: [0, 100], 200: [195, 205]},
    ... 'stops': {50: [0, 100], 203: [200, 300]}}}
    >>> create_empty_vector("ORF45", 25, 50, "starts", d)
    array([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
           0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
           0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.])
    >>> create_empty_vector("ORF45", 10, 200, "starts", d)
    array([nan, nan, nan, nan, nan,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
            0.,  0., nan, nan, nan, nan, nan, nan])
    >>> create_empty_vector("ORF45", 10, 200, "starts", d, 'interval')
    [195, 205]
    >>> create_empty_vector("ORF45", 25, 50, "starts", d, "interval")
    [25, 76]
    """
    exon = codon_dic[gene][codon_type][codon_loc]
    diff_beg = abs(codon_loc - exon[0])
    diff_end = abs(codon_loc - exon[1] + 1)
    na_beg = na_end = 0
    if diff_beg < window:
        na_beg = abs(window - diff_beg)
    if diff_end < window:
        na_end = abs(window - diff_end)
    size_null = window * 2 + 1 - na_beg - na_end
    if output == "array":
        return list(np.concatenate([np.full(na_beg, np.nan),
                                    np.zeros(size_null),
                                    np.full(na_end, np.nan)]))
    else:
        return [codon_loc - window + na_beg,
                codon_loc + window + 1 - na_end]


def create_table(bam: str, dic_codon: Dict[str, Dict], window: int,
                 base_name: str, offset: Dict) -> pd.DataFrame:
    """

    :param bam: A bam file containing footprint
    :param dic_codon: A dictionary that contains the exons hosting each \
     start or stop codon
    :param window: A half window size
    :param base_name: The basename of the bam file
    :param offset: A dictionary linking footprint size to the offset of \
    it's p-site
    :return: A dataframe showing the start location of every footprint starts
    positions
    """
    bf = pysam.AlignmentFile(bam, "rb")
    prod = product(dic_codon.keys(), ["starts", "stops"])
    positions = list(range(-window, window + 1))
    dic_tmp = {"gene": [], "ft": [], "loc": [], "fp_size": []}
    dic = dict(dic_tmp, **{f"pos_{pos}": [] for pos in positions})
    for gene, ft in prod:
        for loc in dic_codon[gene][ft]:
            index_ref = list(range(loc - window, loc + window + 1))
            ni = create_empty_vector(gene, window, loc, ft,
                                     dic_codon, 'interval')
            tmp_dic = {}
            for fp in bf.fetch(gene, ni[0], ni[1]):
                if fp.is_unmapped:
                    continue
                fp_len = fp.query_length
                if fp_len not in offset:
                    continue
                pos = fp.reference_start - fp.query_alignment_start + \
                      offset[fp_len]
                if pos not in index_ref:
                    continue
                if fp_len not in tmp_dic:
                    tmp_dic[fp_len] = create_empty_vector(gene, window, loc,
                                                          ft, dic_codon,
                                                          'array')
                    if len(tmp_dic[fp_len]) != len(index_ref) or \
                            len(tmp_dic[fp_len]) != len(positions):
                        raise IndexError(
                            f"the length of index ({len(index_ref)}), "
                            f"the length of vect ({len(tmp_dic[fp_len])}), "
                            f"and the length of positions ({len(positions)}) "
                            f"should be the same")
                tmp_dic[fp_len][index_ref.index(pos)] += 1
            for fp_size, value in tmp_dic.items():
                dic["gene"].append(gene)
                dic["ft"].append(ft)
                dic["loc"].append(loc)
                dic["fp_size"].append(fp_size)
                for i, pos in enumerate(positions):
                    dic[f"pos_{pos}"].append(value[i])
    dic['sample'] = [base_name] * len(dic["gene"])
    return pd.DataFrame(dic)


def create_count_table(df: pd.DataFrame) -> pd.DataFrame:
    """
    Create a dataframe containing the sum of the 5'end number of \
    footprints.

    :param df: A dataframe showing the start location of every footprint starts
    positions.
    :return: A dataframe containing the suof the 5'end number of \
    footprints.
    """
    return df.drop('loc', axis=1).\
        groupby(['ft', 'fp_size', 'sample']).sum().reset_index()


def get_fp_count(bam: str) -> pd.DataFrame:
    """
    Create a dataframe containing the count of each footprint size.

    :param bam: A bam file
    :return: A table with 2 columns, one containing the footprint size and \
    the other the number of footprint detected.
    """
    d = {}
    bf = pysam.AlignmentFile(bam, "rb")
    for fp in bf.fetch():
        if fp.query_length not in d:
            d[fp.query_length] = 1
        else:
            d[fp.query_length] += 1
    d2 = {"fp_size": [], "total_count": []}
    for k, v in d.items():
        d2['fp_size'].append(k)
        d2['total_count'].append(v)
    return pd.DataFrame(d2)

def tot_count(bam: str) -> int:
    """
    return the totla number of footprint in the bam
    :param bam: a ban file
    :return: The total number of footprint
    """
    count = 0
    bf = pysam.AlignmentFile(bam, "rb")
    for fp in bf.fetch():
        if not fp.is_unmapped:
            count += 1
    return count


def peak_finder(mserie: pd.Series) -> Optional[int]:
    """
    Location of the peak density

    :return: the density peak around 12 nt before start or stop codon \
    if the number of read for this footprint size represent at least
    """
    start = 10
    stop = 16
    good_col = [d for d in mserie.keys() if 'pos' in d]
    val = mserie['tot_line'] / mserie['tot_sum'] * 100
    if val < 3:
        return None
    density = mserie[[f'pos_-{i}' for i in range(start, stop)]].to_list()
    return density.index(max(density)) + start


def get_density_peak(df_sum: pd.DataFrame) -> pd.DataFrame:
    """
    Find the density peak for each footprint size before the start and stop \
    codon

    :param df_sum: A dataframe containing the transcript sum
    :return: The density peak of each footprint size
    """
    good_col = [d for d in df_sum.columns if 'pos' in d]
    df_sum['tot_line'] = df_sum.apply(lambda x: sum(x[good_col].to_list()),
                                      axis=1)
    tmp = df_sum[['ft', 'sample', 'tot_line']].copy()
    tmp = tmp.groupby(['ft', 'sample']).sum().reset_index()
    tmp.rename({'tot_line': 'tot_sum'}, inplace=True, axis=1)
    df_sum = df_sum.merge(tmp, on=['ft', 'sample'], how="left")
    df_sum['offset'] = df_sum.apply(peak_finder, axis=1)
    return df_sum.loc[(df_sum['ft'] == 'starts') & (-df_sum['offset'].isna()),
                      ['fp_size', 'sample', 'offset']]


def create_dic(offset: str) -> Dict:
    """
    Return a dict for each footprint size.

    :param offset: A table containing offset for each size of footprint.
    :return: A dictionary indicating the footprint offset to the \
    p-site for each footprint size
    """
    df = pd.read_csv(offset, sep="\t")
    df.index = df['fp_size']
    df.drop('fp_size', inplace=True, axis=1)
    return df.to_dict()['offset']


@lp.parse(bam="file", annot="file", window=range(5, 101))
def make_periodicity(bam: str, annot: str, window: int,
                     output: str = ".", offset: str = "") -> None:
    """
    Create periodicity figures.

    :param bam: A bam file of aligned features
    :param annot: A bed file containing exon, cds, start and stop location \
    of every gene used to map the footprints in bam parameter
    :param window: The half size of the window
    :param output: Folder where the table will be created
    :param offset: An offset table.
    """
    no_offset = True if offset == '' else False
    offset = {i: 0 for i in range(200)} if offset == '' else create_dic(offset)
    dic = load_bed(annot)
    codon_location = dic_transform(dic)
    base_name = Path(bam).name.replace("_sorted.bam", "").replace(".bam", "")
    df_pos = create_table(bam, codon_location, window, base_name, offset)
    df_sum = create_count_table(df_pos)
    tot = tot_count(bam)
    #df_sum = df_sum.merge(df_count, how="left", on="fp_size")
    df_sum['total_count'] = [tot] * len(df_sum['fp_size'])
    df_pos.to_csv(Path(output) / f"tmp_fp_position_{base_name}.txt", sep="\t",
                  index=False)
    df_sum.to_csv(Path(output) / f"sum_fp_position_{base_name}.txt", sep="\t",
                  index=False)
    if no_offset:
        df_offset = get_density_peak(df_sum)
        df_offset.to_csv(Path(output) / f"offset_{base_name}.txt", sep="\t",
                         index=False)


if __name__ == "__main__":
    # doctest.testmod()
    make_periodicity()
