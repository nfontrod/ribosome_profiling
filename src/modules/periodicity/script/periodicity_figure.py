#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: From a periodicity table create a periodicity figure
"""


import lazyparser as lp
import seaborn as sns
import matplotlib.ticker as ticker
import pandas as pd
from pathlib import Path
from typing import Union, List


def make_figure(df: pd.DataFrame, size: List[int],
				outfile: str, location: str,
				normalize: bool, output: Path = ".") -> None:
	"""

	:param df: A dataframe containing the start positions of footprint around \
	start or stop codon
	:param size: The range of size of footprint of interest
	:param outfile: The name of output file
	:param output: Folder where the figure will be created
	:param normalize: True is the count are normalized, false else.
	"""
	sns.set()
	df2 = df.melt(id_vars=["ft", "sample"],
				  value_vars=[p for p in df.columns if "pos_" in p])
	# df3 = df2.loc[(df2["ft"] == "starts") & (df2["fp_size"] == 33), :].copy()
	df2["variable"] = df2["variable"].str.replace("pos_", "").astype(int)
	df2 = df2.sort_values("variable")
	g = sns.relplot(x="variable", y="value", data=df2, height=12, aspect=1.77,
					kind="line", hue="sample")
	g.ax.set_xticklabels(g.ax.get_xticklabels(), rotation=90, ha='center')
	g.ax.xaxis.set_major_locator(ticker.MultipleLocator(3))
	g.ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
	title = f"Periodicity figure for footprint of size {size[0]}-{size[1]}\n" \
			f"around {location} codon."
	g.fig.suptitle(title)
	g.set_ylabels("Footprint number (per million reads)"
				  if normalize else "Footprint number")
	g.set_xlabels(f"Positions around the first base of the {location} codon")
	g.savefig(output / outfile)


def select_wanted_lines(df: pd.DataFrame, location: str,
						size: List[int],
						samples: Union[str, List[str]]) -> pd.DataFrame:
	"""

	:param df: A dataframe containing the start positions of footprint around \
	start or stop codon
	:param location: starts or stops
	:param size: The range of size of footprint of interest
	:param samples: The list of sample to keep
	"""
	if samples != "__all__":
		df = df.loc[df["sample"].isin(samples), :].copy()
	df = df.loc[(df["fp_size"] >= size[0]) & (df["fp_size"] <= size[1]) &
				(df["ft"] == location), :].copy()
	return df.drop('fp_size', axis=1).\
		groupby(['ft', 'sample']).sum().reset_index()


def normalize_table(df: pd.DataFrame) -> pd.DataFrame:
	"""
	Normalize the read counts.

	:param df: A dataframe containing the start positions of footprint around \
	start or stop codon
	:return: the dataframe normalized
	"""
	interest_col = [p for p in df.columns if 'pos_' in p]
	for col in interest_col:
		df[col] = df[col] / (df['total_count'] / 1000000)
	return df


@lp.parse(table="file", codon=["start", "stop"])
def create_figure(table: str, codon: str = "start",
				  size: List[int] = (0, 50), samples: List[str] = ("__all__"),
				  normalize: str = 'n', output: str = "."):
	"""
	Create a periodicity figure.

	:param table: A table containing the count of every footprints around \
	start and stop codon.
	:param codon: 'start' to create the periodicity figure around start codons,
	'stops' to create the periodicity figure around 'stops' codons. \
	(default start)
	:param size: The range of footprint size to represent. (default [0,50])
	:param samples: The samples list for which we want to test periodicity. \
	(default [__all__] to analyse every samples).
	:param normalize: 'y' to represent footprints per million (default 'n')
	:param output: Folder where the results will be created. (default .)
	"""
	normalize = True if normalize.lower() == 'y' else False
	samples = "__all__" if "__all__" in samples else list(samples)
	df = pd.read_csv(table, sep="\t")
	codon += "s"
	fdf = select_wanted_lines(df, codon, size, samples)
	if fdf.empty:
		print(f"Warning: The dataframe is empty with the parameters: "
			  f"{codon} - {size} - {samples}")
	else:
		norm_name = "_norm" if normalize else ""
		outfile = f"periodicity_{codon[:-1]}_codons_fp_{size[0]}-{size[1]}" \
				  f"{norm_name}.pdf"
		if normalize:
			fdf = normalize_table(fdf)
		make_figure(fdf, size, outfile, codon[:-1], normalize, Path(output))


if __name__ == "__main__":
	create_figure()