#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Peak caller used to check for peak coverage in CDS region \
of ribosome footprints between 2 conditions data while taking into account \
the mRNA levels in those regions
"""
import sys
import lazyparser as lp
from typing import List, Dict, Tuple, Optional
import pandas as pd
from pathlib import Path
import numpy as np
import pyBigWig as pbw
import multiprocessing as mp
from functools import reduce
import warnings


with warnings.catch_warnings():
    warnings.filterwarnings('ignore', r'All-NaN (slice|axis) encountered')


class ConditionNotFound(Exception):
    pass


def build_dataframe(bw: List[str], conditions: List[str],
                    experiments: List[str], replicates: List[str]
                    ) -> pd.DataFrame:
    """
    Build a table containing the experimental design.

    :param bw: A list of bigwig coverage files
    :param conditions: A list fo condition, must be in the same order \
    as in bigwig
    :param experiments: A list saying if a bigwig is from a Ribo-seq \
    or RNA-seq experiment.
    :param replicates: The list of replicate of interest
    :return: The dataframe containing the experimental design

    >>> build_dataframe([f"f{x}.bw" for x in range(1, 9)],
    ... ["C1", "C1", "C2", "C2"] * 2,
    ... ["RIBO"] * 4 + ["RNA"] * 4,
    ... ["R1", "R2"] * 4)
          bw conditions experiments replicates
    0  f1.bw         C1        RIBO         R1
    1  f2.bw         C1        RIBO         R2
    2  f3.bw         C2        RIBO         R1
    3  f4.bw         C2        RIBO         R2
    4  f5.bw         C1         RNA         R1
    5  f6.bw         C1         RNA         R2
    6  f7.bw         C2         RNA         R1
    7  f8.bw         C2         RNA         R2
    """
    return pd.DataFrame({"bw": bw, "conditions": conditions,
                         "experiments": experiments,
                         "replicates": replicates})


def check_conditions(design: pd.DataFrame, test: str, ctrl: str) -> None:
    """
    Error if the design is malformed, otherwise nothing happens.

    :param design: The dataframe containing the experimental design
    :param test: The name of the test condition
    :param ctrl: The name of the control condition

    >>> d = build_dataframe([f"f{x}.bw" for x in range(1, 9)],
    ... ["C1", "C1", "C2", "C2"] * 2,
    ... ["RIBO"] * 4 + ["RNA"] * 4,
    ... ["R1", "R2"] * 4)
    >>> check_conditions(d, "C1", "C2")
    >>> check_conditions(d, "C1", "C3")
    Traceback (most recent call last):
    ...
    ConditionNotFound: C3 is not inside the conditions in experimental design !
    >>> d = build_dataframe([f"f{x}.bw" for x in range(1, 9)],
    ... ["C1", "C2", "C2", "C2"] * 2,
    ... ["RIBO"] * 4 + ["RNA"] * 4,
    ... ["R1", "R2"] * 4)
    >>> check_conditions(d, "C1", "C2")
    Traceback (most recent call last):
    ...
    ValueError: Wrong number of couples conditions-replicates in the design \
table
    """
    for v in [ctrl, test]:
        if v not in design["conditions"].to_list():
            raise ConditionNotFound(f"{v} is not inside the conditions "
                                    f"in experimental design !")
    tmp = design.groupby(["conditions", "replicates"]).count().reset_index()
    tmp = tmp[tmp["bw"] != 2]
    if not tmp.empty:
        raise ValueError("Wrong number of couples conditions-replicates in "
                         "the design table")


def filter_dataframe(df: pd.DataFrame, test: str, ctrl: str) -> pd.DataFrame:
    """
    Return a dataframe containing only the conditions of interest.

    :param df: A design dataframe
    :param test: The test condition
    :param ctrl: The control condition
    :return: The dataframe containing only the condition of interest

    >>> d = build_dataframe([f"f{x}.bw" for x in range(1, 13)],
    ... ["C1", "C1", "C2", "C2", "C3", "C3"] * 2,
    ... ["RIBO"] * 6 + ["RNA"] * 6,
    ... ["R1", "R2"] * 6)
    >>> filter_dataframe(d, "C2", "C1")
           bw conditions experiments replicates
    0   f1.bw         C1        RIBO         R1
    1   f2.bw         C1        RIBO         R2
    2   f3.bw         C2        RIBO         R1
    3   f4.bw         C2        RIBO         R2
    6   f7.bw         C1         RNA         R1
    7   f8.bw         C1         RNA         R2
    8   f9.bw         C2         RNA         R1
    9  f10.bw         C2         RNA         R2
    """
    return df[df["conditions"].isin([test, ctrl])]


def get_test_and_ctrl_replicate(df: pd.DataFrame, replicate: str, test: str,
                                ctrl: str) -> Dict[str, str]:
    """
    get the test and the control bigwig files for ribo \
     and RNA-seq experiments for a given replicate.

    :param df: A dataframe containing the experimental design
    :param replicate: The replicate of interest
    :param test: The test condition
    :param ctrl: The control condition
    :return: A dictionary with the following keys => value: \
        ribo_test => bigwig file of the test condition in ribo-seq experiment
        ribo_ctrl => bigwig file of the ctrl condition in ribo-seq experiment
        rna_test => bigwig file of the test condition in rna-seq experiment
        rna_ctrl => bigwig file of the ctrl condition in rna-seq experiment

    >>> d = build_dataframe([f"f{x}.bw" for x in range(1, 9)],
    ... ["C1", "C1", "C2", "C2"] * 2,
    ... ["RIBO"] * 4 + ["RNA"] * 4,
    ... ["R1", "R2"] * 4)
    >>> get_test_and_ctrl_replicate(d, "R1", "C2", "C1") == {
    ... 'ribo_test': 'f3.bw', 'ribo_ctrl': 'f1.bw', 'rna_test': 'f7.bw',
    ... 'rna_ctrl': 'f5.bw'}
    True
    """
    my_tuple = ((x, y) for x in ["RIBO", "RNA"] for y in [test, ctrl])
    keys = ("ribo_test", "ribo_ctrl", "rna_test", "rna_ctrl")
    return {x: df.loc[(df["replicates"] == replicate) &
                      (df["experiments"] == y[0]) &
                      (df["conditions"] == y[1]), "bw"].to_list()[0]
            for x, y in zip(keys, my_tuple)}


def get_bed_cds(bed: Path) -> Dict:
    """
    :param bed: A bed file contaning exons and CDS for every gene \
    of a genome.
    :return: A dictionary containing for each gene, the coordinates of \
    every CDS

    >>> f = Path("/tmp/bed_test.txt")
    >>> _ = f.open("w").write("OR4F5\\t0\\t15\\texon-OR4F5_1\\t.\\t+\\n" +
    ... "OR4F5\\t101\\t155\\texon-OR4F5_2\\t.\\t+\\n" +
    ... "OR4F5\\t3618\\t6167\\texon-OR4F5_3\\t.\\t+\\n" +
    ... "OR4F5\\t146\\t155\\tCDS-OR4F5_1\\t0\\t+\\n" +
    ... "OR4F5\\t3618\\t4587\\tCDS-OR4F5_2\\t0\\t+\\n" +
    ... "OR4F5\\t146\\t149\\tstart_codon-OR4F5_1\\t.\\t+\\n" +
    ... "OR4F5\\t4587\\t4590\\tstop_codon-OR4F5_1\\t.\\t+\\n")
    >>> get_bed_cds(Path(f))
    {'OR4F5': [[146, 155], [3618, 4587]]}
    >>> _ = f.unlink()
    """
    d = {}
    with bed.open("r") as bfile:
        for line in bfile:
            cline = line.replace("\n", "").split("\t")
            if cline[3].startswith("CDS"):
                if cline[0] not in d:
                    d[cline[0]] = [[int(cline[1]), int(cline[2])]]
                else:
                    d[cline[0]].append([int(cline[1]), int(cline[2])])
    return d


def smoothing_window(coverage: np.array, window: int = 9) -> np.array:
    """
    recovering mean in a given window.

    :param coverage:The coverage values
    :param window: A window size
    :return: smoothed coverage values

    >>> smoothing_window(np.array([0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0]), 5)
    array([0. , 1. , 2. , 3. , 3.6, 3.8, 3.6, 3. , 2. , 1. , 0. ])
    >>> smoothing_window(np.array([0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0]), 1)
    array([0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0])
    >>> smoothing_window(np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]), 5)
    array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    """
    scoverage = np.array([])
    if window == 1:
        return coverage
    if np.max(coverage) == 0:
        return coverage
    ext_size = int((window - 1) / 2)
    scoverage = np.concatenate([scoverage, coverage[0:ext_size]])
    cov_center = [
        np.mean(coverage[i:i + window])
        for i in range(len(coverage) - ext_size * 2)
    ]
    cov_center = np.concatenate([cov_center, coverage[-ext_size:]])
    return np.concatenate([scoverage, np.array(cov_center)])


def compute_coverage(dic: Dict[str, str], gene: str,
                     window: int = 9) -> Dict[str, List]:
    """
    Compute the coverage of every bigwig file in dic.

    :param dic: A dictionary containing 4 bigwig of the same replicates \
    from two different conditions and two differents expreiments types: \
    ribo-seq and rna-seq
    :param gene: The current gene of interest
    :param window: A window size
    :return: A dictionary with the found peaks

    >>> test = Path(__file__).parent / "tests" / "files"
    >>> d = {"ribo_test": str(test / "ribo_test_r1.bw"),
    ... "ribo_ctrl": str(test / "ribo_ctrl_r1.bw"),
    ... "rna_test": str(test / "rna_test_r1.bw"),
    ... "rna_ctrl": str(test / "rna_ctrl_r1.bw")}
    >>> r = compute_coverage(d, "gene1", 1)
    >>> {k: round(sum(r[k]),1) for k in r} == {'ribo_test': 567.0,
    ... 'ribo_ctrl': 180.0, 'rna_test': 12.5, 'rna_ctrl': 17.5}
    True
    >>> {k: len(r[k]) for k in r}
    {'ribo_test': 100, 'ribo_ctrl': 100, 'rna_test': 100, 'rna_ctrl': 100}
    """
    tmp = {k: pbw.open(dic[k]) for k in dic}
    return {k: smoothing_window(
        tmp[k].values(gene, 0, tmp[k].chroms(gene)),
        window) for k in tmp}


def get_ratio_coverage(dic: Dict[str, List]) -> np.array:
    """
    Return the ratio coverage.

    :param dic: A dictionary containing the coverage of a gene
    :return: The ratio coverage

    >>> test = Path(__file__).parent / "tests" / "files"
    >>> d = {"ribo_test": str(test / "ribo_test_r1.bw"),
    ... "ribo_ctrl": str(test / "ribo_ctrl_r1.bw"),
    ... "rna_test": str(test / "rna_test_r1.bw"),
    ... "rna_ctrl": str(test / "rna_ctrl_r1.bw")}
    >>> r = compute_coverage(d, "gene1", 1)
    >>> res = get_ratio_coverage(r)
    >>> len(res[0:50][np.isnan(res[0:50])])
    50
    >>> [round(x, 2) for x in list(res[50:75])] == [0.57, 0.57, 0.57, 0.57,
    ... 0.57, 1.14, 2.86, 2.86, 20.0, 20.0, 45.71, 45.71, 45.71, 45.71, 45.71,
    ... 131.43, 131.43, 131.43, 131.43, 34.29, 34.29, 34.29, -10.0, -10.0,
    ... -10.0]
    True
    """
    ndic = {k: np.array(dic[k], dtype=float) for k in dic}
    numerator = ndic["ribo_test"] / ndic["rna_test"]
    numerator[np.isinf(numerator)] = np.nan
    denominator = ndic["ribo_ctrl"] / ndic["rna_ctrl"]
    denominator[np.isinf(denominator)] = np.nan
    return numerator - denominator


def get_ratio_4_all_replicates(design: pd.DataFrame, replicates: List[str],
                               test: str, ctrl: str,
                               gene: str, window: int,
                               ) -> Tuple[List[np.array], List[float]]:
    """
    Get the coverage difference 4 all replicates and the mean rna coverage \
    4 all replicate if rna_mean_cov is True

    :param design: The dataframe containing the experimental design
    :param replicates: The list of replicates present in the design table
    :param test: The test condition
    :param ctrl: The control condition
    :param gene: the name of a gene
    :param window: A window size
    :return: The list of coverage ratio for a gene

    >>> test = Path(__file__).parent / "tests" / "files"
    >>> names = ["ribo_ctrl_r1.bw", "ribo_ctrl_r2.bw", "ribo_test_r1.bw",
    ... "ribo_test_r2.bw", "rna_ctrl_r1.bw", "rna_ctrl_r2.bw",
    ... "rna_test_r1.bw", "rna_test_r2.bw"]
    >>> d = build_dataframe([str(test / n) for n in names],
    ... ["C1", "C1", "C2", "C2"] * 2,
    ... ["RIBO"] * 4 + ["RNA"] * 4,
    ... ["R1", "R2"] * 4)
    >>> reps = list(d["replicates"].unique())
    >>> res, rna_mean = get_ratio_4_all_replicates(d, reps, "C2", "C1",
    ... "gene1", 1)
    >>> [round(x, 2) for x in list(res[0][50:75])] == [0.57, 0.57, 0.57, 0.57,
    ... 0.57, 1.14, 2.86, 2.86, 20.0, 20.0, 45.71, 45.71, 45.71, 45.71, 45.71,
    ... 131.43, 131.43, 131.43, 131.43, 34.29, 34.29, 34.29, -10.0, -10.0,
    ... -10.0]
    True
    >>> [round(x, 2) for x in list(res[1][50:75])] == [0.57, 0.57, 0.57,
    ... 0.57, 0.57, 1.14, 2.29, 2.29, 19.14, 19.14, 44.86, 44.86, 44.86,
    ... 44.86, 44.86, 134.57, 134.57, 134.57, 134.57, 29.71, 29.71, 29.71,
    ... 2.57, 2.57, 2.57]
    True
    >>> rna_mean[50:55] == [0.5999999940395355, 0.5999999940395355,
    ... 0.5999999940395355, 0.5999999940395355, 0.5999999940395355]
    True
    """
    coverages = []
    coverages_rna = []
    for rep in replicates:
        dic = get_test_and_ctrl_replicate(design, rep, test, ctrl)
        dic_cov = compute_coverage(dic, gene, window)
        coverages.append(get_ratio_coverage(dic_cov))
        coverages_rna.append(dic_cov["rna_test"])
        coverages_rna.append(dic_cov["rna_ctrl"])
    coverages_rna_mean = [float(np.mean(x))
                          for x in zip(*coverages_rna)]
    return coverages, coverages_rna_mean


def get_avg_n_std_coverage(ratio_list: List[np.array]
                           ) -> Tuple[np.array, np.array]:
    """
    Get the average coverage among replicates and their standard deviation

    :param ratio_list: The list of ratio coverage for all replicates
    :return: An array corresponding to the average coverage at each position \
    and an array corresponding to the stand deviation of the coverage among \
    replicate.

    >>> res = get_avg_n_std_coverage([np.array([1, 2, 3, 4, 5, 6])])
    >>> list(res[0])
    [1, 2, 3, 4, 5, 6]
    >>> list(res[1])
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    >>> res = get_avg_n_std_coverage([np.array([1, 2, 3, 4, 5, 6, 7]),
    ... np.array([7, 6, 5, 4, 3, 2, 1])])
    >>> list(res[0])
    [4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0]
    >>> list(res[1])
    [3.0, 2.0, 1.0, 0.0, 1.0, 2.0, 3.0]
    """
    if len(ratio_list) == 1:
        return ratio_list[0], np.zeros(len(ratio_list[0]))
    mean_vec = np.array([np.mean(x)
                         for x in zip(*ratio_list)])
    std_vec = np.array([np.std(x) for x in zip(*ratio_list)])
    return mean_vec, std_vec


def compute_threshold(mean_vec: np.array,
                      cds_coord: List[List[int]]) -> int:
    """
    Compute the threshold to detect peaks.

    :param mean_vec: The vector corresponding to the mean ratio coverage \
    across replicate at each position in a given gene
    :param cds_coord: The coordinates of CDS
    :return: The threshold of interest
    >>> mv = [1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 2.745, 2.745, 3.91,
    ... 3.91, 3.91, 3.91, 3.91, 11.805, 11.805, 11.805, 11.805, 3.13, 3.13,
    ... 3.13, 0.86, 0.86, 0.86]
    >>> round(compute_threshold(mv, [[0, 50]]), 2)
    14.75
    """
    cds_coverage = np.array([])
    for cds in cds_coord:
        cds_coverage = np.concatenate([cds_coverage, mean_vec[cds[0]:cds[1]]])
    cds_coverage = np.asarray(cds_coverage)
    cds_coverage = cds_coverage[~np.isnan(cds_coverage)]
    if cds_coverage.size > 0:
        gstd = np.std(cds_coverage)
        gmed = np.mean(cds_coverage)
    else:
        gstd = np.nan
        gmed = np.nan
    return gmed + (gstd * 3)


def compute_minimum(values: List[float]) -> float:
    """

    :param values: Coverage value at a position between replicates
    :return: the minimum coverage at a position for the majority of replicate.

    >>> compute_minimum([1, 2, 3])
    2
    >>> compute_minimum([1, 2, 3, 4])
    2
    >>> compute_minimum([1, 2, 3, 4, 5])
    3
    >>> compute_minimum([1, 2])
    1
    >>> compute_minimum([1, 2, 3, 4, 5, 6])
    3
    >>> compute_minimum([6, 7, 5, 4, 3, 1, 2])
    4
    >>> compute_minimum([1, 1, 2])
    1
    >>> compute_minimum([1, np.nan, 2])
    nan
    """
    if any(np.isnan(values)):
        return np.nan
    majority = int(len(values) / 2)
    if len(values) % 2 == 0:
        majority -= 1
    return sorted(values)[majority]


def compute_minimum_majority(coverages: List[np.array]) -> np.array:
    """
    compute the minimum coverage value in the majority of replicate.

    For example if the coverage at a position is [1, 2, 3] between replicates \
    then the minimum value of the majority of replicate is 2.

    :param coverages: The list of coverages between every replicates
    :return: The minimum majority coverage

    >>> c = [np.array([1, 2, 3, 4]),
    ... np.array([2, 4, 6, 8]),
    ... np.array([3, 4, np.nan, 1])]
    >>> compute_minimum_majority(c)
    array([ 2.,  4., nan,  4.])
    """
    return np.array([compute_minimum([x[i] for x in coverages])
                     for i in range(len(coverages[0]))])


def found_peaks_coordinates(coverages: List[np.array], threshold: float,
                            min_majority: bool = False) -> Optional[List]:
    """
    Gives the interval of every regions having a coverage values > threshold.

    :param coverages: The list of coverages between every replicates
    :param threshold: A threshold above which, we consider a peak a coverage.
    :param min_majority: if True the a peak is detected if the majority \
    of replicate is above the threshold. If False then a peak is detected \
    if all replicates are above the threshold
    :return: The List of peaks coordinates

    >>> mv = [np.array([1, 1, 1, 1, 2, 1, 2, 1, 2.7, 2.8, 3.9, 3.7, 4, 4.2, 11,
    ... 11.5, 11, 10.5, 10, 3, 2.5, 2.5, 1, 0.2, 0.25]),
    ... np.array([2, 2, 2.5, 3, 2, 1.5, 2, 2.5, 3, 3, 4, 4.1, 4.2, 4.5, 14, 15,
    ... 14, 16, 14, 5, 4, 3, 2, 2, 1]),
    ... np.array([3.0, 3.0, 3.75, 4.5, 3.0, 2.25, 3.0, 3.75, 4.5, 4.5, 6.0,
    ... 6.1499999999999995, 6.300000000000001, 6.75, 21.0, 22.5, 21.0, 24.0,
    ... 21.0, 7.5, 6.0, 4.5, 3.0, 3.0, 1.5])]
    >>> found_peaks_coordinates(mv, 10)
    [[14, 18]]
    >>> found_peaks_coordinates(mv, 10.9)
    [[14, 17]]
    >>> found_peaks_coordinates(mv, 11.5) is None
    True
    >>> found_peaks_coordinates(mv, 13) is None
    True
    >>> found_peaks_coordinates(mv, 13, True)
    [[14, 19]]
    >>> mv = [np.array([1, 1, 1, 1, 2, 1, 2, 1, 2.7, 2.8, 3.9, 3.7, 4, 4.2, 11,
    ... 11.5, 11, 10.5, 10, 3, 2.5, 2.5, 1, 0.2, 0.25]),
    ... np.array([2, 2, 2.5, 3, 2, 1.5, 2, 2.5, 3, 3, 4, 4.1, 4.2, 4.5, 14, 15,
    ... 14, 16, 14, 5, 4, 3, 2, 2, 1]),
    ... np.array([3.0, 3.0, 3.75, 4.5, 3.0, 2.25, 3.0, 3.75, 4.5, 4.5, 6.0,
    ... 6.1499999999999995, 6.300000000000001, 6.75, 4, 22.5, 21.0, 24.0,
    ... 21.0, 7.5, 6.0, 4.5, 3.0, 3.0, 1.5])]
    >>> found_peaks_coordinates(mv, 13, True)
    [[15, 19]]
    """
    peaks = []
    if not min_majority:
        min_values = np.min(np.array(coverages), axis=0)
    else:
        min_values = compute_minimum_majority(coverages)
    min_values[np.isnan(min_values)] = -1000
    for i in range(min_values.size - 1):
        if i == 0 and min_values[i] > threshold:
            peaks.append(i)
            if min_values[i + 1] < threshold:
                peaks.append(i + 1)
        elif min_values[i] <= threshold < min_values[i + 1]:
            peaks.append(i + 1)
        elif min_values[i] > threshold >= min_values[i + 1]:
            peaks.append(i + 1)
    if min_values[-1] > threshold:
        peaks.append(min_values.size)
    if not peaks:
        return None
    if len(peaks) % 2 != 0:
        raise IndexError(f"The length of peak should be even "
                         f"(not {len(peaks)})")
    return [[peaks[i], peaks[i + 1]] for i in range(0, len(peaks) - 1, 2)]


def filter_peaks(peaks: List[List], cds_coords: List[List],
                 mean_cds_cov: List[float], threshold: float,
                 coverage: np.array) -> List[List]:
    """
    Recover peaks if they are located within a cds.

    :param peaks: A list of coverage peaks
    :param cds_coords: List of cds coordinates
    :param mean_cds_cov: The list of mean coverage inside cds.
    :param threshold: The minimum coverage needed to call peaks.
    :param coverage: The coverage accross replicate
    :return: The list of peaks within cds

    >>> cov = [0, 6, 8, 12, 10, 11, 15, 14, 8, 9, 10]
    >>> cov2 = [0, 0, 1, 3, 4, 6, 20, 20, 15, 14, 17]
    >>> filter_peaks([[3, 5], [7, 8], [9, 10]], [[0, 9]], [10], 7, cov)
    [[3, 5], [7, 8]]
    >>> filter_peaks([[3, 5], [7, 8], [9, 10]], [[0, 9]], [10], 7, cov2)
    [[7, 8]]
    >>> filter_peaks([[3, 5], [7, 8], [9, 10]], [[0, 15]], [10], 7, cov)
    [[3, 5], [7, 8], [9, 10]]
    >>> filter_peaks([[3, 5], [7, 8], [9, 10]], [[3, 5]], [10], 7, cov)
    [[3, 5]]
    >>> filter_peaks([[3, 5], [7, 8], [9, 10]], [[3, 4]], [10], 7, cov)
    []
    >>> filter_peaks([[3, 5], [7, 8], [9, 10]], [[0, 6], [9, 10]], [8, 8], 7,
    ... cov)
    [[3, 5], [9, 10]]
    >>> filter_peaks([[3, 5], [7, 8], [9, 10]], [[0, 6], [9, 10]], [2, 8], 7,
    ... cov)
    [[9, 10]]
    """
    npeaks = []
    for peak in peaks:
        keep = False
        for i, cds in enumerate(cds_coords):
            keep = cds[0] <= peak[0] <= cds[1] and \
                cds[0] <= peak[1] <= cds[1]
            if keep:
                if mean_cds_cov[i] >= threshold:
                    peak_cov = mean_cds_coverage(coverage, [peak])[0]
                    if peak_cov >= threshold:
                        npeaks.append(peak)
                break
    return npeaks


def mean_cds_coverage(rna_cov: List[float], cds_coords: List[List]
                      ) -> List[float]:
    """
    Compute the mean coverage of every cds.

    :param rna_cov: Harmonic mean rna coverage
    :param cds_coords: cds coordinates
    :return: The list of mean coverage inside cds.

    >>> r_mean_cov = [0, 0, 5, 7, 8, 0, 4, 2, 1, 0, 9, 3, 2]
    >>> cds_loc = [[2, 5], [6, 9], [10, 13]]
    >>> mean_cds_coverage(r_mean_cov, cds_loc)
    [6.666666666666667, 2.3333333333333335, 4.666666666666667]
    """
    return [float(np.mean(rna_cov[c[0]:c[1]])) for c in cds_coords]


def find_peak_in_gene(design: pd.DataFrame, replicates: List[str],
                      test: str, ctrl: str,
                      gene: str, window: int,
                      cds_coords: List[List],
                      write_bw: bool = False,
                      coverage_threshold: float = 5.0,
                      min_majority: bool = False
                      ) -> Tuple[Optional[Dict], Dict, Dict]:
    """
    Get the coverage ration 4 all replicates.

    :param design: The dataframe containing the experimental design
    :param replicates: The replicates in design dataframe
    :param test: The test condition
    :param ctrl: The control condition
    :param gene: the name of a gene
    :param window: A window size
    :param cds_coords: A list of CDS_coordinates
    :param write_bw: A boolean indicating if we want to write bigwig files
    :param coverage_threshold: The minimum coverage needed to call peaks.
    :param min_majority: if True the a peak is detected if the majority \
    of replicate is above the threshold. If False then a peak is detected \
    if all replicates are above the threshold
    :return: The list of coverage ratio for a gene
    """
    ratio_list, rna_cov = get_ratio_4_all_replicates(design, replicates,
                                                     test, ctrl, gene, window)
    if write_bw:
        ratio_dic = {gene: {res: ratio_list[i]
                            for i, res in enumerate(replicates)}}
    else:
        ratio_dic = {}
    mean_cov, std_rep = get_avg_n_std_coverage(ratio_list)
    mean_dic = {gene: mean_cov} if write_bw else {}
    threshold = compute_threshold(mean_cov, cds_coords)
    peaks = found_peaks_coordinates(ratio_list, threshold, min_majority)
    if peaks is None:
        return peaks, ratio_dic, mean_dic
    cds_mean_cov = mean_cds_coverage(rna_cov, cds_coords)
    peaks = filter_peaks(peaks, cds_coords, cds_mean_cov,
                         coverage_threshold, rna_cov)
    if not peaks:
        return None, ratio_dic, mean_dic
    scores = [round(((np.mean(mean_cov[p[0]:p[1]]) / threshold) - 1) * 100, 1)
              for p in peaks]
    return {gene: {"peaks": peaks, "score": scores, "threshold": threshold}}, ratio_dic, mean_dic


def combine_dic(a: Dict, b: Dict) -> Dict:
    """
    Combine dictionaries together.

    :param a: A dictionary a
    :param b: A dictionary b
    :return: a and b combined
    """
    return dict(a, **b)


def write_bed(peaks_dic: Dict, outfile: str) -> None:
    """
    Save dictionary of peaks into a bed file.

    :param peaks_dic: A dictionary of peaks
    :param outfile: The bedfile were the peaks will be stored
    """
    with open(outfile, "w") as outf:
        c = 0
        for gene in peaks_dic.keys():
            for i, (start, stop) in enumerate(peaks_dic[gene]['peaks']):
                c += 1
                outf.write(f"{gene}\t{start}\t{stop}\tpeak_{c}\t"
                           f"{peaks_dic[gene]['score'][i]}\t+\n")


def get_list_of_genes(bw: str) -> List[str]:
    """
    From a bigwig file return the list of genes of interest

    :param bw: A bigwig file
    :return: The list of gene within the bigwig file

    >>> test = Path(__file__).parent / "tests" / "files"
    >>> get_list_of_genes(str(test / "ribo_test_r1.bw"))
    ['gene1']
    """
    bw = pbw.open(bw)
    genes = list(bw.chroms().keys())
    bw.close()
    return genes


def get_peaks_dict(design: pd.DataFrame, test: str, ctrl: str,
                   dic_bed: Dict, processes: int = 1,
                   window: int = 9, write_bw: bool = False,
                   coverage_threshold: float = 5.0,
                   min_majority: bool = False) -> Tuple[Dict, Dict, Dict]:
    """
    Return a dictionary containing peaks.

    :param design: An experimental design dataframe
    :param test: The test condition
    :param ctrl: The control condition
    :param dic_bed: A dictionary containing bed CDS
    :param processes: The number of processes to launch
    :param window: A window size
    :param write_bw: A boolean indicating if we want to write bigwig files
    :param coverage_threshold: The minimum coverage needed to call peaks.
    :param min_majority: if True the a peak is detected if the majority \
    of replicate is above the threshold. If False then a peak is detected \
    if all replicates are above the threshold
    :return: A dictionary of peaks
    """
    p = processes if processes <= mp.cpu_count() else mp.cpu_count()
    pool = mp.Pool(processes=1 if p < 0 else p)
    proc = []
    genes = get_list_of_genes(design["bw"][0])
    replicates = list(design["replicates"].unique())
    for gene in genes:
        args = [design, replicates, test, ctrl, gene, window, dic_bed[gene],
                write_bw, coverage_threshold, min_majority]
        proc.append(pool.apply_async(find_peak_in_gene, args))
    res = [proc[i].get(timeout=None) for i in range(len(proc))]
    nres = [r[0] for r in res if r[0] is not None]
    if write_bw:
        nratio = [r[1] for r in res]
        nmean = [r[2] for r in res]
        del res
        dic_ratio = reduce(combine_dic, nratio)
        dic_mean = reduce(combine_dic, nmean)
    else:
        dic_ratio = {}
        dic_mean = {}
    dic_peak = {} if nres == [] else reduce(combine_dic, nres)
    return dic_peak, dic_ratio, dic_mean


def write_ratio_bw(ratio_dic: Dict[str, Dict], ctrl: str, test: str) -> None:
    replicates = list(ratio_dic[list(ratio_dic.keys())[0]].keys())
    for rep in replicates:
        bw = pbw.open(f"{test}_vs_{ctrl}_{rep}.bw", "w")
        headers = []
        for gene in list(ratio_dic.keys()):
            cov = ratio_dic[gene][rep]
            v = len(cov)
            headers.append((gene, v))
        bw.addHeader(headers)
        for gene in list(ratio_dic.keys()):
            cov = ratio_dic[gene][rep]
            cov = list(np.nan_to_num(cov))
            v = len(cov)
            bw.addEntries([gene] * v, list(range(v)),
                          ends=list(range(1, v + 1)), values=cov)
        bw.close()


def write_mean_bw(mean_dic: Dict[str, np.array], ctrl: str, test: str) -> None:
    bw = pbw.open(f"{test}_vs_{ctrl}.bw", "w")
    headers = []
    for gene in list(mean_dic.keys()):
        cov = mean_dic[gene]
        v = len(cov)
        headers.append((gene, v))
    bw.addHeader(headers)
    for gene in list(mean_dic.keys()):
        cov = mean_dic[gene]
        cov = list(np.nan_to_num(cov))
        v = len(cov)
        bw.addEntries([gene] * v, list(range(v)),
                      ends=list(range(1, v + 1)), values=cov)
    bw.close()


@lp.parse(design="file", annotation="file")
def peak_caller(design: str, ctrl: str, test: str, annotation: str,
                outfile: str, ps: int = 1, window: int = 1,
                write_bw: bool = False, coverage_threshold: float = 5.0,
                min_majority: bool = False) -> None:
    """
    Find ribosome peaks located in ctrl condition related to the test \
    condition while accounting for RNA levels.

    :param design: A file containing an experimental design.
    :param ctrl: The name of the control condition
    :param test: The name of the test condition
    :param annotation: A bed file containing the annotation used \
    in the bigwig
    :param outfile: The file where the peaks will be stored
    :param ps: Number of threads to use
    :param window: The window size to used
    :param write_bw: A boolean indicating if we want to write bigwig files \
    (default False)
    :param coverage_threshold: The minimum coverage needed to call peaks \
    (default False).
    :param min_majority: if True the a peak is detected if the majority \
    of replicate is above the threshold. If False then a peak is detected \
    if all replicates are above the threshold. (default False)
    """
    df = pd.read_csv(design, sep="\t")
    if "read_size" in df.columns:
        df.drop("read_size", axis=1)
    check_conditions(df, test, ctrl)
    design = filter_dataframe(df, test, ctrl)
    dic_cds = get_bed_cds(Path(annotation))
    peaks_dic, ratio_dic, mean_dic = get_peaks_dict(design, test, ctrl,
                                                    dic_cds, ps, window,
                                                    write_bw,
                                                    coverage_threshold,
                                                    min_majority)
    write_bed(peaks_dic, outfile)
    if write_bw:
        write_ratio_bw(ratio_dic, ctrl, test)
        write_mean_bw(mean_dic, ctrl, test)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        import doctest

        doctest.testmod()
    else:
        peak_caller()
