#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This files aims to download a ncbi genome file and  gtf file \
and then create a fasta file containing genes, a bed file of exon, \
cds and start and stop codons.
"""

import lazyparser as lp
from .config import get_folder
import subprocess as sp
from pathlib import Path
from .get_wanted_rrna import select_sequences
from .exon_parser import main_parser
from .files_from_parsed_gtf import write_bed_file, write_fasta_file, \
    write_gene_size, write_gtf_file
from typing import Dict
from pyfaidx import Fasta
from Bio.Seq import Seq

def get_genome(url: str, output: Path, prefix: str) -> Path:
    """
    Download the genome of interest

    :param url: The url of interest
    :param output: Parent folder where the result will be created
    :param prefix: Folder where the result will be created
    :return: The genome file
    """
    full_url = f"{url}/{prefix}_genomic.fna.gz"
    coutput = get_folder(output, "genome")
    outfile = coutput / f"{prefix}_genomic.fna"
    tmp_file = coutput / f"{prefix}_genomic.fna.gz"
    if not tmp_file.is_file():
        sp.check_call(f"wget {full_url} -P {coutput}", shell=True,
                      stderr=sp.STDOUT)
    if not outfile.is_file():
        cmd = f"gunzip -c -d {outfile}.gz > {outfile}"
        sp.check_call(cmd, shell=True, stderr=sp.STDOUT)
    return outfile


def get_transcriptome(url: str, output: Path, prefix: str) -> Path:
    """
    Download the transcriptome of interest

    :param url: The url of interest
    :param output: parent folder where the result will be created
    :param prefix: Folder where the result will be created
    :return: The transcritpme file
    """
    full_url = f"{url}/{prefix}_rna_from_genomic.fna.gz"
    coutput = get_folder(output, "transcriptome")
    outfile = coutput / f"{prefix}_rna_from_genomic.fna.gz"
    if not outfile.is_file():
        sp.check_call(f"wget {full_url} -P {coutput}", shell=True,
                      stderr=sp.STDOUT)
    return outfile


def get_gtf(url: str, output: Path, prefix: str) -> Path:
    """
    Download the gtf of interest

    :param url: The url of interest
    :param output: parent folder where the result will be created
    :param prefix: Folder where the result will be created
    :return: The gtf file
    """
    full_url = f"{url}/{prefix}_genomic.gtf.gz"
    coutput = get_folder(output, "annotation")
    outfile = coutput / f"{prefix}_genomic.gtf.gz"
    if not outfile.is_file():
        sp.check_call(f"wget {full_url} -P {coutput}", shell=True,
                      stderr=sp.STDOUT)
    return outfile


def mark_cds_containing_stop_codons(dic_gtf: Dict, fasta: str) -> Dict:
    """
    Return dic_gtf but if a cds contains a stop codon, replace its offset \
    by stop_codon.

    :param dic_gtf:  A dictionary containing parsed gtf
    :param fasta: A fasta file
    :return: the dic_gtf updated
    """
    count = 0
    ds = Fasta(fasta)
    for gene in dic_gtf:
        for cds in dic_gtf[gene]['CDS']:
            tmp = dic_gtf[gene]['CDS'][cds]
            if tmp[4] != "multiple":
                tmp_seq = str(ds[gene][int(tmp[1]) + int(tmp[4]):
                                         int(tmp[2])])

                s = Seq(tmp_seq[:len(tmp_seq) - len(tmp_seq) % 3])
                aa = s.translate()
                if "*" in aa:
                    count += 1
                    offset = dic_gtf[gene]['CDS'][cds][4]
                    dic_gtf[gene]['CDS'][cds][4] = f"stop-CDS-{offset}"
    print(f"{count} stop codon detected in CDS")
    return dic_gtf


@lp.parse
def launcher(url: str = "", output_folder: str = "."):
    """

    :param url: A NCBI FTP url containing many data about an assembly
    :param output_folder: Folder where the result will be created
    """
    if url == "":
        url = "https://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_mammalian/Homo_sapiens/latest_assembly_versions/GCF_000001405.39_GRCh38.p13"
    url = url.rstrip("/")
    prefix = url.split("/")[-1]
    output = Path(output_folder) / prefix
    genome = get_genome(url, output, prefix)
    transcriptome = get_transcriptome(url, output, prefix)
    select_sequences(transcriptome, get_folder(output, "filter"),
                     ["tRNA", "rRNA", "snoRNA", "ncRNA", "misc_RNA"])
    gtf = get_gtf(url, output, prefix)
    d = main_parser(gtf, get_folder(output, "annotation"))
    records, fasta = write_fasta_file(genome, d,
                                      get_folder(output, "genome"), prefix)
    write_gene_size(records, get_folder(output, "annotation"), prefix)
    d = mark_cds_containing_stop_codons(d, str(fasta))
    write_bed_file(d, get_folder(output, "annotation"), prefix)
    write_gtf_file(d, get_folder(output, "annotation"), prefix)
    genome.unlink()


launcher()
