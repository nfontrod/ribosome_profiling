#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Configuration file
"""

from pathlib import Path

def get_folder(output: Path, folder_name: str) -> Path:
    """
    Create a new folder named ``folder_named``.

    :param output: parent folder
    :param folder_name: name of the new folder
    :return: the new folder
    """
    new_output = output / folder_name
    new_output.mkdir(exist_ok=True, parents=True)
    return new_output
