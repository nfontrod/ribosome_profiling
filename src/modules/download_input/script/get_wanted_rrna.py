#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a fasta file containing \
ncRNA
"""

from Bio import SeqIO
import lazyparser as lp
from pathlib import Path#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a fasta file containing \
ncRNA
"""

from Bio import SeqIO
from pathlib import Path
from typing import List
import gzip


def ncrna_in_id(ncrna: List[str], description_seq: str) -> bool:
    """
    Return True if the id_seq corresponds to a ncRNA of interest, \
    False else.

    :param ncrna: The list non coding RNA of interest
    :param description_seq: A sequence description
    :return: True if the id_seq corresponds to a ncRNA of interest, \
    False else.
    """
    return any(f"={cncrna}]" in description_seq for cncrna in ncrna)


def get_interest_records(fasta_file: Path, ncrna: List[str]):
    """
    Get the list of interest records.

    :param fasta_file: The fasta file of interest containing ncbi \
    fasta sequence
    :param ncrna: The list of ncRNA of interest
    :return: The list of record
    """
    return [
        record
        for record in SeqIO.parse(fasta_file, "fasta")
        if ncrna_in_id(ncrna, record.description)
    ]


def select_sequences(fasta_file: Path, output: Path,
                     ncrna: List[str] =
                     ("tRNA", "rRNA", "snoRNA")
                     ) -> Path:
    """
    Create the fasta file containing only the ncRNA sequence of interest.

    :param fasta_file: The fasta file of interest
    :param ncrna: The list of ncRNA to keep
    :param output: folder were the result will be created
    :return: the fasta file containing contaminant sequences
    """
    if fasta_file.name.endswith(".gz"):
        with gzip.open(fasta_file, "rt") as handle:
            records = get_interest_records(handle, ncrna)
    else:
        records = get_interest_records(fasta_file, ncrna)
    coutput = output / "contaminant_rna.fa"
    with coutput.open("w") as outfile:
        SeqIO.write(records, outfile, "fasta")
    return coutput
