#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to get only refseq genes along, with \
their exons and respective CDS
"""

from pathlib import Path
from typing import Dict, List, Union, Optional
import gzip
import re
import doctest

# Definition of a global counter
COUNTERS = {"gene_0_CDS": [0, "Gene contains no CDS"],
            "gene_0_start": [0, "Gene contains no start codon"],
            "gene_0_stop": [0, "Gene contains no stop codon"],
            "multiple_CDS": [0, "CDS having multiple coding frames"],
            "codon_multiple_CDS": [0, "Start/stop codon overlapping "
                                      "multiple CDS"],
            "codon_0_CDS": [0, "Start/stop codon not in a CDS"],
            "CDS_multiple_exon": [0, "CDS overlapping multiple exons"],
            "CDS_0_exon": [0, "CDS not overlapping an exons"],
            "overlapping_cds": [0, "Overlapping CDS"]}


def get_gene_name(data: str, pattern) -> str:
    """
    Get the gene id of interest

    :param data: The description of a gtf line
    :param pattern: A regex pattern
    :return: the gene name
    """
    res = re.search(pattern, data)
    if res is not None:
        res = res.group().rstrip(";").replace("gene_id ", "").replace('"', '')
    return res


def get_gbkey(data: str, pattern) -> str:
    """
    Get the gbkey of interest

    :param data: The description of a gtf line
    :param pattern: A regex pattern
    :return: the gene name
    """
    res = re.search(pattern, data)
    if res is not None:
        res = res.group().rstrip(";").replace("gbkey ", "").replace('"', '')
    return res


def convert_line(mline: List) -> List:
    """
    Convert every element in a gtf line in a good format.

    :param mline: A gtf line.
    :return: The line with goof format
    """
    mline[3] = int(mline[3]) - 1
    mline[4] = int(mline[4])
    return mline


def element_included_in_gene(dic_gene: Dict, cline: List[str]) -> bool:
    """
    True if the element in cline in included in a gene, false else.

    :param dic_gene: A dictionary of a gene, its exons and CDS.
    :param cline: A gtf line
    :return: True if the element in cline in included in a gene, false else.
    """
    return (dic_gene["gene"][0] == cline[0]) and \
           (dic_gene["gene"][1] <= cline[3] <= dic_gene["gene"][2]) and \
           (dic_gene["gene"][1] <= cline[4] <= dic_gene["gene"][2]) and \
           (dic_gene["gene"][3] == cline[6])


def gtf_data(cline: List[str]) -> Dict[str, str]:
    """
    From a gtf line return a dictionary containing some data

    :param cline: gtf line
    :return: dictionary containing column 2 and 9 of the gtf line
    """
    return {"source": cline[1], "data": cline[8]}


def add_first_feature(cline: List[str], d: Dict, gene_id: str) -> Dict:
    """
    Add a cds or exon to the dictionary

    :param cline: gtf line
    :param d: the dictionary
    :param gene_id: The gene id
    :return: The dictionary updated
    """
    if cline[2] == "exon":
        d[gene_id][cline[2]] = {cline[3]: [cline[0], cline[3], cline[4],
                                           cline[6], gtf_data(cline)]}
    elif cline[2] == "CDS":
        d[gene_id][cline[2]] = {cline[3]: [cline[0], cline[3], cline[4],
                                           cline[6], int(cline[7]), False,
                                           gtf_data(cline)]}
    elif cline[2] in ["start_codon", "stop_codon"]:
        if int(cline[7]) == 0:
            d[gene_id][cline[2]] = {cline[3]: [cline[0], cline[3], cline[4],
                                               cline[6], gtf_data(cline)]}
    return d


def feature_overlap(dic_element: Dict, cline: List) -> Union[bool, str, None]:
    """
    If cline overlap a feature located in dic_element then return \
    the overlapped feature name else return False.

    :param dic_element: A dictionary containing a list of CDS or exons.
    :param cline: A gtf line
    :return: If cline overlap an feature located in dic_element then return \
    the overlapped feature name else return False.
    """
    ft_list = [
        ft
        for ft in dic_element
        if dic_element[ft][1] <= cline[3] <= dic_element[ft][2]
           or dic_element[ft][1] <= cline[4] <= dic_element[ft][2]
    ]
    if len(ft_list) > 1:
        return None
    if not ft_list:
        return False
    return ft_list[0]


def add_exon(cline: List, d: Dict, gene_id: str) -> Dict:
    """
    Add a new exon.

    Add a new exon to the gene. If it overlap an existing exon for the gene, \
    it keep the longest one.

    :param cline: a gtf line
    :param d: the dictionary containing gene, their cds and exons
    :param gene_id: A gene id.
    :return: the dictionary of gene updated
    """
    if cline[2] != "exon":
        raise ValueError(f"cline[2] should be 'exon' not {cline[2]} !")
    res = feature_overlap(d[gene_id]['exon'], cline)
    if not res:
        d[gene_id]['exon'][cline[3]] = [cline[0], cline[3], cline[4], cline[6],
                                        gtf_data(cline)]
    elif res is not None:
        old_ft = d[gene_id]['exon'][res][2] - d[gene_id]['exon'][res][1] + 1
        new_ft = cline[4] - cline[3] + 1
        if new_ft > old_ft:
            del (d[gene_id]['exon'][res])
            d[gene_id]['exon'][cline[3]] = [cline[0], cline[3], cline[4],
                                            cline[6], gtf_data(cline)]
    return d


def add_cds(cline: List, d: Dict, gene_id: str) -> Dict:
    """
    Add a new cds.

    Add a new cds to the gene. If it overlap an existing cds for the gene, \
    it keep the longest one. Check also if the reading frame is conserved.

    :param cline: a gtf line
    :param d: the dictionary containing gene, their cds and exons
    :param gene_id: A gene id.
    :return: the dictionary of gene updated
    """
    global COUNTERS
    if cline[2] != "CDS":
        raise ValueError(f"cline[2] should be 'cds' not {cline[2]} !")
    res = feature_overlap(d[gene_id]['CDS'], cline)
    if not res:
        d[gene_id]['CDS'][cline[3]] = [cline[0], cline[3], cline[4], cline[6],
                                       int(cline[7]), False, gtf_data(cline)]
    elif res is not None:
        old_ft = d[gene_id]['CDS'][res][2] - d[gene_id]['CDS'][res][1] + 1
        new_ft = cline[4] - cline[3] + 1
        multiple_cds = False
        if d[gene_id]["gene"][3] == "+":
            diff_pos = (int(cline[7]) + int(cline[3])) - \
                       (d[gene_id]['CDS'][res][4] + d[gene_id]['CDS'][res][1])
        else:
            diff_pos = (int(cline[4]) - int(cline[7])) - \
                       (d[gene_id]['CDS'][res][2] - d[gene_id]['CDS'][res][4])
        if abs(diff_pos) % 3 != 0:
            multiple_cds = True
            COUNTERS["multiple_CDS"][0] += 1
        if new_ft > old_ft:
            del (d[gene_id]['CDS'][res])
            d[gene_id]['CDS'][cline[3]] = [cline[0], cline[3], cline[4],
                                           cline[6], int(cline[7]),
                                           multiple_cds, gtf_data(cline)]
        else:
            d[gene_id]['CDS'][res][5] = multiple_cds
    return d


def add_start_stop_codon(cline: List[str], d: Dict, gene_id: str) -> Dict:
    """
    Add a new start or a stop codon.

    :param cline: a gtf line
    :param d: the dictionary containing gene, their cds and exons
    :param gene_id: A gene id.
    :return: the dictionary of gene updated
    """
    if cline[2] not in ["start_codon", "stop_codon"]:
        raise ValueError(f"cline[2] should be 'start_codon' or 'stop_codon' "
                         f"not {cline[2]} !")
    if int(cline[7]) == 0:
        d[gene_id][cline[2]][cline[3]] = [cline[0], cline[3], cline[4],
                                          cline[6], gtf_data(cline)]
    return d


def add_feature(cline: List[str], d: Dict, gene_id: str, gbkey: str) -> Dict:
    """
    Add a new feature.

    :param cline: a gtf line
    :param d: the dictionary containing gene, their cds and exons
    :param gene_id: A gene id.
    :param gbkey: The gbkey
    :return: the dictionary of gene updated
    """
    if gbkey in ["mRNA", 'CDS']:
        if not element_included_in_gene(d[gene_id], cline):
            raise ValueError(f"{cline} corresponds to a {cline[2]} with "
                             f"bad coordinates for gene {gene_id} : "
                             f"{d[gene_id]['gene']}")
        if cline[2] not in d[gene_id]:
            d = add_first_feature(cline, d, gene_id)
        elif cline[2] == "exon":
            d = add_exon(cline, d, gene_id)
        elif cline[2] == "CDS":
            d[gene_id]["coding"] = True
            d = add_cds(cline, d, gene_id)
        else:
            d = add_start_stop_codon(cline, d, gene_id)
    return d


def parse_gtf(gtf: Path) -> Dict[str, Dict[str, Union[List, Dict[str, List]]]]:
    """
    Parse a NCBI gtf.

    :param gtf: A human gtf
    :return: The dictionary of human gtf parsed
    """
    d = {}
    gene_name_pattern = re.compile(r'gene_id ".*?";')
    gbkey_pattern = re.compile(r'gbkey ".*?";')
    with gzip.open(gtf, "rt") as handle:
        for line in handle:
            cline = line.replace("\n", "").split("\t")
            if line[0] != "#" and cline[1] in \
                    ["BestRefSeq", "BestRefSeq%2CGnomon"]:
                gene_id = get_gene_name(cline[-1], gene_name_pattern)
                gbkey = get_gbkey(cline[-1], gbkey_pattern)
                cline = convert_line(cline)
                if cline[2] == "gene" and gene_id not in d:
                    d[gene_id] = {"gene": [cline[0], cline[3], cline[4],
                                           cline[6], gtf_data(cline)],
                                  "coding": False}
                elif cline[2] == "gene":
                    raise ValueError(f"error gene {gene_id} exists "
                                     f"multiple times")
                elif gene_id not in d:
                    raise ValueError(f"{cline[2]} element with no "
                                     f"associated gene : ({cline})")
                else:
                    d = add_feature(cline, d, gene_id, gbkey)
    return d


def check_complete(dict_gene: Dict) -> bool:
    """
    Check if the dictionary has the required keys to be complete.

    :param dict_gene:  A dictionary containing a gene, its CDS, exon and \
    start and stop codon.
    :return: True if dict_gene has the required keys, false else.
    """
    res = ["gene", "coding", "exon", "CDS", "start_codon", "stop_codon"]
    return all(k in dict_gene.keys() for k in res)


def check_cds(d: Dict) -> Union[Dict, None]:
    """
    Check if all CDS are located inside an exon.

    :param d: A dictionary for a gene, containing its CDS, exons and \
    start and stop codon
    """
    global COUNTERS
    bad_cds = []
    for cds in d["CDS"]:
        overlap = [
            exon
            for exon in d['exon']
            if d["exon"][exon][1] <= d["CDS"][cds][1] <= d["exon"][exon][2]
               and d["exon"][exon][1] <= d["CDS"][cds][2] <= d["exon"][exon][2]
               and d["exon"][exon][0] == d["CDS"][cds][0]
               and d["exon"][exon][3] == d["CDS"][cds][3]
        ]
        if not overlap:
            COUNTERS["CDS_0_exon"][0] += 1
            bad_cds.append(cds)
        if len(overlap) > 1:
            COUNTERS["CDS_multiple_exon"][0] += 1
            bad_cds.append(cds)
    for cds in bad_cds:
        del (d["CDS"][cds])
    if len(d["CDS"]) == 0:
        return None
    else:
        return d


def find_longest_cds(d: Dict, my_cds: List,
                     offset: Optional[bool]) -> Dict:
    """
    Return the longest CDS
    :param d:  A dictionary containing a gene, its CDS and exons
    :param my_cds: A list of overlapping cds
    :param offset: Param to replace the offset of cds to keep
    :return: the dictionary update overlapping cds
    """
    cds_len = [d["CDS"][cds][2] - d["CDS"][cds][1]
               for cds in my_cds]
    cds_to_keep = my_cds[cds_len.index(max(cds_len))]
    cds_to_del = [cds for cds in my_cds if cds != cds_to_keep]
    for cds in cds_to_del:
        del (d["CDS"][cds])
    if offset is not None:
        d["CDS"][cds_to_keep][5] = offset
    return d


def check_overlapping_cds(d: Dict) -> Dict:
    """
    Check if CDS don't overlap. For overlapping CDS with the same frame, \
    the longest CDS will be kept and if the don't share the same frame \
    the longest will be kept and the offset will be set to 'multiple'.

    :param d: A dictionary containing a gene, its CDS and exons
    :return: The dictionary updated
    """
    global COUNTERS
    offset_dic = {0: 0, 1: 2, 2: 1}
    for cds1 in d["CDS"]:
        overlap = [
            cds2
            for cds2 in d['CDS']
            if (d["CDS"][cds2][1] <= d["CDS"][cds1][1] <= d["CDS"][cds2][2]
                or d["CDS"][cds2][1] <= d["CDS"][cds1][2] <= d["CDS"][cds2][2])
               and d["CDS"][cds2][0] == d["CDS"][cds1][0]
               and d["CDS"][cds2][3] == d["CDS"][cds1][3]
               and cds1 != cds2
        ]
        if len(overlap) != 0:
            my_cds = [cds1] + overlap
            COUNTERS['overlapping_cds'][0] += len(my_cds)
            mult = any(d["CDS"][cds][5] for cds in my_cds)
            if mult:
                return check_overlapping_cds(
                    find_longest_cds(d, my_cds, True))
            else:
                start_pos = [d["CDS"][cds][1] + offset_dic[d["CDS"][cds][4]]
                             for cds in my_cds]
                same_cds = all(abs(pos - start_pos[0]) % 3 == 0
                               for pos in start_pos[1:])
                if same_cds:
                    return check_overlapping_cds(
                        find_longest_cds(d, my_cds, None))
                else:
                    return check_overlapping_cds(
                        find_longest_cds(d, my_cds, True))
    return d


def check_codon(d: Dict, tcodon: str) -> Union[None, Dict]:
    """
    Check if start or stop codon are located inside an exon.

    :param d: A dictionary for a gene, containing its CDS, exons and \
    start and stop codon
    :param tcodon: The type of codon of interest
    """
    global COUNTERS
    bad_codon = []
    for codon in d[tcodon]:
        if tcodon == "start_codon":
            overlap = [
                cds
                for cds in d['CDS']
                if d["CDS"][cds][1] <= d[tcodon][codon][1] <= d["CDS"][cds][2]
                   and d["CDS"][cds][1] <= d[tcodon][codon][2] <=
                   d["CDS"][cds][2]
                   and d["CDS"][cds][0] == d[tcodon][codon][0]
                   and d["CDS"][cds][3] == d[tcodon][codon][3]
            ]
        else:
            overlap = [
                cds
                for cds in d['CDS']
                if (d["CDS"][cds][1] <= d[tcodon][codon][1] <= d["CDS"][cds][2]
                    or d["CDS"][cds][1] <= d[tcodon][codon][2]
                    <= d["CDS"][cds][2])
                   and d["CDS"][cds][0] == d[tcodon][codon][0]
                   and d["CDS"][cds][3] == d[tcodon][codon][3]
            ]
        if not overlap:
            COUNTERS["codon_0_CDS"][0] += 1
            bad_codon.append(codon)
        if len(overlap) > 1:
            COUNTERS["codon_multiple_CDS"][0] += 1
            bad_codon.append(codon)
    for codon in bad_codon:
        del (d[tcodon][codon])
    if len(d[tcodon]) == 0:
        return None
    return d


def update_ft_num(d: Dict) -> Dict:
    """
    Re-update all feature number.

    :param d: A dictionary for a gene, containing its CDS, exons and \
    start and stop codon
    :return: The same dictionary updated

    >>> md = {"source": "BestRefSeq", "data": 'gene_id "XX"; trans...'}
    >>> e = {'gene': ['NC_000001.11', 65418, 71585, '+', md],
    ...      'coding': True,
    ...      'exon': {65418: ['NC_000001.11', 65418, 65433, '+', md],
    ...               65519: ['NC_000001.11', 65519, 65573, '+', md],
    ...               69036: ['NC_000001.11', 69036, 71585, '+', md]},
    ...      'CDS': {65564: ['NC_000001.11', 65564, 65573, '+', 0, False, md],
    ...              69036: ['NC_000001.11', 69036, 70005, '+', 0, False, md]},
    ...      'start_codon': {65564: ['NC_000001.11', 65564, 65567, '+', md]},
    ...      'stop_codon': {70005: ['NC_000001.11', 70005, 70008, '+', md]}}
    >>> e2 = {'gene': ['NC_000001.11', 65418, 71585, '+', md],
    ...       'coding': True,
    ...       'exon': {1: ['NC_000001.11', 65418, 65433, '+', md],
    ...                2: ['NC_000001.11', 65519, 65573, '+', md],
    ...                3: ['NC_000001.11', 69036, 71585, '+', md]},
    ...       'CDS': {1: ['NC_000001.11', 65564, 65573, '+', 0, False, md],
    ...               2: ['NC_000001.11', 69036, 70005, '+', 0, False, md]},
    ...       'start_codon': {1: ['NC_000001.11', 65564, 65567, '+', md]},
    ...       'stop_codon': {1: ['NC_000001.11', 70005, 70008, '+', md]}}
    >>> update_ft_num(e) == e2
    True
    >>> e = {'gene': ['NC_000001.11', 65418, 71585, '-', md],
    ...      'coding': True,
    ...      'exon': {65418: ['NC_000001.11', 65418, 65433, '-', md],
    ...               65519: ['NC_000001.11', 65519, 65573, '-', md],
    ...               69036: ['NC_000001.11', 69036, 71585, '-', md]},
    ...      'CDS': {65564: ['NC_000001.11', 65564, 65573, '-', 0, False, md],
    ...              69036: ['NC_000001.11', 69036, 70005, '-', 0, False, md]},
    ...      'start_codon': {65564: ['NC_000001.11', 65564, 65567, '-', md]},
    ...      'stop_codon': {70005: ['NC_000001.11', 70005, 70008, '-', md]}}
    >>> e2 = {'gene': ['NC_000001.11', 65418, 71585, '-', md],
    ...       'coding': True,
    ...       'exon': {1: ['NC_000001.11', 69036, 71585, '-', md],
    ...                2: ['NC_000001.11', 65519, 65573, '-', md],
    ...                3: ['NC_000001.11', 65418, 65433, '-', md]},
    ...       'CDS': {1: ['NC_000001.11', 69036, 70005, '-', 0, False, md],
    ...               2: ['NC_000001.11', 65564, 65573, '-', 0, False, md]},
    ...       'start_codon': {1: ['NC_000001.11', 65564, 65567, '-', md]},
    ...       'stop_codon': {1: ['NC_000001.11', 70005, 70008, '-', md]}}
    >>> update_ft_num(e) == e2
    True
    """
    keys_list = [sorted(d["exon"].keys()),
                 sorted(d["CDS"].keys()),
                 sorted(d["start_codon"].keys()),
                 sorted(d["stop_codon"].keys())]
    main_key = ["exon", "CDS", "start_codon", "stop_codon"]
    if d["gene"][3] == "-":
        keys_list = [k[::-1] for k in keys_list]
    for m, keys in enumerate(keys_list):
        for i, key in enumerate(keys):
            tmp = d[main_key[m]][key].copy()
            del (d[main_key[m]][key])
            d[main_key[m]][i + 1] = tmp
    return d


def update_coordinates(d: Dict) -> Dict:
    """
    Update all coordinate of exon, CDS and codon start and stop.

    :param d: A dictionary for a gene, containing its CDS, exons and \
    start and stop codon
    :return: Dictionary with update coordinate.

    >>> md = {"source": "BestRefSeq", "data": 'gene_id "XX"; trans...'}
    >>> e = {'gene': ['NC_000001.11', 65418, 71585, '+', md],
    ...      'coding': True,
    ...      'exon': {65418: ['NC_000001.11', 65418, 65433, '+', md],
    ...               65519: ['NC_000001.11', 65519, 65573, '+', md],
    ...               69036: ['NC_000001.11', 69036, 71585, '+', md]},
    ...      'CDS': {65564: ['NC_000001.11', 65564, 65573, '+', 0, False, md],
    ...              69036: ['NC_000001.11', 69036, 70005, '+', 0, False, md]},
    ...      'start_codon': {65564: ['NC_000001.11', 65564, 65567, '+', md]},
    ...      'stop_codon': {70005: ['NC_000001.11', 70005, 70008, '+', md]}}
    >>> e2 = {'gene': ['NC_000001.11', 65418, 71585, '+', md],
    ...       'coding': True,
    ...        'exon': {65418: ['NC_000001.11', 0, 15, '+', md],
    ...                 65519: ['NC_000001.11', 101, 155, '+', md],
    ...                 69036: ['NC_000001.11', 3618, 6167, '+', md]},
    ...        'CDS': {65564: ['NC_000001.11', 146, 155, '+', 0, False, md],
    ...                69036: ['NC_000001.11', 3618, 4587, '+', 0, False, md]},
    ...        'start_codon': {65564: ['NC_000001.11', 146, 149, '+', md]},
    ...        'stop_codon': {70005: ['NC_000001.11', 4587, 4590, '+', md]}}
    >>> update_coordinates(e) == e2
    True
    >>> e = {'gene': ['NC_000001.11', 998961, 1001052, '-', md],
    ...      'coding': True,
    ...      'exon': {999865: ['NC_000001.11', 999865, 1000097, '-', md],
    ...               999691: ['NC_000001.11', 999691, 999787, '-', md],
    ...               999525: ['NC_000001.11', 999525, 999613, '-', md],
    ...               998963: ['NC_000001.11', 998963, 999432, '-', md]},
    ...       'CDS': {999865: ['NC_000001.11', 999865, 999973, '-', 0,
    ...                        False, md],
    ...               999691: ['NC_000001.11', 999691, 999787, '-', 0,
    ...                        False, md],
    ...               999525: ['NC_000001.11', 999525, 999613, '-', 0,
    ...                        False, md],
    ...               999061: ['NC_000001.11', 999061, 999432, '-', 2,
    ...                        False, md]},
    ...      'start_codon': {999970: ['NC_000001.11', 999970, 999973, '-',
    ...                               md]},
    ...      'stop_codon': {999058: ['NC_000001.11', 999058, 999061, '-', md]}}
    >>> e2 = {'gene': ['NC_000001.11', 998961, 1001052, '-', md],
    ...       'coding': True,
    ...       'exon': {999865: ['NC_000001.11', 955, 1187, '-', md],
    ...                999691: ['NC_000001.11', 1265, 1361, '-', md],
    ...                999525: ['NC_000001.11', 1439, 1527, '-', md],
    ...                998963: ['NC_000001.11', 1620, 2089, '-', md]},
    ...       'CDS': {999865: ['NC_000001.11', 1079, 1187, '-', 0, False, md],
    ...               999691: ['NC_000001.11', 1265, 1361, '-', 0, False, md],
    ...               999525: ['NC_000001.11', 1439, 1527, '-', 0, False, md],
    ...               999061: ['NC_000001.11', 1620, 1991, '-', 2, False, md]},
    ...       'start_codon': {999970: ['NC_000001.11', 1079, 1082, '-', md]},
    ...       'stop_codon': {999058: ['NC_000001.11', 1991, 1994, '-', md]}}
    >>> update_coordinates(e) == e2
    True
    """
    keys_list = [sorted(d["exon"].keys()),
                 sorted(d["CDS"].keys()),
                 sorted(d["start_codon"].keys()),
                 sorted(d["stop_codon"].keys())]
    main_key = ["exon", "CDS", "start_codon", "stop_codon"]
    for m, keys in enumerate(keys_list):
        for key in keys:
            if d["gene"][3] == "+":
                d[main_key[m]][key][1] -= d["gene"][1]
                d[main_key[m]][key][2] -= d["gene"][1]
            else:
                tmp = d["gene"][2] - d[main_key[m]][key][2]
                d[main_key[m]][key][2] = d["gene"][2] - d[main_key[m]][key][1]
                d[main_key[m]][key][1] = tmp
    return d


def check_gene(dic_gene: Dict) -> Union[Dict, None]:
    """
    check if the gene in ``dic_gene`` is correct, adjust it and \
    return it.

    :param dic_gene: A dictionary containing a gene, its CDS, exon and \
    start and stop codon.
    :return: dic_gene adjusted
    """
    global COUNTERS
    if not check_complete(dic_gene):
        return None
    dic_gene = check_cds(dic_gene)
    if dic_gene is None:
        COUNTERS["gene_0_CDS"][0] += 1
        return None
    dic_gene = check_codon(dic_gene, "start_codon")
    if dic_gene is None:
        COUNTERS["gene_0_start"][0] += 1
        return None
    dic_gene = check_codon(dic_gene, "stop_codon")
    if dic_gene is None:
        COUNTERS["gene_0_stop"][0] += 1
        return None
    dic_gene = update_ft_num(dic_gene)
    dic_gene = update_coordinates(dic_gene)
    return check_overlapping_cds(dic_gene)


def check_dic(d: Dict) -> Dict:
    """
    Create a new dictionary of features based on ``d``

    :param d: Dictionary of feature
    :return: new dictionary.
    """
    n = {}
    for k in d:
        tmp = check_gene(d[k])
        if tmp is not None:
            n[k] = tmp
    return n


def recover_statistics(d: Dict) -> Dict[str, List]:
    """
    Create a dictionary of statistics.

    :param d: The parsed gtf
    :return: The statistics of the parsed GTF
    """
    stats = {"gene": [0, "Total kept genes"],
             "exon": [0, "Total kept exons"],
             "CDS": [0, "Total CDS kept"],
             "start_codon": [0, "Total start codon kept"],
             "stop_codon": [0, "Total stop codon kept"]}
    for gene in d:
        stats["gene"][0] += 1
        stats["exon"][0] += len(d[gene]["exon"])
        stats["CDS"][0] += len(d[gene]["CDS"])
        stats["start_codon"][0] += len(d[gene]["start_codon"])
        stats["stop_codon"][0] += len(d[gene]["stop_codon"])
    return stats


def save_stats(stats: Dict[str, List], output: Path = Path(".")) -> None:
    """
    Save the statistical data about the parsing of the gtf.

    :param stats: A gtf of statistical counts
    :param output: Folder that will contains report
    """
    outfile = output / "stats.txt"
    global COUNTERS
    with outfile.open("w") as outf:
        for k in COUNTERS:
            outf.write(f"{COUNTERS[k][0]} {COUNTERS[k][1]}\n")
        outf.write("ALl those features have been removed !\n\n")
        for k2 in stats:
            outf.write(f"{stats[k2][1]} : {stats[k2][0]}\n")


def main_parser(gtf: Path, output: Path) -> Dict:
    """
    parse a GTF.

    :param gtf: A gtf file
    :param output: Folder where the stat files will be created
    """
    d = parse_gtf(Path(gtf))
    d = check_dic(d)
    stats = recover_statistics(d)
    save_stats(stats, output)
    return d


if __name__ == "__main__":
    doctest.testmod()
