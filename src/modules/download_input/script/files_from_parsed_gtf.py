#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a \
fasta file containing genes sequence and a bed file containing, CDS, exons, \
start and stop codons for each gene.
"""

from Bio import SeqIO
from Bio.Seq import Seq
from pathlib import Path
from pyfaidx import Fasta
from typing import Dict, List, Tuple
from itertools import product
from datetime import datetime
from typing import TextIO


def write_fasta_file(fasta_file: Path, dic_gtf: Dict, output: Path,
                     prefix: str) -> Tuple[List[SeqIO.SeqRecord], Path]:
    """
    Write a fasta file containing gene.

    :param fasta_file: The fasta file containing the full genome
    :param dic_gtf: A dictionary containing the parsed \
    gtf file originating for the same assembly as the fasta file
    :param output: Folder where result will be created
    :param prefix: the name of the fasta file
    :return: The list of sequence records written in the fasta file
    """
    ds = Fasta(str(fasta_file))
    outfile = output / f"{prefix}_gene.fa"
    records = []
    for gene in dic_gtf:
        dg = dic_gtf[gene]["gene"]
        seq = Seq(str(ds[dg[0]][dg[1]:dg[2]]))
        if dg[3] == "-":
            seq = seq.reverse_complement()
        records.append(SeqIO.SeqRecord(seq, id=gene))
    with outfile.open("w") as outf:
        SeqIO.write(records, outf, "fasta")
    return records, outfile


def write_gene_size(records: List[SeqIO.SeqRecord], output: Path,
                    prefix: str) -> None:
    """
    Write a text file containing the size of each gene.

    :param records: List of gene sequences
    :param output: Path where the result will be created
    :param prefix: A prefix name for the output text file
    """
    outfile = output / f"{prefix}_gene_size.txt"
    with outfile.open("w") as outf:
        for record in records:
            outf.write(f"{record.id}\t{len(record.seq)}\n")


def write_bed_file(dic_gtf: Dict, output: Path, prefix: str) -> None:
    """
    Write a bed file containing feature of the gtf files.

    :param dic_gtf: A dictionary containing parsed gtf
    :param output: Folder where the bed file will be created
    :param prefix: The genome assembly name
    """
    colors = {"exon": '76,76,255', "CDS": "87,192,89",
              "start_codon": "230,231,65", "stop_codon": "250,92,93"}
    outfile = output / f"{prefix}_annotation.bed"
    first_prod = product(dic_gtf.keys(), colors.keys())
    with outfile.open("w") as outf:
        for gene, reg in first_prod:
            tmp = dic_gtf[gene][reg]
            for loc in tmp:
                score = tmp[loc][4] if reg == "CDS" else "."
                if reg == "CDS" and tmp[loc][5]:
                    score = "multiple"
                row = [gene, tmp[loc][1], tmp[loc][2],
                       f"{reg}-{gene}_{loc}", score, "+", tmp[loc][1],
                       tmp[loc][2], colors[reg]]
                outf.write("\t".join(map(str, row)) + "\n")


def write_get_header(gtf: TextIO, prefix: str) -> None:
    """
    Write a header in the file `gtf`.

    :param gtf: An open file
    :param prefix: The file prefix
    """
    gtf.write(f"#gtf-version 2.2\n")
    gtf.write(f"#!genome-build-accession {prefix}\n")
    gtf.write("#!annotation-date {:%D}\n".format(datetime.now()))


def write_gene_row(gene: str, dic_gtf: Dict, gtf: TextIO) -> None:
    """
    Write a row corresponding to a gene.

    :param gene: A gene name
    :param dic_gtf: A dictionary containing data about features to write on \
    the gtf file
    :param gtf:  An open file
    """
    gened = dic_gtf[gene]["gene"]
    row = [gene, gened[4]["source"], "gene", 1, gened[2] - gened[1],
           '.', '+', '.', gened[4]["data"]]
    gtf.write("\t".join(map(str, row)) + "\n")


def write_gtf_features(gene: str, dic_gtf: Dict, gtf: TextIO):
    """
    Write a row corresponding to a gtf feature.

    :param gene: A gene name
    :param dic_gtf: A dictionary containing data about features to write on \
    the gtf file
    :param gtf:  An open file
    """
    features = ["exon", "CDS", "start_codon", "stop_codon"]
    for reg in features:
        tmp = dic_gtf[gene][reg]
        for loc in tmp:
            data = tmp[loc][6] if reg == "CDS" else tmp[loc][4]
            score = tmp[loc][4] if reg == "CDS" else "."
            if reg == "CDS" and tmp[loc][5]:
                score = "multiple"
            row = [gene, data["source"], reg, tmp[loc][1] + 1, tmp[loc][2],
                   ".", "+", score, data["data"]]
            gtf.write("\t".join(map(str, row)) + "\n")


def write_gtf_file(dic_gtf: Dict, output: Path, prefix: str) -> None:
    """
    Write a new gtf file.

    :param dic_gtf: A dictionary containing parsed gtf
    :param output: Folder where the bed file will be created
    :param prefix: The genome assembly name
    """
    outfile = output / f"{prefix}_annotation.gtf"
    with outfile.open("w") as outf:
        write_get_header(outf, prefix)
        for gene in dic_gtf.keys():
            write_gene_row(gene, dic_gtf, outf)
            write_gtf_features(gene, dic_gtf, outf)
