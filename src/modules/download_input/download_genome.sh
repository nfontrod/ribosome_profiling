#!/bin/bash

if [ -z "$1" ]; then
  profile="docker"
fi
profile=$1

if [ -z "$2" ] && [ "$profile" = "docker" ]; then
  docker container run -it --rm -v $PWD/data:/data lbmc/genome_download:0.0 python3 -m script -o /data
elif [ "$profile" = "docker" ]; then
 docker container run -it --rm -v $PWD/data:/data lbmc/genome_download:0.0 python3 -m script -u $2 -o /data
elif [ -z "$2" ] && [ "$profile" = "singularity" ]; then
  singularity exec -C -B $PWD/data:/data  ./bin/lbmc-genome_download-0.0.img python3 -m script -o /data
else
  singularity exec -C -B $PWD/data:/data ./bin/lbmc-genome_download-0.0.img python3 -m script -u $2 -o /data
fi