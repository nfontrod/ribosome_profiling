#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to find the codons \
corresponding either to the A, P or E sites of each footprint from a bam file.
"""

import pysam
import lazyparser as lp
from typing import Dict, List, Optional, Tuple
import pandas as pd
from pathlib import Path
from doctest import testmod
from Bio.Seq import Seq


class MultipleCDSOverlapError(Exception):
    pass


def load_offset_dic(offset: str, site: str) -> Dict:
    """
    Get a dictionary linking the footprint length to the offset from their \
    5'end to their site of interest.

    :param offset: A file containing offsets
    :param site: The ribosome site of interest (A, P, E)
    :return: A dictionary linking the footprint length to the offset from \
    their 5'end to their site of interest.
    """
    df = pd.read_csv(offset, sep="\t")
    df.index = df["fp_size"]
    df.drop('fp_size', axis=1, inplace=True)
    dic = df.to_dict()['offset']
    for fps in dic.keys():
        if site == "A":
            dic[fps] += 3
        elif site == "E":
            dic[fps] -= 3
    return dic


def load_cds(bed: str) -> List[List]:
    """
    From a bed file containing exons, CDS and start and stop codons \
    return a list of CDS if they have a unique orf

    :param bed: A bed file containing containing exons, CDS and start \
    and stop codons
    :return: A dictionary containing cds if they have a unique ORF.
    """
    list_cds = []
    with open(bed, 'r') as bedin:
        for line in bedin:
            cline = line.replace("\n", "").split("\t")
            if cline[3].startswith("CDS-") and cline[4] in ['0', '1', '2']:
                list_cds.append([cline[0], int(cline[1]), int(cline[2]),
                                 cline[3], int(cline[4]), cline[5]])
    return list_cds


def index_cds(list_cds: List[List]) -> Dict:
    """
    Create a dictionary to index the cds by the gene they belong.

    :param list_cds: List of coding sequence
    :return: dictionary linking each gene to their CDS
    """
    dic = {}
    for cds in list_cds:
        if cds[0] not in dic:
            dic[cds[0]] = [cds]
        else:
            dic[cds[0]].append(cds)
    return dic


def block_adapter(fp: pysam.AlignedSegment) -> List[List]:
    """
    Get the real start and stop of footprint block.

    :param fp: The reads of interest
    :return: The aligned block without gap of the read.
    """
    blocks = fp.get_blocks()
    new_blocks = [list(b) for b in blocks]
    new_blocks[0][0] -= fp.query_alignment_start
    new_blocks[-1][-1] += fp.query_length - fp.query_alignment_end
    return new_blocks


def good_overlap(cds: List, coordinates: List, max_outside_nt: int):
    """
    Says if a footprint with the coordinates `coordinates` overlaps the cds \
    `cds`.

    :param cds: A cds
    :param coordinates: The coordinate of a footprints
    :param max_outside_nt: The maximum number of nucleotides in footprint \
    that are not contained in the CDS allowed.
    :return: True if the footprint overlaps correctly the CDS, false else.

    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [150, 170], 0)
    True
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [98, 120], 2)
    True
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [98, 120], 1)
    False
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [180, 202], 2)
    True
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [180, 202], 1)
    False
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [98, 202], 2)
    True
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [98, 202], 1)
    False
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [99, 202], 1)
    False
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [98, 201], 1)
    False
    >>> good_overlap(['NOCL2', 100, 200, 'NOCL2_name', 0, '+'], [99, 201], 1)
    True
    """
    return cds[1] - max_outside_nt <= coordinates[0] < \
        cds[2] + max_outside_nt and cds[1] - max_outside_nt < \
        coordinates[1] <= cds[2] + max_outside_nt


def get_overlapping_cds(list_cds: List[list], coordinates: List,
                        max_outside_nt: int, fp_name: str) -> List:
    """
    Return the cds overlapping the fooprint.

    :param list_cds: A list of CDS
    :param coordinates: The footprint start coordinates
    :param max_outside_nt: The maximum number of nucleotides in footprint \
    that are not contained in the CDS allowed.
    :param fp_name: The footprint name
    :return: The list of cds overlapping the footprint

    >>> get_overlapping_cds([['NOCL2', 100, 200, 'NOCL2_name', 0, '+'],
    ...                      ['NOCL2', 400, 800, 'NOCL2_name', 2, '+']],
    ...                      [150, 170], 0, 'fp_test')
    ['NOCL2', 100, 200, 'NOCL2_name', 0, '+']
    """
    overlaping_cds = [cds for cds in list_cds
                      if good_overlap(cds, coordinates, max_outside_nt)]
    if len(overlaping_cds) > 1:
        raise MultipleCDSOverlapError(f"The footprint {fp_name} overlap "
                                      f"{len(overlaping_cds)} CDS: "
                                      f"{overlaping_cds}")
    if not overlaping_cds:
        return []
    return overlaping_cds[0]


def get_site_codons(cds: List, coordinates: List, offset: int, delta: int,
                    fp: pysam.AlignedSegment) -> List:
    """
    Return the codon in the ribosome site and the one around it.

    :param cds: A cds
    :param coordinates: The coordinate of a footprint
    :param offset: The offset of interest
    :param delta: the number of surrounding codons around the ribosome site
    :param fp: the footprint of interest
    :return: The list of codon around the A site

    >>> mfp = pysam.AlignedSegment()
    >>> mfp.query_sequence = 'ATGGCGAAAATG'
    >>> mfp.query_name = "A"
    >>> mfp.tags = [('AS', -4), ('XN', 0), ('XM', 0), ('XO', 0), ('XG', 0),
    ... ('NM', 0), ('MD', '12'), ('YT', 'UU'), ('XS', '+'), ('NH', 1)]
    >>> acds = ['A', 50, 100, 'CDS-A', '0', '+']
    >>> get_site_codons(acds, [71, 83], 3, 1, mfp)
    ['ATG', 'GCG', 'AAA']
    >>> get_site_codons(acds, [71, 83], 4, 1, mfp)
    ['ATG', 'GCG', 'AAA']
    >>> get_site_codons(acds, [71, 83], 5, 1, mfp)
    ['ATG', 'GCG', 'AAA']
    >>> get_site_codons(acds, [71, 83], 6, 1, mfp)
    ['GCG', 'AAA', 'ATG']
    >>> mfp.query_sequence = 'TGGCGAAAATG'
    >>> mfp.tags = [('AS', -4), ('XN', 0), ('XM', 0), ('XO', 0), ('XG', 0),
    ... ('NM', 0), ('MD', '11'), ('YT', 'UU'), ('XS', '+'), ('NH', 1)]
    >>> get_site_codons(acds, [72, 83], 4, 1, mfp)
    ['GCG', 'AAA']
    >>> acds = ['A', 50, 100, 'CDS-A', '1', '+']
    >>> mfp.query_sequence = 'ATGGCGAAAATG'
    >>>     >>> mfp.tags = [('AS', -4), ('XN', 0), ('XM', 0), ('XO', 0), ('XG', 0),
    ... ('NM', 0), ('MD', '12'), ('YT', 'UU'), ('XS', '+'), ('NH', 1)]
    >>> get_site_codons(acds, [72, 83], 3, 1, mfp)
    ['ATG', 'GCG', 'AAA']
    >>> mfp.query_sequence = 'ATGGCGAAAATGC'
    >>> mfp.tags = [('AS', -4), ('XN', 0), ('XM', 0), ('XO', 0), ('XG', 0),
    ... ('NM', 0), ('MD', '13'), ('YT', 'UU'), ('XS', '+'), ('NH', 1)]
    >>> get_site_codons(acds, [72, 83], 3, -1, mfp)
    ['ATG', 'GCG', 'AAA', 'ATG']
    >>> get_site_codons(acds, [45, 58], 7, -1, mfp)
    ['AAA', 'ATG']
    >>> get_site_codons(acds, [45, 58], 3, -1, mfp)
    Traceback (most recent call last):
    ...
    IndexError: The footprint A has it's a site before cds
    """
    soft_clip_start = fp.query_alignment_start
    soft_clip_end = fp.query_alignment_end - fp.query_length
    if soft_clip_end != 0:
        query_sequence = fp.query_sequence[0:soft_clip_start] + \
                         fp.get_reference_sequence() + \
                         fp.query_sequence[soft_clip_end:]
    else:
        query_sequence = fp.query_sequence[0:fp.query_alignment_start] + \
                         fp.get_reference_sequence()
    query_sequence = query_sequence.upper()
    if len(query_sequence) != fp.query_length:
        raise ValueError(f'{fp.query_name} has a corrected sequence with a '
                         f'length different than the original sequence')
    ribo_site = coordinates[0] + offset
    if ribo_site < cds[1]:
        raise IndexError(f"The footprint {fp.query_name} has it's a site "
                         f"before cds")
    cds_start = cds[1] + int(cds[4])
    diff = coordinates[0] - cds_start
    if diff < 0:
        fp_offset = abs(diff)
    else:
        fp_offset = 1 if diff % 3 == 2 else 2 if diff % 3 == 1 else 0
    if fp_offset < soft_clip_start:
        fp_offset = fp_offset + soft_clip_start + \
                     (1 if (soft_clip_start % 3) == 2
            else 2 if (soft_clip_start % 3) == 1 else 0)
    new_seq = query_sequence[fp_offset:]
    if soft_clip_end < 0:
        new_seq = new_seq[:soft_clip_end]
    elif soft_clip_end > 0:
        raise ValueError(f"soft_clip end should be negatif not {soft_clip_end}")
    if delta == -1:
        codon_seq = []
        for i in range(0, len(new_seq) - 2, 3):
            codon_tmp = new_seq[i:i+3]
            if codon_tmp in ['TAA', 'TAG', 'TGA']:
                break
            codon_seq.append(codon_tmp)
        return codon_seq
    dist = (offset - fp_offset) - (offset - fp_offset) % 3
    delta_loc = list(range(delta * -3, (delta + 1) * 3, 3))
    codon_seq = []
    dl_pos = []
    for dl in delta_loc:
        if (dist + dl) >= 0:
            codon_tmp = new_seq[dist + dl:dist + 3 + dl]
            if codon_tmp in ['TAA', 'TAG', 'TGA']:
                    break
            dl_pos.append(dl)
            codon_seq.append(codon_tmp)
    if 0 not in dl_pos:
        return []
    return codon_seq


def get_codon_dic() -> Dict:
    """
    :return: A dictionary containing every coding codons (key) \
    associated with 0 as value
    """
    ntl = list("ATGC")
    d = {a + b + c: 0 for a in ntl for b in ntl for c in ntl}
    del(d["TGA"])
    del(d["TAG"])
    del(d["TAA"])
    return d


def get_aa_dic() -> Dict:
    """
    :return: A dictionary containing every aa (key) \
    associated with 0 as value
    """
    return {a: 0 for a in list('RHKDESTNQCGPAILMFWYV')}


def create_codon_table(codon_list: List[str], fp: pysam.AlignedSegment,
                       overlapping_cds: List, dict_count: Dict
                       ) -> Dict:
    """
    From a list of codon/ amino acids,  return a dataframe showing the \
    number of (codon/amino acids) around the ribosome site of interest.

    :param codon_list: A list of codon around the ribosome site of interest
    :param fp: The footprint of interest
    :param overlapping_cds: The cds overlapping ft
    :param dict_count: Dictionary linking each size to the count of codon, \
    aa inside it
    :return: a dataframe with the number of codons/amino acids inside \
    `codon_list`
    """
    if fp.query_length not in dict_count:
        dict_count[fp.query_length] = {'codon': get_codon_dic(),
                                       "aa": get_aa_dic()}

    for codon in codon_list:
        try:
            dict_count[fp.query_length]['codon'][codon] += 1
        except KeyError:
            print(f"{fp.query_name} found inside {overlapping_cds} contains "
                  f"a stop codon. fp: {fp} - {codon_list}")
            exit(1)
        dict_count[fp.query_length]['aa'][str(Seq(codon).translate())] += 1
    return dict_count


def get_site_composition(fp: pysam.AlignedSegment, dic_offset: Dict,
                         delta: int, dic_cds: Dict, max_outside_nt: int,
                         dic_count: Dict) -> Tuple[Dict, Optional[int]]:
    """
    Return a table containing

    :param fp: The footprint of interest
    :param dic_offset: A dictionary of offset
    :param delta: The number of codons to consider around the ribosome site
    :param dic_cds: The dictionary of cds organised by their hosting gene.
    :param max_outside_nt: The maximum number of nucleotides in footprint \
    that are not contained in the CDS allowed.
    :param dic_count: Dictionary linking each size to the count of codon, \
    aa inside it
    :return: dic_count updated + None if it wasn't updated because no \
    cds overlap the footprint `fp`, 0 if no codon where found, 1 if it was \
    updated
    """
    blocks = block_adapter(fp)
    try:
        overlaping_cds = get_overlapping_cds(dic_cds[fp.reference_name],
                                             blocks[0], max_outside_nt,
                                             fp.query_name)
    except KeyError:
        return dic_count, None
    if len(overlaping_cds) == 0:
        return dic_count, None
    codons_list = get_site_codons(overlaping_cds, blocks[0],
                                  dic_offset[fp.query_length],
                                  delta, fp)
    if len(codons_list) == 0:
        return dic_count, 0
    return create_codon_table(codons_list, fp, overlaping_cds, dic_count), 1


def dic_to_dataframe(dic_count: Dict, sample_name: str) -> pd.DataFrame:
    """
    Convert the dictionary containing the count of each codon/amino acids \
    around the footprint site of interest into a dataframe.

    :param dic_count: dictionary containing the count of each codon/amino acids \
    around the footprint site of interest
    :param sample_name: The name of the sample of interest
    :return: The corresponding dataframe
    """
    new_dic = {"count": [], "ft": [], 'ft_type': [], 'fp_size': []}
    for fp_size in dic_count:
        for ft_type in dic_count[fp_size]:
            for ft in dic_count[fp_size][ft_type]:
                new_dic["count"].append(dic_count[fp_size][ft_type][ft])
                new_dic["ft"].append(ft)
                new_dic["ft_type"].append(ft_type)
                new_dic["fp_size"].append(fp_size)
    df = pd.DataFrame(new_dic)
    df['sample'] = [sample_name] * df.shape[0]
    return df


def get_full_composition_table(dic_offset: Dict, bam_file: Path,
                               sample_name: str, delta: int, dic_cds: Dict,
                               max_outside_nt: int):
    """
    Create a new bam file corresponding only to the ribosome site of \
    interest.

    :param dic_offset: A dictionary of offsets to the site of interest
    :param bam_file: The bam file of aligned footprint
    :param sample_name: The name of the sample of interest
    :param delta: the number of codons to consider around the ribosome \
    site
    :param dic_cds: A dictionary linking the gene (key) to the cds it contains
    :param max_outside_nt: The maximum number of nucleotides in footprint \
    that are not contained in the CDS allowed.
    """
    dic_count = {}
    total_fp = 0
    fp_good_size = 0
    fp_not_in_cds = 0
    fp_no_site = 0
    good_fp = 0
    in_bam = pysam.AlignmentFile(bam_file, 'rb')
    for fp in in_bam.fetch():
        total_fp += 1
        if fp.query_length in dic_offset and not fp.is_unmapped:
            fp_good_size += 1
            dic_count, info = get_site_composition(fp, dic_offset, delta,
                                                   dic_cds,
                                                    max_outside_nt, dic_count)
            if info is None:
                fp_not_in_cds += 1
            elif info == 0:
                fp_no_site += 1
            else:
                good_fp += 1
    print(f"{total_fp} footprints were found in the bam file "
          f"{Path(bam_file).name}")
    print(f"{fp_good_size} footprints had a size with a known offset")
    print(f"{fp_not_in_cds} footprints did not overlap a CDS")
    print(f"For {fp_no_site} footprints, codons around the wanted site "
          f"where not found")
    print(f"{good_fp} footprints had their composition successfully retrieved")
    return dic_to_dataframe(dic_count, sample_name)


def select_size(df_count: pd.DataFrame, min_fp_size: int,
                max_fp_size: int) -> pd.DataFrame:
    """
    Select only the footprint having a size of interest and group their \
    codon, amino adic counts.

    :param df_count: Dataframe containing the count of each codon around \
    the ribosome site of interest
    :param min_fp_size: The minimum size require to consider a footprint
    :param max_fp_size: The maximum size require to consider a footprint
    :return: The dataframe containing the sum of counts of covered codons \
    around/covering the ribosome site of interest
    """
    good_sizes = list(range(min_fp_size, max_fp_size + 1))
    df_count = df_count.loc[df_count['fp_size'].isin(good_sizes), :].copy()
    df_count.drop('fp_size', axis=1, inplace=True)
    df = df_count.groupby(['ft', 'ft_type', 'sample', 'replicate'])\
        .sum().reset_index()
    df['size'] = [f"{min_fp_size}-{max_fp_size}"] * df.shape[0]
    tot_count = sum(df.loc[df['ft_type'] == 'codon', 'count'].to_list())
    df['tot_count'] = [tot_count] * df.shape[0]
    return df


@lp.parse(bam="file", site=["A", "P", "E"], bed="file", offset_file="file",
          delta=range(-1, 10))
def ribo_site_finder(bam: str, site: str, sample_name: str, bed: str,
                     offset_file: str, replicate: str,
                     min_fp_size: int, max_fp_size: int,
                     delta: int = 0,
                     max_outside_nt: int = 3, output: str = "."):
    """
    Create a table containing the count of each codon/amino acid around \
    the ribosome site of interest

    :param bam: A bam file of aligned footprints
    :param site: The ribosome site of interest (A, P, or E site)
    :param sample_name: The name of the sample of interest
    :param bed: An annotation file containing exons, cds, start and stop \
    codons of each gene
    :param min_fp_size: The minimum footprint size of interest
    :param max_fp_size: The maximum footprint size of interest
    :param replicate: The replicate name of interest
    :param delta: The number of codon around the ribosome site to consider
    :param offset_file: A table file containing the offset to the p-sites \
    according to footprint length
    :param max_outside_nt: The maximum number of nucleotides in footprint \
    that are not contained in the CDS allowed.
    :param output: Folder where the count file will be created
    """
    offset_dic = load_offset_dic(offset_file, site)
    list_cds = load_cds(bed)
    dic_cds = index_cds(list_cds)
    df = get_full_composition_table(offset_dic, Path(bam), sample_name,
                                    delta, dic_cds, max_outside_nt)
    size = f"{min_fp_size}-{max_fp_size}"
    if delta == -1:
        outfile = f"codon_counts_in_footprints_{sample_name}_{size}.txt"
    else:
        outfile = f"codon_counts_around_{site}_site_delta_{delta}_" \
                  f"{sample_name}_{size}.txt"
    df['replicate'] = [replicate] * df['count'].shape[0]
    df = select_size(df, min_fp_size, max_fp_size)
    df.to_csv(Path(output) / outfile, sep="\t", index=False)


if __name__ == "__main__":
    # testmod()
    ribo_site_finder()
