#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to resize the genomic features \
loacted inside an input bed file
"""

from pathlib import Path
import pandas as pd
from typing import Dict, List, Tuple
from math import ceil
import lazyparser as lp
import subprocess as sp


class ChrNotFoundError(Exception):
    """
    An Error to raise is the size of a chromosome is not defined
    """
    pass


def load_bed_file(bed: Path, tmp_file: bool = False) -> pd.DataFrame:
    """

    :param bed: A bed file
    :param tmp_file:: True to ignore name column (the bed must have 5 \
    columns) else the bed must have 6 columns
    :return: the loaded bed file

    >>> b = Path(__file__).parent / "tests" / "files" / "test.bed"
    >>> load_bed_file(b)
          chr   start    stop    name  score strand
    0    ENO1   15789   15800  peak_1   23.6      +
    1    ENO1   17257   17280  peak_2   15.9      +
    2     PGD   19771   19779  peak_3    8.4      +
    3     PGD   19780   19785  peak_4    5.0      +
    4  UQCRHL     313     314  peak_5   92.3      +
    5    UBR4  126455  126459  peak_6   42.1      +
    6    UBR4  126484  126490  peak_7   11.5      +
    >>> b = Path(__file__).parent / "tests" / "files" / "test_tmp.bed"
    >>> load_bed_file(b, True)
          chr   start    stop  score strand
    0    ENO1   15789   15800   23.6      +
    1    ENO1   17257   17280   15.9      +
    2     PGD   19771   19779    8.4      +
    3     PGD   19780   19785    5.0      +
    4  UQCRHL     313     314   92.3      +
    5    UBR4  126455  126459   42.1      +
    6    UBR4  126484  126490   11.5      +
    """
    names = ["chr", "start", "stop", "name", "score", "strand"]
    if tmp_file:
        names = names[0:3] + names[4:]
    return pd.read_csv(bed, sep="\t",
                       names=names)


def load_chromosome_size_file(size_file: Path) -> Dict[str, int]:
    """
    Load the file containing the size of every chromosome

    :param size_file: A file containing the size of every chromosome
    :return: A dictionary linking each chromosome to its size

    >>> load_chromosome_size_file(Path(__file__).parent / "tests" / "files" /
    ... "test_size.txt" )
    {'ENO1': 15810, 'PGD': 21448, 'UQCRHL': 320, 'UBR4': 139825}
    """
    dic = {}
    with size_file.open("r") as size_in:
        for line in size_in:
            list_line: List[str] = line.strip().split("\t")
            dic[list_line[0]] = int(list_line[1])
    return dic


def check_chromosomes(df: pd.DataFrame, dic_size: Dict[str, int]) -> None:
    """
    Check if the chromosomes inside df have a defined size in ``dic_size``.

    :param df: A dataframe containing genomic features
    :param dic_size: A dictionary containing the size of each chromosome

    >>> d = pd.DataFrame({"chr": list("AB")})
    >>> ds = {"A": 10, "B": 20}
    >>> check_chromosomes(d, ds)
    >>> d = pd.DataFrame({"chr": list("ABD")})
    """
    for chr in df["chr"].unique():
        try:
            _ = dic_size[chr]
        except KeyError:
            raise ChrNotFoundError(f"The chromosome {chr} wasn't found in the "
                                   f"chromosome size file !")



def resize_too_large_feature(start: int, stop: int, size: int
                             ) -> Tuple[int, int]:
    """
    Resize the interval to a given size.

    :param start: The start position of the interval
    :param stop: The stop position of the interval
    :param size: The target size for the interval
    :return: The new start and stop position

    >>> resize_too_large_feature(0, 10, 5)
    (2, 7)
    >>> resize_too_large_feature(0, 10, 2)
    (4, 6)
    >>> resize_too_large_feature(0, 11, 2)
    (5, 7)
    >>> resize_too_large_feature(0, 11, 3)
    (4, 7)
    >>> resize_too_large_feature(10, 20, 5)
    (12, 17)
    """
    middle = ceil((stop + start) / 2)
    return int(middle - (size / 2)), int(middle + (size / 2))


def resize_too_small_feature(start: int, stop: int, size: int,
                             max_size: int) -> Tuple[int, int]:
    """
    Resize genomic intervals that are too large.

    :param start: The start position of the interval
    :param stop: The stop position of the interval
    :param size: The target size for the interval
    :param max_size: The maximum size the interval can reach
    :return: The new start and stop position

    >>> resize_too_small_feature(10, 20, 20, 100)
    (5, 25)
    >>> resize_too_small_feature(10, 20, 21, 100)
    (5, 26)
    >>> resize_too_small_feature(10, 20, 22, 100)
    (4, 26)
    >>> resize_too_small_feature(10, 20, 30, 100)
    (0, 30)
    >>> resize_too_small_feature(10, 20, 40, 100)
    (0, 35)
    >>> resize_too_small_feature(10, 20, 40, 30)
    (0, 30)
    """
    resize = (size - (stop - start)) / 2
    return max(start - int(resize), 0), min(stop + ceil(resize), max_size)


def resize_interval(row: pd.Series, size: int, dic_size: Dict[str, int],
                    resize_big_interval: bool = False) -> pd.Series:
    """
    Resize an interval to the wanted size.

    :param row: A bed row
    :param size: The target size for the interval
    :param dic_size: A dictionary containing the size of every chromosome
    :param resize_big_interval: True to resize too large intervals, False else
    :return: The resized interval

    >>> d = pd.Series({"chr": "ULA", "start": 10, "stop": 20, "name": "lol",
    ... "strand": "+"})
    >>> resize_interval(d.copy(), 20, {"ULA": 50})
    chr       ULA
    start       5
    stop       25
    name      lol
    strand      +
    dtype: object
    >>> resize_interval(d.copy(), 20, {"ULA": 20})
    chr       ULA
    start       5
    stop       20
    name      lol
    strand      +
    dtype: object
    >>> resize_interval(d.copy(), 5, {"ULA": 20})
    chr       ULA
    start      10
    stop       20
    name      lol
    strand      +
    dtype: object
    >>> resize_interval(d.copy(), 5, {"ULA": 20}, True)
    chr       ULA
    start      12
    stop       17
    name      lol
    strand      +
    dtype: object

    """
    interval_size = row.stop - row.start
    if interval_size < size:
        start, stop = resize_too_small_feature(row.start, row.stop, size,
                                               dic_size[row.chr])
        row.start = start
        row.stop = stop
    elif resize_big_interval and interval_size > size:
        start, stop = resize_too_large_feature(row.start, row.stop, size)
        row.start = start
        row.stop = stop
    return row


def create_new_bed(df: pd.DataFrame, size: int, dic_size: Dict[str, int],
                   resize_big_interval: bool = False) -> pd.DataFrame:
    """
    Resize all intervals in a bed file to the wanted size.

    :param df: A bed dataframe
    :param size: The target size for the interval
    :param dic_size: A dictionary containing the size of every chromosome
    :param resize_big_interval: True to resize too large intervals, False else
    :return: The resized bed file
    """
    list_series = [resize_interval(df.iloc[i, :].copy(), size, dic_size,
                        resize_big_interval) for i in range(df.shape[0])]
    return pd.DataFrame(list_series)


def merge_bed(bed: Path) -> Path:
    """
    Execute bedtools to merge intervals.

    :param bed: A bed file with resized features
    """
    new_tmp = Path("./tmp_merged.bed")
    cmd = f"bedtools merge -i {bed} -c 5,6 -o mean,distinct > {new_tmp}"
    sp.check_call(cmd, shell=True)
    return new_tmp


def add_name_col(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add a column name to a dataframe corresponding to a bed file

    :param df: A dataframe corresponding to a bed file
    :return: Add a name column
    >>> d = pd.DataFrame({
    ... 'chr': ['ENO1', 'ENO1', 'PGD', 'PGD', 'UQCRHL', 'UBR4', 'UBR4'],
    ... 'start': [15789, 17257, 19771, 19780, 313, 126455, 126484],
    ... 'stop': [15800, 17280, 19779, 19785, 314, 126459, 126490],
    ... 'score': [23.6, 15.9, 8.4, 5.0, 92.3, 42.1, 11.5],
    ... 'strand': ['+', '+', '+', '+', '+', '+', '+']})
    >>> add_name_col(d)
          chr   start    stop    name  score strand
    0    ENO1   15789   15800  peak_1   23.6      +
    1    ENO1   17257   17280  peak_2   15.9      +
    2     PGD   19771   19779  peak_3    8.4      +
    3     PGD   19780   19785  peak_4    5.0      +
    4  UQCRHL     313     314  peak_5   92.3      +
    5    UBR4  126455  126459  peak_6   42.1      +
    6    UBR4  126484  126490  peak_7   11.5      +
    """
    df["name"] = [f"peak_{i + 1}" for i in range(df.shape[0])]
    return df[["chr", "start", "stop", "name", "score", "strand"]]



@lp.parse(bed="file", genome_size="file")
def bed_resizer(bed: str, size: int, genome_size: str, outfile: str,
                resize_big_interval: bool = False,
                resize_after_merge: bool = True) -> None:
    """

    :param bed: A bed file containing genomic interval
    :param size: The size of interval we want to have
    :param genome_size: File containing the chromosome size
    :param outfile: The name of the outfile
    :param resize_big_interval: True to resize too large intervals, False \
    else (default False)
    :param resize_after_merge: True to resize after the merge else False \
    (default True)
    :return:
    """
    df = load_bed_file(Path(bed))
    dic_size = load_chromosome_size_file(Path(genome_size))
    check_chromosomes(df, dic_size)
    new_bed = create_new_bed(df, size, dic_size, resize_big_interval)
    tmp_file = Path("./tmp.bed")
    new_bed.to_csv(tmp_file, sep="\t", index=False, header=False)
    tmp_2_file = merge_bed(tmp_file)
    tmp_file.unlink()
    df = load_bed_file(tmp_2_file, True)
    df = add_name_col(df)
    if resize_after_merge:
       df = create_new_bed(df, size, dic_size, True)
    df.to_csv(outfile, sep="\t", index=False, header=False)


if __name__ == "__main__":
    bed_resizer()