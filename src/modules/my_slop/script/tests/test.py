#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description:
"""

import doctest
import unittest
import sys
from pathlib import Path

sys.path.insert(0, str(Path(__file__).parents[1].resolve()))


def load_tests(loader, tests, ignore):
    tests.addTest(doctest.DocTestSuite("my_slop"))
    return tests


if __name__ == "__main__":
    unittest.main()