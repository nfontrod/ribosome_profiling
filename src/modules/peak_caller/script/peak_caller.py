#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to identify peaks in \
ribosome profiling data
"""

import pyBigWig as pbw
from pathlib import Path
from typing import Dict, List, Optional
import numpy as np
import lazyparser as lp
import multiprocessing as mp
from functools import reduce


def get_bed_cds(bed: Path) -> Dict:
    """
    :param bed: A bed file contaning exons and CDS for every gene \
    of a genome.
    :return: A dictionary containing for each gene, the coordinates of \
    every CDS
    """
    d = {}
    with bed.open("r") as bfile:
        for line in bfile:
            cline = line.replace("\n", "").split("\t")
            if cline[3].startswith("CDS"):
                if cline[0] not in d:
                    d[cline[0]] = [[int(cline[1]), int(cline[2])]]
                else:
                    d[cline[0]].append([int(cline[1]), int(cline[2])])
    return d


def found_peaks_coordinates(coverage: np.array, threshold: float
                            ) -> Optional[List]:
    """
    Gives the interval of every regions having a coverage values > threshold.

    :param coverage: A coverage list of values
    :param threshold: A threshold above which, we consider a peak a coverage.
    :return: The List of peaks coordinates
    """
    peaks = []
    for i in range(coverage.size - 1):
        if i == 0 and coverage[i] > threshold:
            peaks.append(i)
            if coverage[i + 1] < threshold:
                peaks.append(i + 1)
        elif coverage[i] <= threshold < coverage[i + 1]:
            peaks.append(i + 1)
        elif coverage[i] > threshold >= coverage[i + 1]:
            peaks.append(i + 1)
    if coverage[-1] > threshold:
        peaks.append(coverage.size)
    if not peaks:
        return None
    if len(peaks) % 2 != 0:
        raise IndexError(f"The length of peak should be even "
                         f"(not {len(peaks)})")
    return [[peaks[i], peaks[i + 1]] for i in range(0, len(peaks) - 1, 2)]


def smoothing_window(coverage: np.array, window: int = 9) -> np.array:
    """
    recovering mean in a given window.

    :param coverage:The coverage values
    :param window: A window size
    :return: smoothed coverage values

    >>> smoothing_window(np.array([0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0]), 5)
    array([0. , 1. , 2. , 3. , 3.6, 3.8, 3.6, 3. , 2. , 1. , 0. ])
    >>> smoothing_window(np.array([0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0]), 1)
    array([0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0])
    >>> smoothing_window(np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]), 5)
    array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    """
    scoverage = np.array([])
    if window == 1:
        return  coverage
    if np.max(coverage) == 0:
        return coverage
    ext_size = int((window - 1) / 2)
    scoverage = np.concatenate([scoverage, coverage[0:ext_size]])
    cov_center = [
        np.mean(coverage[i:i + window])
        for i in range(len(coverage) - ext_size * 2)
    ]
    cov_center = np.concatenate([cov_center, coverage[-ext_size:]])
    return np.concatenate([scoverage, np.array(cov_center)])


def find_gene_peak(b: str, cds_coord: List, gene: str,
                   window: int = 9, min_coverage: int = 0) -> Optional[Dict]:
    """
    Find all peaks located in the gene, ``gene``.

    :param b: A bigwigFile object
    :param cds_coord: List of CDS coordinate in the gene
    :param gene: The current gene of interest
    :param window: A window size
    :param min_coverage: the minimum required coverage in a gene to \
    be able to detect peaks
    :return: A dictionary with the found peaks
    """
    bw = pbw.open(b)
    coverage = np.array([])
    full_cov = bw.values(gene, 0, bw.chroms(gene))
    smoothed_cov = smoothing_window(full_cov, window)
    for cds in cds_coord:
        coverage = np.concatenate([coverage, smoothed_cov[cds[0]:cds[1]]])
    coverage = np.asarray(coverage)
    if np.mean(coverage) < min_coverage:
        return None
    n0_cov = coverage[coverage > 0]
    if n0_cov.size > 0:
        gstd = np.std(n0_cov)
        gmed = np.median(n0_cov)
    else:
        gstd = 0
        gmed = 0
    threshold = gmed + gstd * 3
    peaks = found_peaks_coordinates(np.asarray(full_cov), threshold)
    if peaks is None:
        return peaks
    return {gene: {"peaks": peaks, "score": threshold}}


def combine_dic(a: Dict, b: Dict) -> Dict:
    """
    Combine dictionaries together.

    :param a: A dictionary a
    :param b: A dictionary b
    :return: a and b combined
    """
    return dict(a, **b)


def get_peaks_dict(bw: str, dic_bed: Dict, processes: int = 1,
                   window: int = 9, min_coverage: int = 0) -> Dict:
    """
    Return a dictionary containing peaks.

    :param bw: A bigwig file
    :param dic_bed: A dictionary containing bed CDS
    :param processes: The number of processes to launch
    :param window: A window size
    :param min_coverage: the minimum required coverage in a gene to \
    be able to detect peaks
    :return: A dictionary of peaks
    """
    p = processes if processes <= mp.cpu_count() else mp.cpu_count()
    pool = mp.Pool(processes=1 if p < 0 else p)
    proc = []
    b = pbw.open(bw)
    genes = b.chroms()
    b.close()
    for gene in genes:
        args = [bw, dic_bed[gene], gene, window, min_coverage]
        proc.append(pool.apply_async(find_gene_peak, args))
    res = [proc[i].get(timeout=None) for i in range(len(proc))]
    nres = [r for r in res if r is not None]
    if not nres:
        return {}
    return reduce(combine_dic, nres)


def write_bed(peaks_dic: Dict, outfile: str) -> None:
    """
    Save dictionary of peaks into a bed file.

    :param peaks_dic: A dictionary of peaks
    :param outfile: The bedfile were the peaks will be stored
    """
    with open(outfile, "w") as outf:
        c = 0
        for gene in peaks_dic.keys():
            for start, stop in peaks_dic[gene]['peaks']:
                c += 1
                outf.write(f"{gene}\t{start}\t{stop}\tpeak_{c}\t"
                           f"{peaks_dic[gene]['score']}\t+\n")


@lp.parse
def peak_caller(bw: str, bed: str, outfile: str, process: int = 1,
                window: int = 9, coverage: int = 0):
    """
    Call the peaks in a ribosome profiling bigwig.

    :param bw: A bigwig file
    :param bed: An annotation bed file containing \
    containing CDS, and exons positions.
    :param outfile: File where the results will be created
    :param process: The number of processes to launch
    :param window: A window size (must be an odd number)
    :param coverage: the minimum required coverage in a gene to \
    be able to detect peaks
    """
    if window % 2 != 1:
        print(f"Warning: window size must be an odd number, setting it to "
              f"{window - (window % 3)}")
        window -= window % 3
    if window < 1:
        print("Warning: window size to small, setting it to 1")
        window = 1
    elif window > 999:
        print("Warning: window size to big, setting it to 999")
        window = 999
    dic_bed = get_bed_cds(Path(bed))
    dic_peaks = get_peaks_dict(bw, dic_bed, process, window, coverage)
    write_bed(dic_peaks, outfile)


if __name__ == "__main__":
    peak_caller()
