#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This script samples control peaks of the same size and number \
to see if a test of peaks in enriched in codons or amino acids
"""

import doctest
import pandas as pd
from peak_composition import codon_count, load_bed
from typing import Dict, List, Tuple, Any
import numpy as np
from pathlib import Path
import lazyparser as lp
import sys
from tqdm import tqdm
from Bio import SeqIO
import multiprocessing as mp
import seaborn as sns
import matplotlib.pyplot as plt
from statsmodels.stats.multitest import multipletests


DIC_SEQ = {"DDX5": "AATGGGCAAAGGA"}


def create_cds_list(cds_dic: Dict[str, List]) -> List[List]:
    """
    From a dictionary containing CDS create a list containing CDS.

    :param cds_dic: A dictionary containing CDS
    :return: The list of CDS

    >>> dc = {"DDX5": [["DDX5", 0, 30, "CDS_1", '0', "+"],
    ...                ["DDX5", 100, 145, "CDS_2", '0', "+"]],
    ... "DDX3": [["DDX3", 0, 30, "CDS_3", '1', "+"]],
    ... "FUS": [["FUS", 5, 14, "CDS_4", '2', "+"]],
    ... "FUS2": [["FUS2", 5, 13, "CDS_4", '2', "+"],
    ... ["FUS2", 5, 12, "CDS_4", '2', "+"]]}
    >>> create_cds_list(dc) == [['DDX5', 0, 30, 'CDS_1', 0, '+', 30],
    ... ['DDX5', 100, 145, 'CDS_2', 0, '+', 45],
    ... ['DDX3', 0, 30, 'CDS_3', 1, '+', 27],
    ... ['FUS', 5, 14, 'CDS_4', 2, '+', 6],
    ... ['FUS2', 5, 13, 'CDS_4', 2, '+', 6],
    ... ['FUS2', 5, 12, 'CDS_4', 2, '+', 3]]
    True
    """
    list_cds = []
    for gene, lcds in cds_dic.items():
        for cds in lcds:
            if cds[4].isdigit():
                cds[4] = int(cds[4])
                size = int((cds[2] - cds[1] - cds[4]) / 3) * 3
                if size > 2:
                    cds.append(size)
                    list_cds.append(cds)
    return list_cds


def randomly_select_cds(list_cds: List[List], min_size: int) -> List:
    """
    Select a CDS with a size at least equal to min_size.

    :param list_cds: A list of cds
    :param min_size: The minimum size of interest
    :return: The selected cds

    >>> lc = [['DDX5', 0, 30, 'CDS_1', 0, '+', 30],
    ... ['DDX5', 100, 145, 'CDS_2', 0, '+', 45],
    ... ['DDX3', 0, 30, 'CDS_3', 1, '+', 27],
    ... ['FUS', 5, 14, 'CDS_4', 2, '+', 6],
    ... ['FUS2', 5, 13, 'CDS_5', 2, '+', 6],
    ... ['FUS2', 5, 12, 'CDS_6', 2, '+', 3]]
    >>> res = [randomly_select_cds(lc, 4)[3] for _ in range(20)]
    >>> "CDS_6" not in res
    True
    >>> res = [randomly_select_cds(lc, 31)[3] for _ in range(20)]
    >>> res == ["CDS_2"] * 20
    True
    """
    selected = np.random.randint(0, len(list_cds))
    cds = list_cds[selected]
    while cds[-1] < min_size:
        selected = np.random.randint(0, len(list_cds))
        cds = list_cds[selected]
    return cds



def randomly_sample_list_cds(list_cds: List[List], min_size: List[int]
                             ) -> List[List]:
    """
    Select a list of CDS

    :param list_cds: The list of CDS of interest
    :param min_size: The list of minimum size for the CDS
    :return: The list of selected CDS

    >>> lc = [['DDX5', 0, 30, 'CDS_1', 0, '+', 30],
    ... ['DDX5', 100, 145, 'CDS_2', 0, '+', 45],
    ... ['DDX3', 0, 30, 'CDS_3', 1, '+', 27],
    ... ['FUS', 5, 14, 'CDS_4', 2, '+', 6],
    ... ['FUS2', 5, 13, 'CDS_5', 2, '+', 6],
    ... ['FUS2', 5, 12, 'CDS_6', 2, '+', 3]]
    >>> r = randomly_sample_list_cds(lc, [20, 25, 30, 45])
    >>> len(r) == 4
    True
    >>> n = [v[3] for v in r]
    >>> "CDS_1" in n or "CDS_2" in n or "CDS_3" in n
    True
    >>> "CDS_6" not in r
    True
    """
    return [randomly_select_cds(list_cds, ms) for ms in min_size]


def create_peak_in_selected_cds(selected_cds: List, size: int) -> List:
    """

    :param selected_cds: A selected CDS
    :param size: The peak size in codons
    :return: The pic inside this CDS

    >>> cs = ["GN1", 3, 10, "CDS-5", 0, "+", 2]
    >>> res = create_peak_in_selected_cds(cs, 1)
    >>> res in [['GN1', 3, 6], ['GN1', 6, 9]]
    True
    >>> r = create_peak_in_selected_cds(["GN1", 3, 10, "CDS-5", 1, "+", 2], 1)
    >>> r in [['GN1', 4, 7], ['GN1', 7, 10]]
    True
    >>> r = create_peak_in_selected_cds(["GN1", 3, 13, "CDS-5", 1, "+", 2], 2)
    >>> r in [['GN1', 4, 10], ['GN1', 7, 13]]
    True
    """
    size *= 3
    min_value = selected_cds[1] + selected_cds[4]
    max_value = selected_cds[2] - (selected_cds[2] - min_value) % 3 - size
    start = np.random.choice(np.arange(min_value, max_value + 1, 3), 1)[0]
    stop = start + size
    if stop > selected_cds[2]:
        raise ValueError("The stop of the peaks can't be greater than the "
                         "stop of the cds : \n "
                         f"peaks: {[selected_cds[0], start, stop]}\n"
                         f"cds: {selected_cds}")
    if (stop - start) % 3 != 0:
        raise ValueError(f"The length of each peaks should be divisble by "
                         f"3 : {[selected_cds[0], start, stop]}")
    return [selected_cds[0], start, stop]


def create_peaks_from_cds(list_cds: List[List], list_size: List[int]
                          ) -> List[List]:
    """
    Get a list of peaks inside the selected CDS.

    :param list_cds: A list of CDS
    :param list_size: The list of peak size in codons
    :return: The list of selected peaks

    >>> lc = [['DDX5', 0, 30, 'CDS_1', 0, '+', 30],
    ... ['DDX5', 100, 145, 'CDS_2', 0, '+', 45],
    ... ['DDX3', 0, 30, 'CDS_3', 1, '+', 27],
    ... ['FUS', 5, 14, 'CDS_4', 2, '+', 6],
    ... ['FUS2', 5, 13, 'CDS_5', 2, '+', 6],
    ... ['FUS2', 5, 12, 'CDS_6', 2, '+', 3]]
    >>> len(create_peaks_from_cds(lc, [5, 6, 3, 2, 2, 1]))
    6
    """
    if len(list_cds) != len(list_size):
        raise IndexError("list_cds and list_size should have the same length")
    peaks = []
    for i in range(len(list_size)):
        tmp = create_peak_in_selected_cds(list_cds[i], list_size[i])
        tmp.append(f"peak_{i + 1}")
        tmp.append("+")
        peaks.append(tmp)
    return peaks


def create_codon_table(list_peaks: List[List], size: str
                       ) -> pd.DataFrame:
    """
    Create the table indicating the codons contained inside each peaks.

    :param list_peaks: The list of peaks
    :param size: The size of footprint in test dataset
    :return: A table indicating the codons/aa inside each peaks

    >>> lpe = [['DDX5', 1, 7, 'peak_1', '+'], ['DDX5', 7, 10, 'peak_2', '+']]
    >>> d = create_codon_table(lpe, "10-50")
    >>> d.loc[d["count"] != 0, ["count", "ft", "ft_type", "peak", "gene",
    ... "sample", "codon_in_peak"]]
         count   ft ft_type    peak  gene sample  codon_in_peak
    6        1  ATG   codon  peak_1  DDX5   CTRL              2
    40       1  GGC   codon  peak_1  DDX5   CTRL              2
    71       1    G      aa  peak_1  DDX5   CTRL              2
    76       1    M      aa  peak_1  DDX5   CTRL              2
    81       1  AAA   codon  peak_2  DDX5   CTRL              1
    144      1    K      aa  peak_2  DDX5   CTRL              1
    """
    list_df = [
        codon_count(my_peak, DIC_SEQ, "CTRL", "R-CTRL", size)
        for my_peak in list_peaks
    ]
    return pd.concat(list_df, ignore_index=True, axis=0)


def get_mean_codon_control_set(df: pd.DataFrame) -> pd.DataFrame:
    """
    Create a dataframe showing the mean content of codon, amino acids in \
    peaks.

    :param df: A table indicating the codons/aa inside each peaks
    :return: A table indicating the mean codon/aa in peaks

    >>> lpe = [['DDX5', 1, 7, 'peak_1', '+'], ['DDX5', 7, 10, 'peak_2', '+']]
    >>> d = create_codon_table(lpe, "10-50")
    >>> r = get_mean_codon_control_set(d)
    >>> r.loc[:, ["ATG", "GGC", "AAA", "G", "M", "K"]]
    ft      ATG   GGC  AAA     G     M    K
    count  0.25  0.25  0.5  0.25  0.25  0.5
    """
    df["count"] = df["count"] / df["codon_in_peak"]
    return df.pivot_table(values="count", columns="ft", aggfunc="mean")


def load_test_peak(df: pd.DataFrame) -> List:
    """
    From a dataframe containing peaks, recover the list of size wanted for \
    the control peaks.

    :param df: A dataframe containing peaks and its content in aa and codons
    :return: The list of peak size in codons

    >>> d = pd.DataFrame(
    ... {'count': [1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    ... 'ft': ['AAT', 'N'] * 10,
    ... 'ft_type': ['codon', 'aa'] * 10,
    ... 'peak': [f'peak_{i + 1}'
    ... for i in sorted(list(range(10)) + list(range(10)))],
    ... 'gene': ['AAGAB', 'AAGAB', 'AARS2', 'AARS2', 'AASDHPPT', 'AASDHPPT',
    ... 'ABCC4', 'ABCC4', 'ABCD3', 'ABCD3', 'ABCD3', 'ABCD3', 'ABCE1',
    ... 'ABCE1', 'ABCE1', 'ABCE1', 'ABCE1', 'ABCE1', 'ABHD2', 'ABHD2'],
    ... 'sample': ['BRAF_vs_DMSO'] * 20,
    ... 'replicate': ['R1'] * 20,
    ... 'size': ['18-50'] * 20,
    ... 'codon_in_peak': [7, 7, 3, 3, 11, 11, 4, 4, 5 , 5 , 5 , 5 , 3 , 3 ,
    ... 9 , 9 , 4 , 4 , 8 , 8]})
    >>> load_test_peak(d)
    [7, 3, 11, 4, 5, 5, 3, 9, 4, 8]
    """
    if len(df["size"].unique()) != 1 or len(df["replicate"].unique()) != 1 \
            or len(df["sample"].unique()) != 1:
        raise ValueError(f"The columns size, replicate and sample of the "
                         f"count table should be one !")
    r = df.drop_duplicates(subset=["peak", "codon_in_peak"])
    return list(r["codon_in_peak"].values)


def build_control_composition(list_size: List[int], cds_list: List[List],
                              fp_size: str):
    """

    :param list_size: The list of codon in peaks in the test condition
    :param cds_list: The list of cds in the genome
    :param fp_size: The size of footprints in the test condition
    :return: The mean composition of codon amino acid in control peaks.

    >>> ls = [2]
    >>> cdl = [['DDX5', 0, 7, 'CDS_1', 1, '+', 6],
    ... ['DDX5', 5, 10, 'CDS_2', 2, '+', 3]]
    >>> r = build_control_composition(ls, cdl, "50-60")
    >>> r.loc[:, ["ATG", "GGC", "AAA", "GGA", "M", "G", "K"]]
    ft     ATG  GGC  AAA  GGA    M    G    K
    count  0.5  0.5  0.0  0.0  0.5  0.5  0.0
    >>> r = build_control_composition([2, 1], cdl, "50-60")
    >>> r.loc["count", "ATG"] in [0.25, 0.75]
    True
    >>> r.loc["count", "GGC"] in [0.25, 0.75]
    True
    >>> r.loc["count", "AAA"] in [0.0, 0.5]
    True
    >>> r.loc["count", "GGA"] == 0
    True
    """
    list_size_nt = [s * 3 for s in list_size]
    selected_cds = randomly_sample_list_cds(cds_list, list_size_nt)
    selected_peaks = create_peaks_from_cds(selected_cds, list_size)
    df_codon = create_codon_table(selected_peaks, fp_size)
    return get_mean_codon_control_set(df_codon)


def create_many_control(ps: int, list_size: List[int], cds_list: List[List],
                        fp_size: str, iteration: int):
    """

    :param ps: the number of process to create
    :param list_size: The list of codon in peaks in the test condition
    :param cds_list: The list of cds in the genome
    :param fp_size: The size of footprints in the test condition
    :param iteration: The number of control to make
    :return: The mean composition of codon amino acid in control peaks.
    """
    # list_df = [
    #     build_control_composition(list_size, cds_list, dic_seq, fp_size)
    #     for _ in range(iteration)
    # ]
    ps = max(min(mp.cpu_count(), ps), 1)
    pool = mp.Pool(processes=ps)
    processes = []
    for _ in range(iteration):
        args = [list_size, cds_list, fp_size]
        processes.append(pool.apply_async(build_control_composition, args))
    results = []
    for proc in tqdm(processes):
        results.append(proc.get(timeout=None))
    return pd.concat(results, axis=0, ignore_index=True)


def get_pvalue(values: np.array, target: float, iteration: int,
               alpha: float = 0.05, alternative: str = "two_sided"
               ) -> Tuple[float, str]:
    """
    Return The p-value and the regulation
    :param values:  The list of control values
    :param target: The test value
    :param iteration: The iteration of interest
    :param alpha: Type 1 error threshold
    :param alternative: two_sided, greater or less
    :return: The p-value and the regulation
    >>> get_pvalue(np.arange(10), 1.75, 10)
    (0.2, ' . ')
    >>> get_pvalue(np.arange(10), -1, 10)
    (0.0, ' . ')
    >>> get_pvalue(np.arange(10), 9.75, 10)
    (0.0, ' . ')
    >>> get_pvalue(np.arange(10), 8.8, 10)
    (0.1, ' . ')
    >>> get_pvalue(np.arange(100), 98.8, 100)
    (0.01, ' + ')
    >>> get_pvalue(np.arange(100), 100, 100)
    (0.0, ' + ')
    >>> get_pvalue(np.arange(100), -1, 100)
    (0.0, ' - ')
    >>> get_pvalue(np.arange(0, 0.10, 0.01), 0.05, 10)
    (0.5, ' . ')
    >>> get_pvalue(np.arange(100), 98.8, 100, 0.05, "two_sided")
    (0.01, ' + ')
    >>> get_pvalue(np.arange(100), 98.8, 100, 0.05, "greater")
    (0.01, ' + ')
    >>> get_pvalue(np.arange(100), 98.8, 100, 0.05, "less")
    (0.99, ' . ')
    >>> get_pvalue(np.arange(1, 101), 1, 100, 0.05)
    (0.01, ' - ')
    >>> get_pvalue(np.arange(1, 101), 1, 100, 0.05, 'two_sided')
    (0.01, ' - ')
    >>> get_pvalue(np.arange(1, 101), 1, 100, 0.05, 'less')
    (0.01, ' - ')
    >>> get_pvalue(np.arange(1, 101), 1, 100, 0.05, 'greater')
    (1.0, ' . ')
    """
    values = np.sort(values)
    val_len = len(values)
    if val_len != iteration:
        msg = f"Error the length of values vector {len(values)} " \
              f"is different than iteration {iteration}"
        raise ValueError(msg)
    idx = values[values <= target].size
    impov = idx / val_len
    idx = values[values >= target].size
    enrich = idx / val_len
    if alternative == "less":
        enrich = 1
    if alternative == "greater":
        impov = 1
    regulation = " . "
    if impov < enrich:
        pval = impov
        if pval <= alpha:
            regulation = " - "
    else:
        pval = enrich
        if pval <= alpha:
            regulation = " + "
    if iteration < 20:
        regulation = " . "
    return pval, regulation


def compute_row(test_value: float, ctrl_values: np.array, ft: str,
                condition: str, replicate: str, size_fp: str, iteration: int
                ) -> pd.Series:
    """
    Compute a row of the final table showing the enrichment of a feature \
    in find in ribosome profiling peaks of a test condition compared \
    to control peaks defined randomly.

    :param test_value: The mean percentage of  a codon/amino acid find \
    in test peaks
    :param ctrl_values: The mean percentage of  a codon/amino acid find \
    in control peaks
    :param ft: The feature (codon or aa) of interest (e.g AAC, W)
    :param condition: The name of the condition of interest
    :param replicate: The name of the replicate
    :param size_fp: The size of the footprints selected to defined \
    peaks in test conditions
    :param iteration: The number of control set created
    :return: The row of the final table

    >>> compute_row(99, np.arange(0, 100), "A", "test_cond", "R1", "10-50",
    ... 100).to_dict() == {
    ... 'ft': 'A',
    ... 'ft_type': 'aa',
    ... 'size_test_fp': '10-50',
    ... 'replicate': 'R1',
    ... 'test_condition': 'test_cond',
    ... 'control_condition': 'RANDOM_CTRL',
    ... 'mean_test': 99,
    ... 'mean_100_control': 49.5,
    ... 'pvalue': 0.01,
    ... 'regulation': ' + '}
    True
    """
    ft_type = "codon" if len(ft) == 3 else "aa"
    pval, reg = get_pvalue(ctrl_values, test_value, iteration)
    return pd.Series({
        "ft": ft,
        "ft_type": ft_type,
        "size_test_fp": size_fp,
        "replicate": replicate,
        "test_condition": condition,
        "control_condition": "RANDOM_CTRL",
        "mean_test": test_value,
        f"mean_{iteration}_control": np.mean(ctrl_values),
        "pvalue": pval,
        "regulation": reg
    })


def compute_table(df_test: pd.DataFrame, df_control: pd.DataFrame,
                  condition: str, replicate: str, size_fp: str, iteration: int
                  ) -> pd.DataFrame:
    """
    Compute a row of the final table showing the enrichment of a feature \
    in find in ribosome profiling peaks of a test condition compared \
    to control peaks defined randomly.

    :param df_test: The dataframe indicating the count of each codon/aa \
    in peaks
    :param df_control: The dataframe indicating the mean proportion \
    of codon and amino acids in each set of control of peaks create \
    an `iteration`number of time
    :param ft: The feature (codon or aa) of interest (e.g AAC, W)
    :param condition: The name of the condition of interest
    :param replicate: The name of the replicate
    :param size_fp: The size of the footprints selected to defined \
    peaks in test conditions
    :param iteration: The number of control set created
    :return: The row of the final table)
    """
    test_series = get_mean_codon_control_set(df_test).loc["count", :]
    ft_list = list(test_series.index)
    list_row = [
        compute_row(test_series[ft], df_control[ft].values, ft, condition,
                    replicate, size_fp, iteration) for ft in ft_list
    ]
    return pd.DataFrame(list_row)


def load_genome(fasta_file: str) -> Dict:
    print("load genome")
    return {record.id: record.seq
            for record in SeqIO.parse(fasta_file, "fasta")}


def get_pval_color(mseries: pd.Series) -> str:
    """

    :param mseries: A row of the relative frequency dataframe
    :return: the color of interest

    >>> ds = pd.Series({"regulation": " - ", "p-adj": "<0.01"})
    >>> get_pval_color(ds)
    'blue'
    >>> ds = pd.Series({"regulation": " + ", "p-adj": "<0.01"})
    >>> get_pval_color(ds)
    'red'
    >>> ds = pd.Series({"regulation": " - ", "p-adj": "0.1"})
    >>> get_pval_color(ds)
    'black'
    """
    pvalue = float(mseries["p-adj"].replace("<", ""))
    if pvalue > 0.05:
        return "black"
    if mseries["regulation"] == " + ":
        return "red"
    return "blue"


def add_stat(df_freq: pd.DataFrame, g: sns.FacetGrid, ) -> sns.FacetGrid:
    """
    Add corrected p-values to the plot.

    :param df_freq: Dataframe containing relative frequencies
    :param g: A plot
    :return: The plot updated
    """
    ft_dic = {f: i for i, f in enumerate(df_freq['ft'].unique())}
    df_freq["abscissa"] = df_freq["ft"].map(ft_dic)
    df_freq["color"] = df_freq.apply(get_pval_color, axis=1)
    ylim = g.ax.get_ylim()
    h = (ylim[1] - ylim[0]) * .02
    for i in range(df_freq.shape[0]):
        row = df_freq.iloc[i, :]
        g.ax.annotate(row["p-adj"],
                      xy=(row["abscissa"], row["relative_frequency"] + h),
                      xytext=(0, 1),
                      textcoords='offset points',
                      xycoords='data', ha='center', va='bottom',
                      fontsize='xx-small', clip_on=False,
                      annotation_clip=False,
                      color=row["color"])
    return g


def create_rel_freq_figure(df_freq: pd.DataFrame, outfig: Path,
                           ft_type: str, palette: Dict[str, Any]) -> None:
    """
    Create violin figures.

    :param df_freq: The dataframe of frequencies
    :param outfig: The name of the output figure file
    :param ft_type: The type of component of interest
    :param feature: The kind of feature of interest
    :param df_stat: A dataframe containing statistical test results
    :param test: The kind of test performed
    :param palette: A dictionary indicating the color for each dataset
    :param control_name: The name of list of feature used as control
    """
    sns.set(context="talk")
    g = sns.catplot(x="ft", y="relative_frequency", hue="test_condition",
                    data=df_freq, aspect=2.2,
                    height=12, kind="point", palette=palette, join=False,
                    dodge=True)
    g.ax.set_ylabel(f"Relative frequency of {ft_type} "
                    f"(against random peaks)")
    g.ax.set_title(f"Relative mean frequency against random peaks "
                   f"of all {ft_type}")
    g = add_stat(df_freq, g)
    plt.axhline(y=0, color="red")
    g.savefig(outfig)
    plt.cla()
    plt.clf()
    plt.close()


def get_color_dict(list_name: List[str], colors: List[str]) -> Dict[str, Any]:
    """

    :param list_name: The name of the list of exons/genes located in the \
    files given with --list_files parameter (in the same order)
    :param colors: The list of selected colors
    :return: A dictionary linking each dataset name to a color

    >>> get_color_dict(["lol", "bouh", "foo"], []) == {
    ... 'lol': (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
    ... 'bouh': (1.0, 0.4980392156862745, 0.054901960784313725),
    ... 'foo': (0.17254901960784313, 0.6274509803921569, 0.17254901960784313)}
    True
    >>> get_color_dict(["lol", "bouh", "foo"], ["black", "red", "blue"]) == {
    ... 'lol': 'black', 'bouh': 'red', 'foo': 'blue'}
    True
    """
    if not colors:
        return {n: c for n, c in zip(list_name, sns.color_palette())}
    elif len(colors) == len(list_name):
        return {n: c for n, c in zip(list_name, colors)}
    else:
        raise IndexError("The length of --colors parameter should be the same "
                         "as the parameter --list_name")


def make_figures(df_final: pd.DataFrame, samples: List[str],
                 colors: List[str], outfile: str) -> None:
    """
    Create the plot containing the relative frequency between peaks \
    of different condition and their control corresponding to randomly \
    sampled peaks

    :param df_final: A dataframe containing the relative frequencies \
     of codons or aa between peaks and control peaks (randomly sampled).
    :param samples: The name of the control list of interest
    :param colors: The color to give to those lists
    :param outfile: The name of the output file
    """
    palette = get_color_dict(samples, colors)
    for level in df_final["ft_type"].unique():
        ft_df = df_final[df_final["ft_type"] == level].copy()
        create_rel_freq_figure(ft_df,
                               Path(outfile.replace(".txt", f"_{level}.pdf")),
                               "aa", palette)


def adjust_null_pvalue(df_stat: pd.DataFrame, iteration: int
                           ) -> pd.DataFrame:
    """
    For p-value that have a zero value, correct it by the 1 divided with \
    the number of iterations.

    :param df_stat: A dataframe containing relative frequency of codon and \
    encoded amino acids in peak compared to control peaks
    :param iteration: The number of control lists of peaks created
    :return: A new column: pval

    >>> ds = pd.DataFrame({"pvalue": [0.0, 0.1, 0.2]})
    >>> adjust_null_pvalue(ds, 100)["pval"].to_list()
    [0.01, 0.1, 0.2]
    """
    df_stat["pval"] = np.round(
        [pval if pval != 0 else 1 / iteration for pval in
         df_stat["pvalue"].to_list()], 5)
    return df_stat


def pvalue_correction(df_stat: pd.DataFrame) -> pd.DataFrame:
    """
    Correct the p-values in a dataframe.

    :param df_stat: A dataframe containing relative frequency of codon and \
    encoded amino acids in peak compared to control peaks
    :return: The dataframe with the corrected p-values

    >>> ds = pd.DataFrame({"ft_type": ["aa"] * 5 + ["codon"] * 5,
    ... "pval": [0.0001, 0.0008, 0.001, 0.007, 0.01, 0.02, 0.03, 0.04, 0.05,
    ... 0.8]})
    >>> pvalue_correction(ds)
      ft_type    pval    p-adj
    0      aa  0.0001  0.00050
    1      aa  0.0008  0.00167
    2      aa  0.0010  0.00167
    3      aa  0.0070  0.00875
    4      aa  0.0100  0.01000
    5   codon  0.0200  0.06250
    6   codon  0.0300  0.06250
    7   codon  0.0400  0.06250
    8   codon  0.0500  0.06250
    9   codon  0.8000  0.80000
    """
    list_df = []
    for level in df_stat["ft_type"].unique():
        tmp = df_stat[df_stat["ft_type"] == level].copy()
        tmp["p-adj"] = np.round(multipletests(tmp["pval"].values,
                                method="fdr_bh")[1], 5)
        list_df.append(tmp)
    df_stat = pd.concat(list_df, axis=0, ignore_index=True)
    return df_stat


def add_inferior_sign(df_stat: pd.DataFrame) -> pd.DataFrame:
    """
    Add inferior sign before p-values that were previously equals to zero.

    :param df_stat: A dataframe containing relative frequency of codon and \
    encoded amino acids in peak compared to control peaks
    :return: The dataframe with p-values having < signs if they were equal \
    to zero in the first place.

    >>> ds = pd.DataFrame({"pvalue": [0.0, 0.1, 0.2, 0.0, 0.5] * 2,
    ... "ft_type": ["codon"] * 5 + ["aa"] * 5})
    >>> ds = adjust_null_pvalue(ds, 1000)
    >>> ds = pvalue_correction(ds)
    >>> add_inferior_sign(ds)
      ft_type    pval    p-adj
    0   codon  <0.001  <0.0025
    1   codon     0.1  0.16667
    2   codon     0.2     0.25
    3   codon  <0.001  <0.0025
    4   codon     0.5      0.5
    5      aa  <0.001  <0.0025
    6      aa     0.1  0.16667
    7      aa     0.2     0.25
    8      aa  <0.001  <0.0025
    9      aa     0.5      0.5
    """
    df_stat["pval"] = df_stat["pval"].astype(str)
    df_stat["p-adj"] = df_stat["p-adj"].astype(str)
    df_stat.loc[df_stat["pvalue"] == 0, "p-adj"] = "<" + df_stat["p-adj"]
    df_stat.loc[df_stat["pvalue"] == 0, "pval"] = "<" + df_stat["pval"]
    df_stat.drop("pvalue", axis=1, inplace=True)
    return df_stat


def compute_relative_freq(df_stat: pd.DataFrame, iteration: int
                          ) -> pd.DataFrame:
    """
    Compute the relative frequency of codons, amino acids in peaks.

    :param df_stat: A dataframe containing relative frequency of codon and \
    encoded amino acids in peak compared to control peaks
    :param iteration: The number of control lists of peaks created
    :return: The dataframe with the column relative frequency

    >>> ds = pd.DataFrame({"mean_test": [0.1, 0.25, 0.5, 1],
    ... "mean_100_control": [0.9, 0.25, 0.4, 0.9]})
    >>> compute_relative_freq(ds, 100)
       mean_test  mean_100_control  relative_frequency
    0       0.10              0.90          -88.888889
    1       0.25              0.25            0.000000
    2       0.50              0.40           25.000000
    3       1.00              0.90           11.111111
    """
    df_stat["relative_frequency"] = \
        (df_stat["mean_test"] - df_stat[f"mean_{iteration}_control"]) / \
        df_stat[f"mean_{iteration}_control"] * 100
    return df_stat


@lp.parse(peak_count_files="file", annotation="file", genome="file",
          min_fp_size=range(101), max_fp_size="max_fp_size > 0",
          threshold=range(100), resize=range(20), levels=["codon", "aa"])
def main_function(peak_count_file: str, annotation: str,
                  genome: str, colors: List[str],
                  iteration: int = 10000, ps: int = 1,
                  outfile: str = "./permutation_stat_table.txt") -> None:
    """
    Count the codon located in peaks.

    :param peak_count_file: The list files containing the codon counts in \
    peaks of ribosome profiling coverage obtained in a test conditions
    :param annotation: A bed file containing CDS and exons for every gene.
    :param genome: A genome fasta file containing gene sequences
    :param colors: The list of color of interest
    :param replicate: The name of the replicate of the sample from which \
    the replicate came from.
    :param min_fp_size: Minimum footprint size considered (default 20)
    :param max_fp_size: Maximum footprint size considered (default 40)
    :param iteration: The number of control sets of peaks to make \
    (default 10000)
    :param ps: The number of process to create
    :param outfile: The file containing the codon/aa enrichment p
    """
    df_count_full = pd.read_csv(peak_count_file, sep="\t")
    samples = list(df_count_full["sample"].unique())
    if len(samples) != len(colors):
        raise IndexError(f"peak_count_files, samples and colors should have "
                         f"the same length")
    global DIC_SEQ
    DIC_SEQ = load_genome(genome)
    if len(DIC_SEQ) == 1:
        raise ValueError("The lenght of the dictionary should not be 1")
    list_df = []
    for sample in samples:
        df_count = df_count_full[df_count_full["sample"] == sample].copy()
        cds_dic = load_bed(Path(annotation), "CDS-")
        cds_list = create_cds_list(cds_dic)
        list_peak_size = load_test_peak(df_count)
        size = df_count["size"].unique()[0]
        replicate = df_count["replicate"].unique()[0]
        df_ctrl = create_many_control(ps, list_peak_size, cds_list, size,
                                      iteration)
        df_stat = compute_table(df_count, df_ctrl, sample, replicate,
                                size, iteration)
        list_df.append(df_stat)
    final_tmp = pd.concat(list_df, axis=0, ignore_index=True)
    final_tmp = adjust_null_pvalue(final_tmp, iteration)
    df_final = pvalue_correction(final_tmp)
    df_final = add_inferior_sign(df_final)
    df_final = compute_relative_freq(df_final, iteration)
    df_final.to_csv(outfile, sep="\t", index=False)
    make_figures(df_final, samples, colors, outfile)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        doctest.testmod()
    else:
        main_function()
