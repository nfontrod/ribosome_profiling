#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to find codons located inside \
peaks in ribo-seq experiment.
"""

from typing import Dict, List, Optional
from pathlib import Path
import doctest
import pandas as pd
from pyfaidx import Fasta
import lazyparser as lp
from Bio.Seq import Seq


def is_good_ft(str_in_line: Optional[str], name: str) -> bool:
    """
    True if a line must be reported, False else.

    :param str_in_line: If set, the function will return True if \
    the name contains the substring defined in this variable.
    :param name: A bed feature name
    :return: if str_in_line is not None \
    True if name contains str_in_line, False else. if str_in_line is None \
    always return True

    >>> is_good_ft(None, "exon_1")
    True
    >>> is_good_ft("CDS", "exon_1")
    False
    >>> is_good_ft("CDS", "CDS_1")
    True
    """
    if str_in_line is None:
        return True
    elif str_in_line in name:
        return True
    return False


def has_good_score(threshold: float, score: str) -> bool:
    """
    True is the score of a bed line is above threshold, false, else.

    :param threshold: The threshold, if it is equal to zero, it's not applied.
    :param score: A score
    :return: always True is Threshold = 0 else score must be higher than \
    threshold.

    >>> has_good_score(0, ".")
    True
    >>> has_good_score(3, "0")
    False
    >>> has_good_score(3, "10")
    True
    >>> has_good_score(3, ".")
    Traceback (most recent call last):
    ...
    ValueError: could not convert string to float: '.'
    """
    if threshold == 0:
        return True
    sc = float(score)
    return sc >= threshold


def load_bed(bed: Path, str_in_name: Optional[str] = None,
             threshold: float = 0) -> Dict[str, List]:
    """
    Load a bed file into a dictionary

    :param bed: A bed file
    :param str_in_name: A string that must be located inside the \
    names column.
    :param threshold: The alignement must have at least \
    threshold score to be reportted. If threshold is equal to 0,
    all feature are reported.
    :return: A dictionary containing, for each gene (key) \
    the list of every element (CDS/peaks) in it.
    """
    d = {}
    with bed.open("r") as bedf:
        for line in bedf:
            lline = line.replace("\n", "").split("\t")
            if is_good_ft(str_in_name, lline[3]) and \
                    has_good_score(threshold, lline[4]) and \
                    lline[4] != "multiple" and "stop-CDS" not in lline[4]:
                nrow = [lline[0], int(lline[1]), int(lline[2]), lline[3],
                        lline[4], lline[5]]
                if lline[0] not in d:
                    d[lline[0]] = [nrow]
                else:
                    d[lline[0]].append(nrow)
    return d


def is_overlapping(cds: List, peak: List) -> bool:
    """
    Return True if the two feature are overlapping, False, else.

    :param cds: The bed line corresponding to a CDS
    :param peak: A peak
    :return: Return True if the two feature are overlapping, False, else.

    >>> is_overlapping(["DDX5", 0, 100, "CDS", ".", "+"],
    ...                ["DDX5", 85, 120, "peak_1", 3.0, "+"])
    True
    >>> is_overlapping(["DDX5", 0, 100, "CDS", ".", "+"],
    ...                ["DDX5", 85, 120, "peak_1", 3.0, "-"])
    False
    >>> is_overlapping(["DDX5", 0, 100, "CDS", ".", "+"],
    ...                ["DDX5", 85, 90, "peak_1", 3.0, "+"])
    True
    >>> is_overlapping(["DDX5", 10, 100, "CDS", ".", "+"],
    ...                ["DDX5", 0, 90, "peak_1", 3.0, "+"])
    True
    """
    return cds[0] == peak[0] and \
        (cds[1] <= peak[1] <= cds[2] or cds[1] <= peak[2] <= cds[2]) and \
        (cds[5] == peak[5])


def get_overlapping(peak: List, cds: List[List]) -> Optional[List]:
    """
    Get cds overlapping a peak.

    :param peak: A peak
    :param cds: A cds
    :return: The cds overlapping the peak.

    >>> p = ["DDX5", 10, 20, "peak1", 3, "+"]
    >>> cdst = [["DDX5", 0, 30, "CDS_1", ".", "+"],
    ...         ["DDX5", 0, 30, "CDS_2", ".", "-"],
    ...         ["DDX3", 0, 30, "CDS_3", ".", "+"],
    ...         ["FUS", 45, 100, "CDS_4", ".", "+"]]
    >>> get_overlapping(p, cdst)
    ['DDX5', 0, 30, 'CDS_1', '.', '+']
    >>> cdst = [["DDX17", 0, 30, "CDS_1", ".", "+"],
    ...         ["DDX5", 0, 30, "CDS_2", ".", "-"],
    ...         ["DDX3", 0, 30, "CDS_3", ".", "+"],
    ...         ["FUS", 45, 100, "CDS_4", ".", "+"]]
    >>> get_overlapping(p, cdst)
    >>> cdst = [["DDX5", 0, 30, "CDS_1", ".", "+"],
    ...         ["DDX5", 0, 45, "CDS_2", ".", "+"],
    ...         ["DDX3", 0, 30, "CDS_3", ".", "+"],
    ...         ["FUS", 45, 100, "CDS_4", ".", "+"]]
    >>> get_overlapping(p, cdst)
    Warning peak1 overlap multiple cds : ['CDS_1', 'CDS_2']
    """
    overlap = [c for c in cds if is_overlapping(c, peak)]
    if len(overlap) > 1:
        cds_name = [c[3] for c in overlap]
        print(f"Warning {peak[3]} overlap multiple cds : {cds_name}")
        return None
    elif len(overlap) == 0:
        return None
    return overlap[0]


def adjust_peak(peak: List, cds: List, resize: int = 0) -> List:
    """
    adjust peak coordinate to find codon inside it.

    :param peak: a peak feature
    :param cds: an overlapping cds
    :param resize: The size value
    :return: The peak resized

    >>> adjust_peak(["DDX5", 10, 20, "peak1", 3, "+"],
    ...             ["DDX5", 11, 20, "CDS", 0, "+"], 2)
    ['DDX5', 11, 20, 'peak1', 3, '+']
    >>> adjust_peak(["DDX5", 12, 20, "peak1", 3, "+"],
    ...             ["DDX5", 10, 20, "CDS", 2, "+"], 2)
    ['DDX5', 12, 18, 'peak1', 3, '+']
    >>> adjust_peak(["DDX5", 20, 32, "peak1", 3, "+"],
    ...             ["DDX5", 10, 100, "CDS", 1, "+"], 2)
    ['DDX5', 20, 32, 'peak1', 3, '+']
    >>> adjust_peak(["DDX5", 20, 32, "peak1", 3, "+"],
    ...             ["DDX5", 10, 100, "CDS", 1, "+"], 4)
    ['DDX5', 17, 35, 'peak1', 3, '+']
    >>> adjust_peak(["DDX5", 20, 32, "peak1", 3, "+"],
    ...             ["DDX5", 10, 100, "CDS", 2, "+"], 2)
    ['DDX5', 18, 33, 'peak1', 3, '+']
    """
    if peak[5] != "+" or cds[5] != "+":
        raise ValueError(f"The strand of CDS and peak should always be '+' !")
    peak[1] -= resize
    peak[2] += resize
    if peak[1] < cds[1]:
        peak[1] = cds[1]
    if peak[2] > cds[2]:
        peak[2] = cds[2]
    offset = int(cds[4])
    if peak[1] > cds[1]:
        offset = int((peak[1] - (cds[1] + offset)) % 3)
        if offset == 2:
            offset = 1
        elif offset == 1:
            offset = 2
    peak[1] += offset
    peak[2] -= (peak[2] - peak[1]) % 3
    return peak


def get_codon_dic() -> Dict:
    """
    :return: A dictionary containing every coding codons (key) \
    associated with 0 as value
    """
    ntl = list("ATGC")
    d = {a + b + c: 0 for a in ntl for b in ntl for c in ntl}
    del(d["TGA"])
    del(d["TAG"])
    del(d["TAA"])
    return d


def get_aa_dic() -> Dict:
    """
    :return: A dictionary containing every aa (key) \
    associated with 0 as value
    """
    return {a: 0 for a in list('RHKDESTNQCGPAILMFWYV')}


def codon_count(adjusted_peak: List, dic_seq: Fasta,
                sample: str, replicate: str, size: str
                ) -> Optional[pd.DataFrame]:
    """
    Count the codons localized inside adjusted peak.

    :param adjusted_peak: A peak in frame
    :param dic_seq: A dictionary of sequence
    :param sample: A sample name
    :param replicate: A replicate name
    :param size: The min footprint lenght (min_fpl) followed by the max \
    footprint length (max_fpl) in the following string form : 'min_fpl-max_fpl'
    :return: A table containing the codon counts inside the peaks

    >>> ds = {"DDX5": "AAGAAGAAGAAAAAAGAAGAAGAAGAAGAAG"}
    >>> adj_peak = ["DDX5", 3, 12, "peak1", 3, "+"]
    >>> dtmp = codon_count(adj_peak, ds, "CTRL_test", "R1", "20-35")
    >>> dtmp.head(3)
       count   ft ft_type   peak  gene     sample replicate   size
    0      1  AAA   codon  peak1  DDX5  CTRL_test        R1  20-35
    1      0  AAT   codon  peak1  DDX5  CTRL_test        R1  20-35
    2      2  AAG   codon  peak1  DDX5  CTRL_test        R1  20-35
    >>> dtmp[dtmp['ft_type'] == 'aa'].head(3)
        count ft ft_type   peak  gene     sample replicate   size
    61      0  R      aa  peak1  DDX5  CTRL_test        R1  20-35
    62      0  H      aa  peak1  DDX5  CTRL_test        R1  20-35
    63      3  K      aa  peak1  DDX5  CTRL_test        R1  20-35

    """
    if (adjusted_peak[2] - adjusted_peak[1]) % 3 != 0:
        raise ValueError(f"adjusted peak {adjusted_peak} should have a "
                         f"lenght of 3 ")
    codons = str(dic_seq[adjusted_peak[0]][adjusted_peak[1]:adjusted_peak[2]])
    d = get_codon_dic()
    daa = get_aa_dic()
    for i in range(0, len(codons) - 2, 3):
        try:
            cur_codon = codons[i:i+3].upper()
            d[cur_codon] += 1
            daa[str(Seq(cur_codon).translate())] += 1
        except KeyError:
            print(f"Warning: codon stop at peak {adjusted_peak}")
            return None
    dic = {"count": [], "ft": [], "ft_type": [], "peak": [], "gene": [],
           "sample": [], "replicate": [], "size": [], "codon_in_peak": []}
    for c in d:
        dic["count"].append(d[c])
        dic["ft"].append(c)
        dic["ft_type"].append("codon")
    for a in daa:
        dic["count"].append(daa[a])
        dic["ft"].append(a)
        dic["ft_type"].append("aa")
    dic["peak"] = [adjusted_peak[3]] * len(dic["count"])
    dic["gene"] = [adjusted_peak[0]] * len(dic["count"])
    dic["sample"] = [sample] * len(dic["count"])
    dic["replicate"] = [replicate] * len(dic["count"])
    dic["size"] = [size] * len(dic["count"])
    dic["codon_in_peak"] = [int((adjusted_peak[2] - adjusted_peak[1]) / 3)] * \
                           len(dic["count"])
    return pd.DataFrame(dic)


def create_table(dic_peak: Dict, dic_cds: Dict, genome: str,
                 sample: str, replicate: str, size: str,
                 resize: int) -> pd.DataFrame:
    """
    Create the table counting codons present in peaks.

    :param dic_peak: A dictionary containing peaks
    :param dic_cds: a dictionary containing CDS
    :param genome: A genome fasta file containing gene sequences
    :param sample: The name of the sample from which the peaks comes from
    :param replicate: The name of the replicate of the sample from which \
    the replicate came from.
    :param size: The size of footprint considered
    :param resize: The number of nucleotide to add at each side of a peak.
    :return: The dataframe of codon count in peaks
    """
    tot_peak = 0
    coding_peak = 0
    good_peak = 0
    list_df = []
    dic_seq = Fasta(genome)
    for gene in dic_peak:
        for peak in dic_peak[gene]:
            tot_peak += 1
            try:
                overlapping_cds = get_overlapping(peak, dic_cds[gene])
            except KeyError:
                print(f"{gene} does't have any unique CDS")
                overlapping_cds = None
            if overlapping_cds is not None:
                coding_peak += 1
                adj_peak = adjust_peak(peak, overlapping_cds, resize)
                tmp_df = codon_count(adj_peak, dic_seq, sample,
                                     replicate, size)
                if tmp_df is not None:
                    good_peak += 1
                    list_df.append(tmp_df)
    print(f"total enlarged peaks: {tot_peak}")
    print(f"coding peaks: {coding_peak}")
    print(f"conserved peaks: {good_peak}")
    return pd.concat(list_df, ignore_index=True, axis=0)


@lp.parse(peak_bed="file", annotation="file", bam_file="file", genome="file",
          min_fp_size=range(101), max_fp_size="max_fp_size > 0",
          threshold=range(100), resize=range(20))
def main_function(peak_bed: str, annotation: str,
                  genome: str, sample: str, replicate: str,
                  min_fp_size: int = 20, max_fp_size: int = 40,
                  threshold: float = 0, resize: int = 0,
                  outfile: str = "./count_table.txt") -> None:
    """
    Count the codon located in peaks.

    :param peak_bed: A bed file containing peaks coverage in a \
    ribosome profiling analysis.
    :param annotation: A file containing CDS and exons for every gene.
    :param genome: A genome fasta file containing gene sequences
    :param sample: The name of the sample from which the peaks comes from
    :param replicate: The name of the replicate of the sample from which \
    the replicate came from.
    :param min_fp_size: Minimum footprint size considered (default 20)
    :param max_fp_size: Maximum footprint size considered (default 40)
    :param threshold: The threshold score below which peaks are not \
    considered (default 0)
    :param resize: The number of nucleotide to add at each side of a peak. \
    (default 0)
    :param outfile: The file containing the codon counts \
    (default './count_table.txt')
    """
    cds_dic = load_bed(Path(annotation), "CDS-")
    peak_dic = load_bed(Path(peak_bed), None, threshold)
    size = f"{min_fp_size}-{max_fp_size}"
    df = create_table(peak_dic, cds_dic, genome, sample, replicate, size,
                      resize)
    df.to_csv(outfile, sep="\t", index=False)


if __name__ == "__main__":
    # doctest.testmod()
    main_function()
