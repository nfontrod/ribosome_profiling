#!/bin/bash

docker build -t lbmc/urqt_pigz:latest src/build_images/preprocessing_image/
docker build -t lbmc/genome_download:0.0 src/modules/download_input/
docker build -t lbmc/peak_caller:0.0 src/modules/peak_caller/
docker build -t lbmc/peak_composition:0.0 src/modules/peak_composition/
docker build -t lbmc/peak_stat:0.0 src/modules/peak_statistics/
docker build -t lbmc/fp_periodicity:0.0 src/modules/periodicity/
docker build -t lbmc/fp_composition:0.0 src/modules/find_ribosome_sites/
docker build -t lbmc/fp_intron_periodicity:0.0 src/modules/intron_periodicity/
docker build -t lbmc/fp_peak_caller_ribo_rna:0.0 src/modules/peak_caller_ribo-rna/
docker build -t lbmc/my_slop:0.0 src/modules/my_slop/