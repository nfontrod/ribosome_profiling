#!/bin/bash

singularity build ./bin/lbmc-urqt_pigz-latest.img src/build_images/preprocessing_image/Singularity
singularity build ./bin/lbmc-genome_download-0.0.img src/modules/download_input/Singularity
singularity build ./bin/lbmc-peak_caller-0.0.img src/modules/peak_caller/Singularity
singularity build ./bin/lbmc-peak_composition-0.0.img src/modules/peak_composition/Singularity
singularity build ./bin/lbmc-peak_stat-0.0.img src/modules/peak_statistics/Singularity
singularity build ./bin/lbmc-fp_periodicity-0.0.img src/modules/periodicity/Singularity
singularity build ./bin/lbmc-fp_composition-0.0.img src/modules/find_ribosome_sites/Singularity
singularity build ./bin/lbmc-fp_intron_periodicity-0.0.img src/modules/intron_periodicity/Singularity
singularity build ./bin/lbmc-fp_peak_caller_ribo_rna-0.0.img src/modules/peak_caller_ribo-rna/Singularity
singularity build ./bin/lbmc-my_slop-0.0.img src/modules/my_slop/Singularity